set $lastcs = -1

define hook-stop
  # There doesn't seem to be a good way to detect if we're in 16- or
  # 32-bit mode, but we always run with CS == 8 in 32-bit mode.
  if $cs == 8 || $cs == 27
    if $lastcs != 8 && $lastcs != 27
      set architecture i386
    end
    x/i $pc
  else
    if $lastcs == -1 || $lastcs == 8 || $lastcs == 27
      set architecture i8086
    end
    # Translate the segment:offset into a physical address
    printf "[%4x:%4x] ", $cs, $eip
    x/i $cs*16+$eip
  end
  set $lastcs = $cs
end

echo + target remote localhost:1234\n
target remote localhost:1234

# first stage 
echo + symbol-file obj/boot0/boot0\n
add-symbol-file obj/boot0/boot0.o 0x7c00
# second stage
echo + add-symbol-file obj/boot/boot\n
add-symbol-file obj/boot/boot.o 0x8000
# kernel
echo + symbol-file obj/kernel/kernel\n
add-symbol-file obj/kernel/kernel 0xc0100000 -s .data 0xc011c000
