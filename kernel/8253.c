#include "common/x86.h"
#include "common/types.h"
#include "kernel/stdio.h"

#include "kernel/8259.h"
#include "kernel/8253.h"

unsigned int volatile sys_ticks = 0;


void
time_tick(void)
{
        sys_ticks++;
}

static unsigned int
get_current_ticks(void)
{
        return sys_ticks;
}

__attribute__ ((noinline)) void 
delay_ms(unsigned int ms)
{
        unsigned int until = get_current_ticks() + ms;

        dprintf("Start ticking for %u until %u\n", ms, until);
        while (until > get_current_ticks()) {
                __asm __volatile("pause");
        }
        dprintf("Stop ticking...\n");
}

void
pit_init(uint16_t freq)
{
        uint16_t reload;

        reload = 1193180 / freq;

        kprintf("Computed reloaded value to: %u\n", reload);

        /*
         * Binary counting, Mode 3, Read or Load LSB first then MSB,
         * Channel 0
         */
        outb(PIT_MODE, 0x36);

        /* when to fire */
        outb(PIT_CHANNEL_0, reload & 0x00ff);   /* LSB */
        outb(PIT_CHANNEL_0, reload >> 8);       /* MSB */

        /* Enable timer interrupts */
        irq_setmask_8259A(irq_mask_8259A & ~(1 << 0));
}
