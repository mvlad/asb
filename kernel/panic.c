#include "common/x86.h"

#include "kernel/panic.h"
#include "kernel/stdio.h"

/**
 * Variable panicstr contains argument to first call to panic; used as flag
 * to indicate that the kernel has already called panic.
 */
const char *panicstr;

void
_panic(const char *file, int line, const char *fmt, ...)
{
        va_list ap;

        if (panicstr) {
                goto dead;
        }

        panicstr = fmt;

        /* Be extra sure that the machine is in as reasonable state */
        __asm __volatile("cli; cld");

        va_start(ap, fmt);
        kprintf("KERNEL PANIC at %s:%d: ", file, line);
        vcprintf(fmt, ap);
        kprintf("\n");
        va_end(ap);

dead:
        native_hlt();
}

/* like panic, but don't */
void
_warn(const char *file, int line, const char *fmt,...)
{
        va_list ap;

        va_start(ap, fmt);
        kprintf("KERNEL warning at %s:%d: ", file, line);
        vcprintf(fmt, ap);
        kprintf("\n");
        va_end(ap);
}

