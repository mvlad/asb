#include "common/types.h"
#include "common/x86.h"

#include "kernel/mmu.h"
#include "kernel/trap.h"
#include "kernel/console.h"
#include "kernel/stdio.h"
#include "kernel/assert.h"

#include "kernel/8259.h"
#include "kernel/8253.h"

#include "kernel/serial.h"
#include "kernel/cga.h"
#include "kernel/kbd.h"

extern unsigned int sys_ticks;

/**
 * CPU#0 task state
 */
struct task_state cpu_ts;

/**
 * For debugging, so print_tf can distinguish between printing a saved
 * trapframe and printing the current trapframe and print some additional
 * information in the latter case.
 */
static struct tf *last_tf;

struct irq_handler {
        uint32_t irq_id;
        uintptr_t handler;
        uint32_t priv_level;
};

/** IRQ arrays to hold all vectored interrupts */
extern struct irq_handler irq_handler_array[];

/**
 * Global descriptor table.
 *
 * Set up global descriptor table (GDT) with separate segments for
 * kernel mode and user mode.  Segments serve many purposes on the x86.
 * We don't use any of their memory-mapping capabilities, but we need
 * them to switch privilege levels. 
 *
 * The kernel and user segments are identical except for the DPL.
 * To load the SS register, the CPL must equal the DPL.  Thus,
 * we must duplicate the segments for the user and the kernel.
 *
 * In particular, the last argument to the SEG macro used in the
 * definition of gdt specifies the Descriptor Privilege Level (DPL)
 * of that descriptor: 0 for kernel and 3 for user.
 *
 */
struct segdesc gdt[6] = {
        /** 0x0 - unused (always faults -- for trapping NULL far pointers) */
        SEG_NULL,

        /** 0x8 - kernel code segment */
        [GD_KT >> 3] = SEG(STA_X | STA_R, 0x0, 0xffffffff, 0),

        /** 0x10 - kernel data segment */
        [GD_KD >> 3] = SEG(STA_W, 0x0, 0xffffffff, 0),

        /** 0x18 - user code segment */
        [GD_UT >> 3] = SEG(STA_X | STA_R, 0x0, 0xffffffff, 3),

        /** 0x20 - user data segment */
        [GD_UD >> 3] = SEG(STA_W, 0x0, 0xffffffff, 3),

        /** 
         * Per-CPU TSS descriptors (starting from GD_TSS0) are initialized in
         * do_init_trap()
         */
        [GD_TSS0 >> 3] = SEG_NULL
};

struct pseudodesc gdt_pd = {
        sizeof(gdt) - 1, (unsigned long) gdt
};


/**
 * Interrupt descriptor table.  
 *
 * Must be built at run time because shifted function addresses can't be
 * represented in relocation records.
 */
struct gatedesc idt[256] = { { 0 } };

struct pseudodesc idt_pd = {
        sizeof(idt) - 1, (uint32_t) idt
};

/**
 *  @brief print contents of registers
 */
static void
print_regs(struct regs *regs)
{
        kprintf("  eax  0x%08x\n", regs->reg_eax);
        kprintf("  edx  0x%08x\n", regs->reg_edx);
        kprintf("  ebx  0x%08x\n", regs->reg_ebx);
        kprintf("  ecx  0x%08x\n", regs->reg_ecx);
        kprintf("  edi  0x%08x\n", regs->reg_edi);
        kprintf("  esi  0x%08x\n", regs->reg_esi);
        kprintf("  ebp  0x%08x\n", regs->reg_ebp);
        kprintf("  oesp 0x%08x\n", regs->reg_oesp);
}

static void __attribute__ ((noinline))
restore_tf(struct tf *tf)
{
        __asm __volatile("movl %0,%%esp\n"
                        "\tpopal\n"
                        "\tpopl %%es\n"
                        "\tpopl %%ds\n"
                        "\taddl $0x8,%%esp\n" /* skip tf_trapno and tf_errcode */
                        "\tiret"
                        : : "g" (tf) : "memory");

        /* placate the compiler */
        kprintf("iret failed");
        native_hlt();
}


static const char *
trapname(uint32_t trapno)
{
        static const char * const excnames[] = {
                "Divide error",
                "Debug",
                "Non-Maskable Interrupt",
                "Breakpoint",
                "Overflow",
                "BOUND Range Exceeded",
                "Invalid Opcode",
                "Device Not Available",
                "Double Fault",
                "Coprocessor Segment Overrun",
                "Invalid TSS",
                "Segment Not Present",
                "Stack Fault",
                "General Protection",
                "Page Fault",
                "(unknown trap)",
                "x87 FPU Floating-Point Error",
                "Alignment Check",
                "Machine-Check",
                "SIMD Floating-Point Exception"
        };

        if (trapno < sizeof(excnames) / sizeof(excnames[0])) {
                return excnames[trapno];
        }

        if (trapno == T_SYSCALL) {
                return "System call";
        }

        if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16) {
                return "Hardware Interrupt";
        }

        return "(unknown trap)";
}

/**
 * @brief print a trapframe information
 * @param tf the trapframe to print
 */
static void
print_tf(struct tf *tf)
{
        kprintf("TRAP frame at %p\n", tf);
        print_regs(&tf->tf_regs);

        kprintf("  es   0x----%04x\n", tf->tf_es);
        kprintf("  ds   0x----%04x\n", tf->tf_ds);
        kprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));

        /*
         * If this trap was a page fault that just happened
         * (so %cr2 is meaningful), print the faulting linear address.
         */
        if (tf == last_tf && tf->tf_trapno == T_PGFLT) {
                kprintf("cr2  0x%08x\n", rcr2());
        }

        kprintf("  err  0x%08x", tf->tf_err);

        /* For page faults, print decoded fault error code:
         * - U/K=fault occurred in user/kernel mode
         * - W/R=a write/read caused the fault
         * - PR=a protection violation caused the fault (NP=page not present).
         */
        if (tf->tf_trapno == T_PGFLT) {
                kprintf(" [");

                if (tf->tf_err & 4) {
                        kprintf("user, ");
                } else {
                        kprintf("kernel, ");
                }

                if (tf->tf_err & 2) {
                        kprintf("write, ");
                } else {
                        kprintf("read, ");
                }

                if (tf->tf_err & 1) {
                        kprintf("protection]\n");
                } else {
                        kprintf("not-present]\n");
                }

        } else {
                kprintf("\n");
        }

        kprintf("  eip  0x%08x\n", tf->tf_eip);
        kprintf("  cs   0x----%04x\n", tf->tf_cs);
        kprintf("  flag 0x%08x\n", tf->tf_eflags);

        if ((tf->tf_cs & 3) != 0) {
                kprintf("  esp  0x%08x\n", tf->tf_esp);
                kprintf("  ss   0x----%04x\n", tf->tf_ss);
        }
}


static void
monitor(const char *prompt)
{
        char *buf;

        while (1) {
                buf = readline(prompt);
                char c = *buf;
                switch (c) {
                        case 'q':
                        case 'r':
                                kprintf("Rebooting...\n");
                                outb(0x92, 0x3);
                                break;
                        default:
                                break;
                }
        }
}

static void
read_data_serial(void)
{
        int c = getchar_serial();

        if (c < 0) {
                kprintf("read err: %e\n", c);
        } else if ((c == '\b' || c == '\x7f')) {
                serial_putc('\b');
        } else if (c >= ' ') {
                serial_putc(c);
        } else if (c == '\n' || c == '\r') {
                serial_putc('\n');
        }
}

static void
read_data_kbd(void)
{
        int c = getchar_kbd();

        if (c < 0) {
                kprintf("read err: %e\n", c);
        } else if ((c == '\b' || c == '\x7f')) {
                cga_putc('\b');
        } else if (c >= ' ') {
                cga_putc(c);
        } else if (c == '\n' || c == '\r') {
                cga_putc('\n');
        }
}

static void
handle_serial_interrupt(void)
{
        read_data_serial();
        irq_eoi();
}

static void
handle_kbd_interrupt(void)
{
        read_data_kbd();
        irq_eoi();
}

static void
handle_timer_interrupt(void)
{
        time_tick();
        irq_eoi();
}

static void
handle_int3(struct tf *tf)
{
        print_tf(tf);
        monitor("$ ");
}

static void __attribute__ ((noinline))
trap_dispatch(struct tf *tf)
{
        if (tf->tf_trapno == IRQ_OFFSET + IRQ_SPURIOUS) {
                kprintf("Spurious interrupt on irq 7\n");
                print_tf(tf);
                return;
        }

        switch (tf->tf_trapno) {
                case T_PGFLT:
                        kprintf("Trapping PF\n");
                        print_tf(tf);
                        native_hlt();
                        return;
                case T_SYSCALL:
                        kprintf("Trapping SYSCALL\n");
                        native_hlt();
                        return;
                case T_BRKPT:
                case T_DEBUG:
                        handle_int3(tf);
                        return;
                case IRQ_OFFSET + IRQ_TIMER:
                        handle_timer_interrupt();
                        restore_tf(tf);
                        return;
                case IRQ_OFFSET + IRQ_KBD:
                        handle_kbd_interrupt();
                        restore_tf(tf);
                        return;
                case IRQ_OFFSET + IRQ_SERIAL:
                        handle_serial_interrupt();
                        restore_tf(tf);
                        return;
        }

        print_tf(tf);

        if (tf->tf_cs == GD_KT) {
                panic("Unhandled trap in kernel");
        }

}

/**
 * \brief handle a trap
 * \param tf a pointer to a Trapframe
 * \ingroup KERN
 *
 * This function gets called aftering setting up stack
 * frame from trapentry.S and calls trap_dispatch
 */
void
trap(struct tf *tf)
{
        /* 
         * The environment may have set DF and some versions of GCC rely on DF
         * being clear
         */
        asm volatile("cld" ::: "cc");

        extern char *panicstr;
        if (panicstr) {
                native_hlt();
        }

        /*
         * Check that interrupts are disabled.  If this assertion fails, DO NOT
         * be tempted to fix it by inserting a "cli" in the interrupt path.
         */
        if ((read_eflags()) & FL_IF) {
                kprintf("Interrupts are not disabled!\n");
                native_hlt();
        }

        if ((tf->tf_cs & 3) == 3) {
                kprintf("Trapping in user-mode...\n");
        }

        /* 
         * Record that tf is the last real trapframe so print_trapframe can
         * print some additional information.
         */
        last_tf = tf;

        /* Dispatch based on what type of trap occurred */
        trap_dispatch(tf);
}

/**
 * @brief Set up the Task State Segment (TSS) and the TSS descriptor for CPU 0. 
 *
 * Setup a TSS so that we get the right stack when we trap to the kernel.
 * 
 */
static void
task_state_init(void)
{
        struct task_state *nts = &cpu_ts;

        nts->ts_esp0 = KSTACKTOP;
        nts->ts_ss0 = GD_KD;

        /*
         * Using gdt[GD_TSS0 >> 3] for CPU#0 TSS descriptor
         *
         * The kernel stack where mapped in mem_init_mp()
         * 
         * Initialize the TSS slot of the gdt. 
         */
        gdt[GD_TSS0 >> 3] = SEG16(STS_T32A,  (uint32_t) (nts), sizeof(struct task_state), 0);
        gdt[GD_TSS0 >> 3].sd_s = 0;

        /*
         * Load the TSS selector (like other segment selectors, the
         * bottom three bits are special; we leave them 0)
         */
        ltr(GD_TSS0);

        /* Load the IDT */
        lidt(&idt_pd);

        kprintf("Loaded IDT\n");
}


/**
 *  @brief Load GDT and segment descriptors. 
 */
static void
gdt_init(void)
{

        lgdt(&gdt_pd);

        /*
         * The kernel never uses GS or FS, so we leave those set to
         * the user data segment.
         */
        asm volatile("movw %%ax, %%gs" :: "a" (GD_UD | 3));
        asm volatile("movw %%ax, %%fs" :: "a" (GD_UD | 3));

        /* 
         * The kernel does use ES, DS, and SS.  
         *
         * We'll change between the kernel and 
         * user data segments as needed.
         */
        asm volatile("movw %%ax, %%es" :: "a" (GD_KD));
        asm volatile("movw %%ax, %%ds" :: "a" (GD_KD));
        asm volatile("movw %%ax, %%ss" :: "a" (GD_KD));

        /* Load the kernel text segment into CS. */
        asm volatile("ljmp %0, $1f\n 1:\n" :: "i" (GD_KT));

        /* 
         * For good measure, clear the local descriptor table (LDT),
         * since we don't use it.
         */
        lldt(0);

}

static void
init_idt(void)
{
        int i;

        for (i = 0; irq_handler_array[i].irq_id != T_DEFAULT; i++) {
                SETGATE(idt[irq_handler_array[i].irq_id], 0, GD_KT,
                        irq_handler_array[i].handler,
                        irq_handler_array[i].priv_level);
                dprintf("Setting up gate %d ", i);
                dprintf("irqid 0x%x ", irq_handler_array[i].irq_id);
                dprintf("handler 0x%x ", irq_handler_array[i].handler);
                dprintf("priv_level 0x%x\n", irq_handler_array[i].priv_level);
        }

        dprintf("Setting up int3\n");
        /* break point / int3 */
        SETGATE(idt[irq_handler_array[T_BRKPT].irq_id], 0, GD_KT,
                irq_handler_array[T_BRKPT].handler,
                irq_handler_array[T_BRKPT].priv_level);
}

void
trap_init(void)
{

        kprintf("Setting up GDT\n");

        gdt_init();

        kprintf("Setting up traps...\n");

        init_idt();

        task_state_init();
}
