#include "common/types.h"

#include "kernel/stdio.h"
#include "kernel/stdarg.h"
#include "kernel/string.h"

#define LARGE_DOUBLE_THR (9.1e18)

#ifndef ABS
#define ABS(x) (((x) > 0) ? (x) : -(x))
#endif

static const char hex[] = "0123456789abcdef";

typedef void (*print_func)(int ch, void *putdat);

static void 
putch(int ch, int *cnt)
{
        cons_putc(ch);
        *cnt++;
}

/**
 * @brief Get an unsigned int of various possible sizes from a varargs list,
 * depending on the lflag parameter.
 */
static unsigned long long
getuint(va_list *ap, int lflag)
{
	if (lflag >= 2) {
		return va_arg(*ap, unsigned long long);
        } else if (lflag) {
		return va_arg(*ap, unsigned long);
        } else {
		return va_arg(*ap, unsigned int);
        }
}

/**
 * @brief Get an signed int of various possible sizes from a varargs list,
 * depending on the lflag parameter
 */
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2) {
		return va_arg(*ap, long long);
        } else if (lflag) {
		return va_arg(*ap, long);
        } else {
		return va_arg(*ap, int);
        }
}

/**
 * @brief Print a number (base <= 16) in reverse order, using specified putch
 * function and associated pointer putdat.
 * @param putch a pointer to a function
 * @param putdat associated pointer
 * @param num a number
 * @param base base
 * @param width with
 * @param padc bla
 */
static void
printnum(print_func putch, void *putdat, unsigned long long num, 
         unsigned base, int width, int padc)
{
	/* first recursively print all preceding (more significant) digits */
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		/* print any needed pad characters before first digit */
		while (--width > 0)
			putch(padc, putdat);
	}

	/* then print this (the least significant) digit */
	putch(hex[num % base], putdat);
}

static void 
printfloat(print_func putch, void *putdat, double num, unsigned int base, 
           int width, int padc, uint8_t digits)
{
        double round = 0.5;
        uint8_t i;

        long long int_part;
        double rem;

        if (ABS(num) >= LARGE_DOUBLE_THR) {
                if (num < 0.0) {
                        putch('-', putdat);
                }
                return;
        }
        if (num < 0.0) {
                putch('-', putdat);
                num = -num;
        }

        for (i = 0;  i < digits; i++) {
                round /= 10.0;
        }
        num += round;

        int_part = (long long) num;
        rem = num - int_part;
        printnum(putch, putdat, int_part, base, width, padc);

        if (digits > 0) {
                putch('.', putdat);
        }

        while (digits--) {
                rem *= 10.0;
                int to_print = (int) rem;
                printnum(putch, putdat, to_print, base, width, padc);
                rem -= to_print;
        }
}

static void
vprintfmt(print_func putch, void *putdat, const char *fmt, va_list ap)
{
	register const char *p;
	register int ch, err;

	unsigned long long num;

	int base, lflag; 
        int width, prec, aflag;

	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0') {
				return;
                        }
			putch(ch, putdat);
		}

		/* Process a %-escape sequence */
		padc = ' ';
		width = prec = -1;
		lflag = aflag = 0;
reswitch:
                ch = *(unsigned char *) fmt++;
		switch (ch) {

		/* flag to pad on the right */
		case '-':
			padc = '-';
			goto reswitch;
		/* flag to pad with 0's instead of spaces */
		case '0':
			padc = '0';
			goto reswitch;
		/* width field */
		case '1': case '2':
		case '3': case '4':
		case '5': case '6':
		case '7': case '8':
		case '9':
			for (prec = 0; ; ++fmt) {
				prec = prec * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
			goto process_prec;
		case '*':
			prec = va_arg(ap, int);
			goto process_prec;
		case '.':
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			aflag = 1;
			goto reswitch;
process_prec:
			if (width < 0) {
				width = prec, prec = -1;
                        }

			goto reswitch;
		/* long flag (doubled for long long) */
		case 'l':
			lflag++;
			goto reswitch;
		/* character */
		case 'c':
			putch(va_arg(ap, int), putdat);
                        break;
		/* string */
		case 's':
			if ((p = va_arg(ap, char *)) == NULL) {
				p = "(null)";
                        }

			if (width > 0 && padc != '-') {
                                width -= strnlen(p, prec);
#if 0
				for (; width > 0; width--) {
					putch(padc, putdat);
                                }
#endif
                                while (width--)
                                        putch(padc, putdat);
                        }

			for (; (ch = *p++) != '\0' && (prec < 0 || --prec >= 0); width--) {
				if (aflag && (ch < ' ' || ch > '~')) {
					putch('?', putdat);
                                } else {
					putch(ch, putdat);
                                }
                        }

			for (; width > 0; width--) {
				putch(' ', putdat);
                        }
                        break;
		/* (signed) decimal */
		case 'd':
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
			goto number;
		/* unsigned decimal */
		case 'u':
			num = getuint(&ap, lflag);
			base = 10;
			goto number;
		/* (unsigned) octal */
		case 'o':
			num = getuint(&ap, lflag);
			base = 8;
                        goto number;
		/* pointer */
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
		/* (unsigned) hexadecimal */
		case 'x':
			num = getuint(&ap, lflag);
			base = 16;
number:
			printnum(putch, putdat, num, base, width, padc);
                        break;
                case 'f': {
                        double n = va_arg(ap, double);
                        printfloat(putch, putdat, n, 10, width, padc, 12);
                        }
                        break;
		/* escaped '%' character */
		case '%':
			putch(ch, putdat);
                        break;
		/* unrecognized escape sequence - just print it literally */
		default:
			putch('%', putdat);
			for (fmt--; fmt[-1] != '%'; fmt--)
				; /* do nothing */
                        break;
		}
	}
}


int 
vcprintf(const char *fmt, va_list ap)
{
        int cnt = 0;
        vprintfmt((void *) putch, &cnt, fmt, ap);
        return cnt;
}

int kprintf(const char *fmt, ...)
{
        va_list ap;
        int cnt;

        va_start(ap, fmt);
        cnt = vcprintf(fmt, ap);
        va_end(ap);

        return cnt;
}
