OBJDIRS += kernel

INCLUDE = -Iinclude
KERN_LDFLAGS := $(LDFLAGS) -T kernel/kernel.ld -nostdlib

# entry.S must be first, so that it's the 
# first code in the text segment
KERN_SRCFILES := kernel/entry.S

KERN_SRCFILES += kernel/console.c kernel/serial.c\
		 kernel/cga.c kernel/kbd.c

KERN_SRCFILES += kernel/panic.c kernel/string.c kernel/trap.c 

# PIC and PIT
KERN_SRCFILES += kernel/8259.c kernel/8253.c

# memory
KERN_SRCFILES += kernel/pmap.c

KERN_SRCFILES += kernel/printf.c

KERN_SRCFILES += kernel/draw.c kernel/kmain.c

# Only build files if they exist.
KERN_SRCFILES := $(wildcard $(KERN_SRCFILES))

KERN_OBJFILES := $(patsubst %.c, $(OBJDIR)/%.o, $(KERN_SRCFILES))
KERN_OBJFILES := $(patsubst %.S, $(OBJDIR)/%.o, $(KERN_OBJFILES))

# How to build kernel object files
$(OBJDIR)/kernel/%.o: kernel/%.S
	$(V)echo + as [KERN] $<
	$(V)mkdir -p $(@D)
	$(V)$(CC) -nostdinc $(INCLUDE) $(KERN_CFLAGS) -c -o $@ $<

$(OBJDIR)/kernel/%.o: kernel/%.c
	$(V)echo + CC [KERN] $<
	$(V)mkdir -p $(@D)
	$(V)$(CC) -nostdinc $(INCLUDE) $(KERN_CFLAGS) -c -o $@ $<


# How to build the kernel itself
$(OBJDIR)/kernel/kernel: $(KERN_OBJFILES) $(KERN_BINFILES)
	$(V)echo + LD [KERN]
	$(V)$(LD) -o $@ $(KERN_LDFLAGS) $(KERN_OBJFILES) $(GCC_LIB) -b binary $(KERN_BINFILES)
	$(V)$(OBJDUMP) -S $@ > $@.asm
	$(V)$(NM) -n $@ > $@.sym


# these have to be built in order to create the image
TARGET_BOOT := $(OBJDIR)/boot0/boot0 $(OBJDIR)/boot/boot 
TARGET_KERN := $(OBJDIR)/kernel/kernel

TARGETS := $(TARGET_BOOT) $(TARGET_KERN)

# qemu image we create initially
TMP_IMAGE := $(OBJDIR)/kernel/kernel.img~

# this the disk we're going to boot
HDD_IMAGE := $(OBJDIR)/kernel/hda.img

# partitions and disks
#
# the layout of the disk
PART_SCHEME = boot/partition.second.64M

# EXT2 partitions in the partition scheme
PART_DISK = /dev/mapper/loop0p2

SFDISK_E = $(shell test -x /sbin/sfdisk || echo "no")
KPARTX_E = $(shell test -x /sbin/kpartx || echo "no")

ifeq ($(SFDISK_E), no)
$(error No /sbin/sfdisk)
endif

ifeq ($(KPARTX_E), no)
$(error No /sbin/kpartx)
endif
	

# How to build the kernel disk image: issue a sync each to to make sure 
# we've got the proper thing(s)
$(OBJDIR)/kernel/kernel.img: $(TARGETS)
	$(V)echo Generating bootable disk...
	$(V)echo Creating initial disk image...
	$(V)qemu-img create -f raw $(TMP_IMAGE) 64M >/dev/null 2>&1
	$(V)dd if=$(OBJDIR)/boot0/boot0 of=$(TMP_IMAGE) \
		  bs=446 count=1 conv=notrunc 2>/dev/null
	$(V)dd if=$(OBJDIR)/boot/boot of=$(TMP_IMAGE)\
		  seek=1 bs=512 conv=notrunc 2>/dev/null
	$(V)sync
	$(V)echo Formating disk...
	$(V)sudo sh -c "/sbin/sfdisk $(TMP_IMAGE) --force\
	       	< $(PART_SCHEME) >$(OBJDIR)/boot/format.out 2>/dev/null"
	$(V)sync
	$(V)sudo sh -c "/sbin/kpartx -avs $(TMP_IMAGE) \&>/dev/null"
	$(V)echo Creating EXT2 partition...
	$(V)sudo sh -c "/sbin/mkfs.ext2 $(PART_DISK) >/dev/null 2>&1"
	$(V)sudo e2mkdir $(PART_DISK):/sbin
	$(V)sudo e2mkdir $(PART_DISK):/bin
	$(V)sudo e2mkdir $(PART_DISK):/usr
	$(V)sudo e2mkdir $(PART_DISK):/home
	$(V)sudo e2mkdir $(PART_DISK):/boot
	$(V)sudo e2mkdir $(PART_DISK):/lib
	$(V)sudo e2mkdir $(PART_DISK):/boot/kernel
	$(V)sudo e2mkdir $(PART_DISK):/boot/kernel/path
	$(V)sudo e2mkdir $(PART_DISK):/boot/kernel/path/to
	$(V)sync
	$(V)echo Copying kernel...
	$(V)sudo e2cp $(TARGET_KERN) $(PART_DISK):/boot/kernel/image/path/to/kern
	$(V)sync
	$(V)sudo sh -c "/sbin/kpartx -d $(TMP_IMAGE) >/dev/null 2>&1"
	$(V)cp $(TMP_IMAGE) $(HDD_IMAGE)
	$(V)sync
	$(V)echo Image ready in '**'$(HDD_IMAGE)'**'

