#include "common/x86.h"

#include "kernel/console.h"
#include "kernel/serial.h"

#include "kernel/8259.h"

bool serial_exists;

static int
serial_proc_data(void)
{
	if (!(inb(USART1 + USART_LSR) & USART_LSR_DATA))
		return -1;

	return inb(USART1 + USART_RX);
}

void
serial_putc(int c)
{
	int i;
	
	for (i = 0; !data_avail() && i < 12800; i++)
		delay();
	
	outb(USART1 + USART_TX, c);
}

void
serial_intr(void)
{
	if (serial_exists) {
		cons_intr(serial_proc_data);
        }
}

void
serial_init(void)
{
	/* Turn off the FIFO */
	outb(USART1 + USART_FCR, 0);
	
	/* Set speed; requires DLAB latch */
	outb(USART1 + USART_LCR, USART_LCR_DLAB);
	outb(USART1 + USART_DLL, (uint8_t) (115200 / 9600));
	outb(USART1 + USART_DLM, 0);

	/* 8 data bits, 1 stop bit, parity off; turn off DLAB latch */
	outb(USART1 + USART_LCR, USART_LCR_WLEN8 & ~USART_LCR_DLAB);

	/* No modem controls */
	outb(USART1 + USART_MCR, 0);

	/* Enable rcv interrupts */
	outb(USART1 + USART_IER, USART_IER_RDI);


	/* Serial port doesn't exist if USART_LSR returns 0xFF */
	serial_exists = (inb(USART1 + USART_LSR) != 0xff);

	/* Clear any preexisting overrun indications and interrupts */
	(void) inb(USART1 + USART_IIR);
	(void) inb(USART1 + USART_RX);

	/* Enable serial interrupts */
	if (serial_exists) {
		irq_setmask_8259A(irq_mask_8259A & ~(1 << 4));
        }
}
