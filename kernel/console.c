#include "common/x86.h"

#include "kernel/serial.h"
#include "kernel/cga.h"
#include "kernel/kbd.h"

#include "kernel/console.h"
#include "kernel/string.h"
#include "kernel/stdio.h"

#include "kernel/error.h"

#define BUFLEN  1024

extern bool serial_exists;

/**
 * Manages the console input buffer, where we stash characters received
 * from the keyboard or serial port whenever the corresponding interrupt occurs.
 */
static struct {
	uint8_t buf[CONSBUFSIZE];
	uint32_t rpos;
	uint32_t wpos;
} cons;


void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {

		if (c == 0) {
			continue;
                }

		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE) {
			cons.wpos = 0;
                }
	}
}

int
cons_getc(void)
{
	int c;

        /* 
         * poll for any pending input characters, so that this function works
         * even when interrupts are disabled 
         */
	serial_intr();
	kbd_intr();

	/* grab the next character from the input buffer. */
	if (cons.rpos != cons.wpos) {
		c = cons.buf[cons.rpos++];

		if (cons.rpos == CONSBUFSIZE) {
			cons.rpos = 0;
                }

		return c;
	}
	return 0;
}

static int
serial_getc(void)
{
	int c;

        /* drain serial */
	serial_intr();

	/* grab the next character from the input buffer. */
	if (cons.rpos != cons.wpos) {
		c = cons.buf[cons.rpos++];

		if (cons.rpos == CONSBUFSIZE) {
			cons.rpos = 0;
                }

		return c;
	}
	return 0;
}

static int
kbd_getc(void)
{
	int c;

        /* drain keyboard */
	kbd_intr();

	/* grab the next character from the input buffer. */
	if (cons.rpos != cons.wpos) {
		c = cons.buf[cons.rpos++];

		if (cons.rpos == CONSBUFSIZE) {
			cons.rpos = 0;
                }

		return c;
	}
	return 0;
}

void
cons_putc(int c)
{
	serial_putc(c);
	cga_putc(c);
}


int
getchar(void)
{
	int c;

	while ((c = cons_getc()) == 0)
		/* do nothing */;
	return c;
}

int
getchar_serial(void)
{
	int c = serial_getc();
	return c;
}

int
getchar_kbd(void)
{
	int c = kbd_getc();
	return c;
}

char *
readline(const char *prompt)
{
        int i, c, echo;
        static char buf[BUFLEN];

        if (prompt != NULL) {
                kprintf("%s", prompt);
        }

        i = 0;
        echo = 1;

        while (1) {
                c = getchar();
                if (c < 0) {
                        if (c != -E_EOF) {
                                kprintf("read err: %e\n", c);
                        }
                        return NULL;
                } else if ((c == '\b' || c == '\x7f') && i > 0) {
                        if (echo) {
                                cons_putc('\b');
                        }
                        i--;
                } else if (c >= ' ' && (i < BUFLEN - 1)) {
                        if (echo) {
                                cons_putc(c);
                        }
                        buf[i++] = c;
                } else if (c == '\n' || c == '\r') {
                        if (echo) {
                                cons_putc('\n');
                        }
                        buf[i] = 0;
                        return buf;
                }
        }
}

void
cons_init(void)
{
	cga_init();

	kbd_init();
        serial_init();
}

