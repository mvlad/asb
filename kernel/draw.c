#include "common/types.h"
#include "common/x86.h"
#include "common/cga.h"

#include "kernel/stdio.h"
#include "kernel/draw.h"

extern uint16_t *vid_mem;
extern uint8_t cur_x;
extern uint8_t cur_y;

void 
cls(void)
{
        int i;
        
        for (i = 0; i < CRT_SIZE; i++) {
                vid_mem[i] = BLANK;
        }

        /* Move the hardware cursor back to the start. */
        cur_x = cur_y = 0;
}

void
draw_dot(uint8_t x, uint8_t y, uint8_t color)
{
        uint16_t *loc, pos;

        pos = SET_CUR_POS(x, y);

        loc = vid_mem + pos;
        *loc = ' ' | (color << 8);

        cur_x += 1;
        cur_y += 1;

        /* move it */
        cga_move_cursor(SET_CUR_POS(cur_x, cur_y));
}

void 
draw_line(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t color)
{
        int dx, dy;
        int r;

        dx = x1 - x0;
        dy = y1 - y0;

        r  = (dx - dy) / 2;

        while (x0 <= x1) {
                x0 = x0 + 1;
                r  = r + dy;
                if (r >= dx) {
                        r = r - dx;
                        y0 = y0 + 1;
                }
                draw_dot(x0, y0, color);
        }
}

void
fill_by_dot(uint8_t color)
{
        uint8_t x, y;

        for (x = 0; x < CRT_COLS; x++) {
                for (y = 0; y < CRT_ROWS; y++) {
                        draw_dot(x, y, color);
                }
        }
}


void
draw_circle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color)
{
        int16_t f = 1 - r;
        int16_t ddF_x = 1; 
        int16_t ddF_y = -2 * r;
        int16_t x = 0;
        int16_t y = r;

        draw_dot(x0, y0 + r, color);
        draw_dot(x0, y0 - r, color);
        draw_dot(x0 + r, y0, color);
        draw_dot(x0 - r, y0, color);

        while (x < y) {
                if (f >= 0) {
                        y--;
                        ddF_y += 2;
                        f += ddF_y;
                }

                x++;
                ddF_x += 2;
                f += ddF_x;

                draw_dot(x0 + x, y0 + y, color);
                draw_dot(x0 - x, y0 + y, color);
                draw_dot(x0 + x, y0 - y, color);
                draw_dot(x0 - x, y0 - y, color);

                draw_dot(x0 + y, y0 + x, color);
                draw_dot(x0 - y, y0 + x, color);
                draw_dot(x0 + y, y0 - x, color);
                draw_dot(x0 - y, y0 - x, color);

        }
}

void
draw_horiz_line(uint8_t x, uint8_t y, uint8_t w, uint8_t color)
{
        uint8_t i;

        for (i = x; i < w; i++) {
                draw_dot(i, y, color);
        }
}

void
draw_vert_line(uint8_t x, uint8_t y, uint8_t h, uint8_t color)
{
        uint8_t i;

        for (i = y; i < h; i++) {
                draw_dot(x, i, color);
        }
}

void 
draw_rect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color)
{
        draw_horiz_line(x, y, w, color);
        draw_horiz_line(x, y + h - 1, w, color);

        draw_vert_line(x, y, h, color);
        draw_vert_line(x + w - 1, y, h, color);
}
