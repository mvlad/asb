#include "common/mb.h"
#include "kernel/mmu.h"
#include "kernel/trap.h"

/**
 * This code is linked at address ~(KERNBASE + 1 Meg), 
 * but the bootloader loads it at address ~1 Meg.
 *        
 * RELOC(x) maps a symbol x from its link address to its actual
 * location in physical memory (its load address).	 
 */
#define	RELOC(x)                        ((x) - KERNBASE)
#define MULTIBOOT_HEADER_FLAGS          0x00000003


/** @cond DOXYGEN_IGNORE_ASM */
.text

/* The Multiboot header */
.align 4
multiboot_header:
     /* magic */
     .long   MULTIBOOT_HEADER_MAGIC
     /* flags */
     .long   MULTIBOOT_HEADER_FLAGS
     /* checksum */
     .long   (-(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS))

/*
 * '_start' specifies the ELF entry point.  Since we haven't set up
 * virtual memory when the bootloader enters this code, we need the
 * bootloader to jump to the *physical* address of the entry point.
 */
.globl _start
_start = RELOC(entry)

.globl entry
entry:
        
        /* multi boot values are passed by registers, which we'll trash */

        /* save the magic value */
        movl    %eax, RELOC(magic)
        /* and multiboot_info */
        movl    %ebx, RELOC(multiboot)

	movw	$0x1234, 0x472			/* warm boot */

        /*
         * We haven't set up virtual memory yet, so we're running from
         * the physical address the boot loader loaded the kernel at: 1MB
         * (plus a few bytes).  However, the C code is linked to run at
         * KERNBASE + 1MB.  
         *
         * Hence, we set up a trivial page directory that
         * translates virtual addresses [KERNBASE, KERNBASE + 4MiB) to
         * physical addresses [0, 4MiB).
         *
         * We also map virtual addresses [0, 4MB) to physical addresses 
         * [0, 4MB).
         */

         /* dynamically construct a page table with 1024 entries */
         movl    $0x0, %edx
         movl    $0x0, %ebx
 
         /* counter for number of pages = 1024 * PGSIZE = 4MiB */
         movl    $0x0, %eax
 
again:
         movl    %eax, %ecx
         movl    %edx, %ebx
         /* permissions for respective page */
         or      $(PTE_P | PTE_W), %ebx
 
         /* indexed by ecx, incremented each loop */
         mov     %ebx, RELOC(pgtable)(,%ecx,4)
 
         inc     %eax
         /* each page has 4k */
         add     $PGSIZE, %edx
         /* we need 1024 pages for page table directory*/
         cmp     $0x400, %eax
         jne     again
 
         /* 
          * setup the actual mappings describe above
          * due to linking issues we use x + PTE_P
          */
 
         /* clear up, used as index -> ecx = 0x0 */
         xorl    %ecx, %ecx
 
 	/* map virtual addresses [0, 4MB) to physical addresses [0, 4MB) */
         movl    $((pgtable - KERNBASE) + PTE_P), RELOC(pgdir)(,%ecx,4)
 
         /* move index to KERNBASE >> PDXSHIFT -> ecx = 0x300, 768 */
         movl    $(KERNBASE >> PDXSHIFT), %ecx
 
 	/* 
          * map virtual addresses [KERNBASE, KERNBASE + 4MB) 
          * to physical addresses [0, 4MB) 
          */
         movl    $((pgtable - KERNBASE) + PTE_P + PTE_W), RELOC(pgdir)(,%ecx,4)

	 /*
          * Load the physical address of page directory into %cr3
          */
	movl	$(RELOC(pgdir)), %eax
	movl	%eax, %cr3

	/* Turn on paging. */
	movl	%cr0, %eax
	orl	$(CR0_PE | CR0_PG | CR0_WP), %eax
	movl	%eax, %cr0

        /*
	 * Now paging is enabled, but we're still running at a low EIP.
	 * 
         * Jump up above KERNBASE before entering C code.
         */
	mov	$relocated, %eax
	jmp	*%eax
relocated:

        /*
         * Clear the frame pointer register (EBP) so that once we get into
         * debugging C code, stack backtraces will be terminated properly.
         */
	movl	$0x0, %ebp	        

	/* set the stack pointer */
	movl	$(bootstacktop), %esp

        /* push multiboot data on stack */
        pushl   multiboot
        pushl   magic

	call	kmain

	/* Should never get here, but in case we do, just spin. */
spin:
        jmp spin

entry_trap:
        pushl %ds
        pushl %es
        pushal

        movl $GD_KD, %eax
        movw %ax, %ds
        movw %ax, %es

        pushl %esp
        call trap
        /* see restore_tf() from trap.c */

magic: .long 0
multiboot: .long 0

.data
.align  PGSIZE

/* allocate space for page table */
.globl pgtable
pgtable: .space 4096

/* allocate space for page directory */
.globl pgdir
pgdir: .space 4096

.data
.p2align	PGSHIFT		/* force page alignment */

.globl bootstack
bootstack: .space KSTKSIZE

.globl bootstacktop   
bootstacktop:


.global irq_handler_array
irq_handler_array:

TRAPHANDLER_NOEC(irq_handler_divide, T_DIVIDE, 0)
TRAPHANDLER_NOEC(irq_handler_debug, T_DEBUG, 0)
TRAPHANDLER_NOEC(irq_handler_nmi, T_NMI, 0)
TRAPHANDLER_NOEC(irq_handler_brkpt, T_BRKPT, 3)
TRAPHANDLER_NOEC(irq_handler_oflow, T_OFLOW, 0)
TRAPHANDLER_NOEC(irq_handler_bound, T_BOUND, 0)
TRAPHANDLER_NOEC(irq_handler_illop, T_ILLOP, 0)
TRAPHANDLER_NOEC(irq_handler_device, T_DEVICE, 0)

TRAPHANDLER(irq_handler_dblflt, T_DBLFLT, 0)
TRAPHANDLER(irq_handler_tss, T_TSS, 0)
TRAPHANDLER(irq_handler_segnp, T_SEGNP, 0)
TRAPHANDLER(irq_handler_stack, T_STACK, 0)
TRAPHANDLER(irq_handler_gpflt, T_GPFLT, 0)
TRAPHANDLER(irq_handler_pgflt, T_PGFLT, 0)

TRAPHANDLER_NOEC(irq_handler_fperr, T_FPERR, 0)

TRAPHANDLER(irq_handler_align, T_ALIGN, 0)
TRAPHANDLER_NOEC(irq_handler_mchk, T_MCHK, 0)
TRAPHANDLER_NOEC(irq_handler_simderr, T_SIMDERR, 0)
TRAPHANDLER_NOEC(irq_handler_syscall, T_SYSCALL, 3)

TRAPHANDLER_NOEC(irq_handler_timer, IRQ_OFFSET + IRQ_TIMER, 0)
TRAPHANDLER_NOEC(irq_handler_kbd, IRQ_OFFSET + IRQ_KBD, 0)
TRAPHANDLER_NOEC(irq_handler_serial, IRQ_OFFSET + IRQ_SERIAL, 0)
TRAPHANDLER_NOEC(irq_handler_spurious, IRQ_OFFSET + IRQ_SPURIOUS, 0)
TRAPHANDLER_NOEC(irq_handler_ide, IRQ_OFFSET + IRQ_IDE, 0)

TRAPHANDLER_NOEC(irq_handler_default, T_DEFAULT, 0)

/** @endcond */
