#include "common/x86.h"
#include "common/mb.h"

#include "kernel/console.h"
#include "kernel/string.h"
#include "kernel/stdio.h"

#include "kernel/assert.h"

#include "kernel/mmu.h"
#include "kernel/trap.h"
#include "kernel/pmap.h"

#include "kernel/8259.h"
#include "kernel/8253.h"

#include "common/cga.h"
#include "kernel/draw.h"

void kmain(unsigned long magic, unsigned long addr);
const char version[] = "19991";

#define CHECK_FLAG(flags, bit)  ((flags) & (1 << (bit)))

static void
printMemMap(struct multiboot_info *mbi)
{
        struct mmap_entry *mmap; 
        unsigned long nr_entries = mbi->mmap_length / sizeof(struct mmap_entry); 
        unsigned e = 0;

        kprintf("E820 Memory map ");
        kprintf("(mmap_addr = 0x%x, mmap_length = 0x%x)",
                        (unsigned) mbi->mmap_addr, 
                        (unsigned) mbi->mmap_length);

        kprintf(", nr_entries = 0%x\n", nr_entries);

        mmap = (struct mmap_entry *) mbi->mmap_addr;
        for (; (unsigned long) mmap < 
                        mbi->mmap_addr + mbi->mmap_length; e++) {

                kprintf("\tsize = 0x%x, addr = 0x%x%x,"
                        " length = 0x%x%x, type = 0x%x\n",
                        (unsigned) e,
                        (unsigned long) mmap->base_addr_high,
                        (unsigned long) mmap->base_addr_low,
                        (unsigned long) mmap->length_high,
                        (unsigned long) mmap->length_low,
                        (unsigned long) mmap->type);

                mmap = (struct mmap_entry *)  
                        ((unsigned long) mmap + sizeof(struct mmap_entry));
        }
}


/**
 * @brief print drive information received over multiboot_info
 * @ingroup KERN
 */
static void 
printDriveInfo(struct multiboot_info *mbi)
{
        struct drive_info *di = (struct drive_info *) mbi->drives_addr;

        kprintf("Drive: 0x%x, ", di->drive_number);
        kprintf("mode: 0x%x, ", di->drive_mode);

        kprintf("C/H/S: ");
        kprintf("%d/%d/%d\n", di->drive_cylinders, di->drive_heads, 
                        di->drive_sectors);
#if 0
        kprintf("Size: %d bytes\n", di->total_sectors * SECTSIZE);
#endif
}

/**
 * @brief print multiboot information passed by ABOOT
 * @param mbi a pointer to a multiboot_info structure
 * @ingroup KERN
 */
static void 
print_mbi_info(struct multiboot_info *mbi)
{

        kprintf("flags = 0x%x\n", (unsigned) mbi->flags);

        /* mem_* valid? */
        if (CHECK_FLAG(mbi->flags, 0)) {
                kprintf("mem_lower = %uKB, mem_upper = %uKB\n",
                        (unsigned) mbi->mem_lower, 
                        (unsigned) mbi->mem_upper);
        }

        /* boot_device passed? */
        if (CHECK_FLAG(mbi->flags, 1)) {
                kprintf("boot_device = 0x%x\n", (unsigned) mbi->boot_device);
        }

        /* command line passed? */
        if (CHECK_FLAG(mbi->flags, 2)) {
                kprintf("cmdline = '%s'\n", (char *) mbi->cmdline);
        }

        /* memory map */
        if (CHECK_FLAG(mbi->flags, 6)) {
                printMemMap(mbi);
        }

        /* drive info */
        if (CHECK_FLAG(mbi->flags, 7)) {
                printDriveInfo(mbi);
        }
}

static
void kmain_draw(void)
{
        uint32_t i, j;

        fill_by_dot(0x00);

        delay_ms(50);

        for (i = 0; i <= CRT_COLS; i++) {
                draw_dot(i, 0, 0xff);
                delay_ms(2);
        }

        delay_ms(5);

        for (i = 0; i <= CRT_ROWS; i++) {
                draw_rect(0, 0, 80, i, 0xff);
                delay_ms(2);
        }

        delay_ms(5);

        for (i = CRT_ROWS; i > 0; i--) {
                draw_rect(0, 0, 80, i, 0x00);
                delay_ms(2);
        }

        delay_ms(5);
        fill_by_dot(0x00);

        draw_rect(0, 0, 80, 25, 0xff);
        delay_ms(10);

        draw_line(0, 0, CRT_COLS, CRT_ROWS, 0xbb);
        delay_ms(50);

        for (j = 0; j < 40; j++) {
                draw_line(0, 0, j, 25, 0xdd);
                delay_ms(5);
        }

        for (j = 4; j < 20; j += 5) {
                for (i = 4; i < CRT_COLS - 4; i++) {
                        draw_rect(0, 0, 80, 25, 0xff);
                        draw_circle(i, j, 3, 0xaa);
                        delay_ms(2);
                        fill_by_dot(0x00);
                }
        }

        kprintf("\n\n");
        cls();
}

/**
 * @brief entry point into the kernel
 * @param magic MULTIBOOT_VALID, passed by bootloader
 * @param addr phsyical address of multiboot_info, passed by bootloader
 *
 * This function is called from entry.S, right before passing on the stack the
 * magic header value and the physical address of a multiboot_info structure
 *
 * @ingroup KERN
 */
void
kmain(unsigned long magic, unsigned long addr)
{
        char *eversion = NULL;
	extern char edata[], end[];
        struct multiboot_info *mbi = (struct multiboot_info *) addr;

	/* 
	 * clear the uninitialized global data (BSS) section
	 * ensuring that all static/global variables start out zero. 
         */
	memset(edata, 0, end - edata);

	cons_init();

        /*
         * check multiboot 
         */
        if (magic != MULTIBOOT_VALID) {
                kprintf("Invalid magic number given: 0x%x\n", magic);
                goto halt;
        }

        kprintf("\nRunning... %x!\n", 4919);
        kprintf("Version: %ld\n", strtol(version, &eversion, 10));
        kprintf("Printing a float: %f\n", 433.233917);

        kprintf("Multiboot info:\n");
        print_mbi_info(mbi);

        /* initialize traps native */
        trap_init();

        /* initialize memory */
        mem_init(mbi);

        /* initialize PIT and PIC */
        pit_init(100);
        pic_init();


        /* enable interrupts */
        g_interrupts_enable();

        kmain_draw();

        mem_info();


        while (1)
                ; /* loop */

halt:
        native_hlt();
}
