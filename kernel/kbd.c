#include "common/x86.h"

#include "kernel/console.h"
#include "kernel/kbd.h"
#include "kernel/string.h"
#include "kernel/stdio.h"

#include "kernel/8259.h"

/**
 * @addtogroup KERNEL
 * @{
 */

static uint8_t shiftcode[256] = {
	[0x1D] = CTL,
	[0x2A] = SHIFT,
	[0x36] = SHIFT,
	[0x38] = ALT,
	[0x9D] = CTL,
	[0xB8] = ALT
};

static uint8_t togglecode[256] = {
	[0x3A] = CAPSLOCK,
	[0x45] = NUMLOCK,
	[0x46] = SCROLLLOCK
};

static uint8_t normalmap[256] = {
	NO,   0x1B, '1',  '2',  '3',  '4',  '5',  '6',
	'7',  '8',  '9',  '0',  '-',  '=',  '\b', '\t',
	'q',  'w',  'e',  'r',  't',  'y',  'u',  'i',
	'o',  'p',  '[',  ']',  '\n', NO,   'a',  's',
	'd',  'f',  'g',  'h',  'j',  'k',  'l',  ';',
	'\'', '`',  NO,   '\\', 'z',  'x',  'c',  'v',
	'b',  'n',  'm',  ',',  '.',  '/',  NO,   '*',
	NO,   ' ',  NO,   NO,   NO,   NO,   NO,   NO,
	NO,   NO,   NO,   NO,   NO,   NO,   NO,   '7',
	'8',  '9',  '-',  '4',  '5',  '6',  '+',  '1',
	'2',  '3',  '0',  '.',  NO,   NO,   NO,   NO,
	[0xC7] = KEY_HOME,      [0x9C] = '\n',
	[0xB5] = '/',           [0xC8] = KEY_UP,
	[0xC9] = KEY_PGUP,	[0xCB] = KEY_LF,
	[0xCD] = KEY_RT,	[0xCF] = KEY_END,
	[0xD0] = KEY_DN,	[0xD1] = KEY_PGDN,
	[0xD2] = KEY_INS,	[0xD3] = KEY_DEL
};

static uint8_t shiftmap[256] = {
	NO,   033,  '!',  '@',  '#',  '$',  '%',  '^',
	'&',  '*',  '(',  ')',  '_',  '+',  '\b', '\t',
	'Q',  'W',  'E',  'R',  'T',  'Y',  'U',  'I',
	'O',  'P',  '{',  '}',  '\n', NO,   'A',  'S',
	'D',  'F',  'G',  'H',  'J',  'K',  'L',  ':',
	'"',  '~',  NO,   '|',  'Z',  'X',  'C',  'V',
	'B',  'N',  'M',  '<',  '>',  '?',  NO,   '*',
	NO,   ' ',  NO,   NO,   NO,   NO,   NO,   NO,
	NO,   NO,   NO,   NO,   NO,   NO,   NO,   '7',
	'8',  '9',  '-',  '4',  '5',  '6',  '+',  '1',
	'2',  '3',  '0',  '.',  NO,   NO,   NO,   NO,
	[0xC7] = KEY_HOME,	[0x9C] = '\n',
	[0xB5] = '/',           [0xC8] = KEY_UP,
	[0xC9] = KEY_PGUP,	[0xCB] = KEY_LF,
	[0xCD] = KEY_RT,	[0xCF] = KEY_END,
	[0xD0] = KEY_DN,	[0xD1] = KEY_PGDN,
	[0xD2] = KEY_INS,	[0xD3] = KEY_DEL
};

#define C(x) (x - '@')

static uint8_t ctlmap[256] = {
	NO,      NO,      NO,      NO,      NO,      NO,      NO,      NO, 
	NO,      NO,      NO,      NO,      NO,      NO,      NO,      NO, 
	C('Q'),  C('W'),  C('E'),  C('R'),  C('T'),  C('Y'),  C('U'),  C('I'),
	C('O'),  C('P'),  NO,      NO,      '\r',    NO,      C('A'),  C('S'),
	C('D'),  C('F'),  C('G'),  C('H'),  C('J'),  C('K'),  C('L'),  NO, 
	NO,      NO,      NO,      C('\\'), C('Z'),  C('X'),  C('C'),  C('V'),
	C('B'),  C('N'),  C('M'),  NO,      NO,      C('/'),  NO,      NO,
	[0x97] = KEY_HOME,
	[0xb5] = C('/'),		[0xc8] = KEY_UP,
	[0xc9] = KEY_PGUP,		[0xcb] = KEY_LF,
	[0xcd] = KEY_RT,		[0xcf] = KEY_END,
	[0xd0] = KEY_DN,		[0xd1] = KEY_PGDN,
	[0xd2] = KEY_INS,		[0xd3] = KEY_DEL
};

static uint8_t *charcode[4] = {
	normalmap,
	shiftmap,
	ctlmap,
	ctlmap
};

/**
 * @brief Get data from the keyboard.  
 * @retval If we finish a character, return it.  Else 0.
 * @retval -1 if no data.
 */
static int
kbd_proc_data(void)
{
	int c;
	uint8_t data;
	static uint32_t shift;

	if ((inb(KBSTATP) & KBS_DIB) == 0) {
		return -1;
        }

	data = inb(KBDATAP);

	if (data == 0xE0) {
		/* E0 escape character */
		shift |= E0ESC;
		return 0;
	} else if (data & 0x80) {
		/* Key released */
                if (!(shift & E0ESC)) {
                        data = data & 0x7f;
                }
		shift &= ~(shiftcode[data] | E0ESC);
		return 0;
	} else if (shift & E0ESC) {
		/* Last character was an E0 escape; or with 0x80 */
		data |= 0x80;
		shift &= ~E0ESC;
	}

	shift |= shiftcode[data];
	shift ^= togglecode[data];

	c = charcode[shift & (CTL | SHIFT)][data];
	if (shift & CAPSLOCK) {
		if ('a' <= c && c <= 'z') {
			c += 'A' - 'a';
                } else if ('A' <= c && c <= 'Z') {
			c += 'a' - 'A';
                }
	}

	/* Process special keys: Ctrl-Alt-Del, reboot */
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
		kprintf("Rebooting!\n");
		outb(0x92, 0x3);
	}

	return c;
}

void
kbd_intr(void)
{
	cons_intr(kbd_proc_data);
}

void
kbd_init(void)
{
	/* Drain the kbd buffer so that Bochs generates interrupts. */
	kbd_intr();
	irq_setmask_8259A(irq_mask_8259A & ~(1 << 1));
}

/**
 * @}
 */
