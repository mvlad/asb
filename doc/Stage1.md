Stage 1 (boot0)
===============

BIOS reads the first 512 bytes of HDD (this is where **boot0** resides),
it gets loaded at 0x7c00 and uses `read_sect()` 
to load at 0x8000 (physical address) the second stage bootloader: **boot**.
It does so by reading from second sector (where **boot** resides) from the HDD,
in the amount of 40 sectors (`(512 * 40) = 20`kB).

Because the partitions start at `0x100000` (1MiB) there's plenty 
of room to store there the second stage boot loader.

`read_sect()` (method used to read the HDD) uses INT 13 function `C\H\S` mode, 
with default disk geometry of 16 sectors per track
240 heads and it assumes that the drive is the first HDD in the system.

The first stage is running *entirely* in 16-bit real mode.

![MBR](http://www.ibm.com/developerworks/library/l-linuxboot/fig2.gif)


Resources
---------

* [Linuxboot](http://www.ibm.com/developerworks/library/l-linuxboot/index.html)

* [Master Boot Record](http://technet.microsoft.com/en-us/library/cc976786.aspx)
