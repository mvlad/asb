Stage 2 (boot)
==============

The main idea with a second stage boot loader is that once we're running
in protected mode it makes no sense to jump back-and-forth to real-mode to do
any kind of BIOS invocations, thus once we get all required information, we store
them, jump to protected mode, load the kernel and pass the (stored information)
necessary information to the kernel.

*E820 memory map*

[E820 memory map](http://www.uruk.org/orig-grub/mem64mb.html) is saved into `e820_mem_map`, and the
number of memory entries in the map into `e820_nr_entries` with with the help of `get_mem_size()` function.

*Disk geometry*

* disk geometry is retrieved by `get_dap_disk_geom()` to 
`DAP` (necessary for storing `total_sectors`)
* `get_disk_geom()` for CHS (Cyls/Heads/Sectors-Per-Track)
information are being saved into `heads`, `cyls`, and `sectors` global
variables.

Additionally, we want to be able to boot the kernel of a **EXT2** partition, being
it easier to deploy the kernel in this way using *e2tools*, and its associated 
utilities.

## Switching to protected mode

Before jumping into protected mode, **boot** performs the following actions:

* calls `enable_a20()`, to enable accessing physical memory addresses
over 1MiB
* retrieves the memory map and drive geometry
* installs a fake [GDT table](http://wiki.osdev.org/GDT_Tutorial), required to go into protected mode
* sets up a stack
* calls `bmain()` (entry in C code)

So far, everything is done in assembly, thus after setting up a stack, 
the second stage boot loader jumps to C code at `bmain()`.

The C code is responsible for two things:

- Read the kernel of a EXT2 partition, thus being able to [interpret the 
partition boot](#partition-table) table and, and, as we're running already in 32-bit, 
to make uses of the ATA LBA28 procedure to load the kernel.
- Besides file-system handling to sanitize the stored data/information retrieved
prior to jump to protected mode, it passes a `multiboot_info`
data structure (as a mean of communicating) to the kernel.

## Mounting EXT2 partition

While in protected mode:

* read the **MBR** (for reading the partition boot table), to find a 
**EXT2** partition (assuming that's where the kernel resides)
* if the partition is a **EXT2** partition (and it was the intended partition
`DEST_PARTITION`) performs the following actions:
    - mounts the partition, by populating the [SUPERBLOCK](http://www.nongnu.org/ext2-doc/ext2.html#DEF-SUPERBLOCK) 
    - tries to find the kernel on the partition using `ext2fs_dir()`
    - loads the kernel into memory
    - it passes `multiboot_info` and `MULTIBOOT_VALID`
    to the kernel
    - finally it jump to its `entry_func()`

A disk diagram with 3 primary partitions and an extended one is shown below:

![Basic Disk](http://i.technet.microsoft.com/dynimg/IC212673.gif)

For all disk operations we use [LBA28 - ATA PIO mode](http://wiki.osdev.org/ATA_PIO_Mode),
specifically `readsect()`  function, a function which uses `inb/outb`
so there is no need for switching back-and-forth to real mode.

## Loading the kernel

Before jumping to the kernel entry point (`entry_func()`), it passes the
memory map and drive geometry (retrieved and stored prior to switching to protected
mode) to the kernel using `multiboot_header` procedure, in
the following way:

* the contents of `%eax` register store the magic header as physical address
* the contents of `%ebx` hold `multiboot_info` as physical address

**Methods**

* `disk_open()`, `disk_read()`, `disk_close()` and `disk_seek()` are the main functions, 
which load the kernel
* ELF validation/loading takes place with:
    * `elf_valid()` checks if the image provided is a valid ELF image
    * `elf_load()`, uses `disk_read()` to load `PT_LOAD` sections at 
    physical address `Proghdr::p_pa`

Kernel ELF Program headers

    Program Headers:
    Type           Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align
    LOAD           0x001000 0xc0100000 0x00100000 0x1b4ab 0x1b4ab R E 0x1000
                                       ^^^^^^^^^^
                                        address gets loaded
    LOAD           0x01d000 0xc011c000 0x0011c000 0x0b4a4 0x1e008 RW 0x1000
                                       ^^^^^^^^^^
                                        address gets loaded

## Reading the partition table

-  `do_open_partition()`  checks if the partition is 
a **EXT2** partition and if this is indeed the destination partition we want to
open. It should match the destination partition we put the kernel on (see
`PART_SCHEME` in main makefile) with the help of `e2cp` (see
`kernel/Makefile`). 

**Partition table entry format**

* BYTE = 1 byte
* WORD = 2 bytes
* LONG = 4 bytes 

|off         |size      | type                                            |
|----------- |----------| ----------------------------------------------- |
|`0x00`      |1 byte    | boot indicator (0x80 = active, 0x00 = inactive) |
|`0x01`      |1 byte    | start head                                      |
|`0x02`      |2 bytes   | start cylinder, sector                          |
|`0x04`      |1 byte    | system type (0x83 = Linux, 0xA6 = OpenBSD)      |
|`0x05`      |1 byte    | end head                                        |
|`0x06`      |2 bytes   | end cylinder, sector                            |
|`0x08`      |4 bytes   | start LBA sector                                |
|`0x0C`      |4 bytes   | number of sectors in partition                  |

In the case of a partition that extends beyond the 8GB boundary,
the LBA values will be correct, the CHS values will have their
maximums (typically `(C,H,S) = (1023,255,63)`).

Each partition is `16 bytes * 4 = 64` bytes (`0x40`) in size.

## Passing multiboot information to the kernel ##

The `multiboot_info` data structure looks in the following way:

            +-------------------+
    0       | flags             |    (required)
            +-------------------+
    4       | mem_lower         |    (present if flags[0] is set)
    8       | mem_upper         |    (present if flags[0] is set)
            +-------------------+
    12      | boot_device       |    (present if flags[1] is set)
            +-------------------+
    16      | cmdline           |    (present if flags[2] is set)
            +-------------------+
    20      | mods_count        |    (present if flags[3] is set)
    24      | mods_addr         |    (present if flags[3] is set)
            +-------------------+
    28 - 40 | syms              |    (present if flags[4] or
            |                   |                flags[5] is set)
            +-------------------+
    44      | mmap_length       |    (present if flags[6] is set)
    48      | mmap_addr         |    (present if flags[6] is set)
            +-------------------+
    52      | drives_length     |    (present if flags[7] is set)
    56      | drives_addr       |    (present if flags[7] is set)
            +-------------------+
    60      | config_table      |    (present if flags[8] is set)
            +-------------------+
    64      | boot_loader_name  |    (present if flags[9] is set)
            +-------------------+
    68      | apm_table         |    (present if flags[10] is set)
            +-------------------+
    72      | vbe_control_info  |    (present if flags[11] is set)
    76      | vbe_mode_info     |
    80      | vbe_mode          |
    82      | vbe_interface_seg |
    84      | vbe_interface_off |
    86      | vbe_interface_len |
            +-------------------+

Before jumping to the kernel entry the following takes place:

- `save_drive_info()` saves the drive geometry into drive_info
data structure. 
- `save_multiboot_info()` -- saves flags, `bltdr_name`, cmdline, 
E820 memory map and calls `save_drive_info()`
- `multiboot_info` data structure is passed, together  with `MULTIBOOT_VALID`
thru registers:


        __asm __volatile("movl %0, %%ebx" : : "r" (&mbi));
        __asm __volatile("movl %0, %%eax" : : "r" (MULTIBOOT_VALID));

## Resources

* [Ralf Brown's interrupt list](http://www.cs.cmu.edu/~ralf/files.html)

* [Memory layout info](http://stanislavs.org/helppc/bios_data_area.html)

* [Old PNP BIOS spec](ftp://download.intel.com/support/motherboards/desktop/sb/pnpbiosspecificationv10a.pdf)

* [T13 BIOS Enhanced Disk Drive (drafts)](http://www.t10.org/t13/#Project_drafts)

