#include "boot/errno.h"
#include "boot/console.h"

extern errno_t errnum;

char *err_list[] = {
        [ERR_NONE] = 0,
        [ERR_BAD_FILENAME] = "Filename must be either an absolute pathname or blocklist",
        [ERR_BAD_FILETYPE] = "Bad file or directory type",
        [ERR_BAD_PART_TABLE] = "Partition table invalid or corrupt",
        [ERR_BELOW_1MB] = "Loading below 1MB is not supported",
        [ERR_BOOT_FAILURE] = "Unknown boot failure",
        [ERR_EXEC_FORMAT] = "Invalid or unsupported executable format",
        [ERR_FILELENGTH] = "Filesystem compatibility error, cannot read whole file",
        [ERR_FILE_NOT_FOUND] = "File not found",
        [ERR_FSYS_CORRUPT] = "Inconsistent filesystem structure",
        [ERR_FSYS_MOUNT] = "Cannot mount selected partition",
        [ERR_NO_DISK] = "Selected disk does not exist",
        [ERR_NO_PART] = "No such partition",
        [ERR_OUTSIDE_PART] = "Attempt to access block outside partition",
        [ERR_READ] = "Disk read error",
        [ERR_WONT_FIT] = "Selected item cannot fit into memory",
};


void 
print_error(void)
{
        if (errnum > ERR_NONE && errnum < MAX_ERR_NUM) {
                bprintf(">> Error %u: %s\n", errnum, err_list[errnum]);
        }
}
