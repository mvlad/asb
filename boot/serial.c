#include "common/x86.h"
#include "boot/serial.h"

void
serial_putc(int c)
{
	int i;
	
	for (i = 0; !data_avail() && i < 12800; i++)
		delay();
	
	outb(USART1 + USART_TX, c);
}

void
serial_init(void)
{
	/* Turn off the FIFO */
	outb(USART1 + USART_FCR, 0);
	
	/* Set speed; requires DLAB latch */
	outb(USART1 + USART_LCR, USART_LCR_DLAB);
	outb(USART1 + USART_DLL, (uint8_t) (115200 / 9600));
	outb(USART1 + USART_DLM, 0);

	/* 8 data bits, 1 stop bit, parity off; turn off DLAB latch */
	outb(USART1 + USART_LCR, USART_LCR_WLEN8 & ~USART_LCR_DLAB);

	/* No modem controls */
	outb(USART1 + USART_MCR, 0);
	/* Enable rcv interrupts */
	outb(USART1 + USART_IER, USART_IER_RDI);

	/* Clear any pre-existing overrun indications and interrupts */
	(void) inb(USART1 + USART_IIR);
	(void) inb(USART1 + USART_RX);
}
