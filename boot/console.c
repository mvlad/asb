#include "common/x86.h"

#include "boot/console.h"
#include "boot/serial.h"
#include "boot/cga.h"

/**
 * @brief Convert the integer d as a string and save the string in 'buf'
 * @param buf where to save the string
 * @param base if equal with 'd' interpret as decimal, if 'x' interpret as 
 * hexadecimal
 * @param d the integer to convert
 */
static void 
itoa(char *buf, int base, int d)
{
        char *p = buf;
        char *p1, *p2;
        unsigned long ud = d;
        int divisor = 10;

        /* 
         * If %d is specified and D is minus, 
         * put `-' in the head. 
         */
        if (base == 'd' && d < 0) {
                *p++ = '-';
                buf++;
                ud = -d;
        } else if (base == 'x') {
                divisor = 16;
        }

        /* Divide UD by DIVISOR until UD == 0. */
        do {
                int remainder = ud % divisor;
                if (remainder < 10) {
                        *p++ = remainder + '0';
                } else {
                        *p++ = remainder + 'a' - 10;
                }

        } while (ud /= divisor);

        /* Terminate BUF. */
        *p = 0;

        /* Reverse BUF. */
        p1 = buf;
        p2 = p - 1;

        while (p1 < p2) {
                char tmp = *p1;

                *p1 = *p2;
                *p2 = tmp;

                p1++; p2--;
        }
}

void 
bprintf(const char *format, ...)
{
        char **arg = (char **) &format;
        int c;
        char buf[20];

        arg++;

        while ((c = *format++) != 0) {

                if (c != '%') {
                        cga_putc(c);
                        serial_putc(c);
                } else {
                        char *p;

                        c = *format++;
                        switch (c) {
                                case 'd':
                                case 'u':
                                case 'x':
                                        itoa(buf, c, *((int *) arg++));
                                        p = buf;
                                        goto string;
                                        break;
                                case 's':
                                        p = *arg++;
                                        if (!p) {
                                                p = "(null)";
                                        }
string:
                                        while (*p) {
                                                cga_putc(*p);
                                                serial_putc(*p);
                                                *p++;
                                        }
                                        break;
                                default:
                                        cga_putc(*((int *) arg));
                                        serial_putc(*((int *) arg));
                                        arg++;
                                        break;
                        }
                }
        }
}

void 
cons_init(void)
{
        cga_init();
        serial_init();
}
