/** @cond DOXYGEN_IGNORE_ASM */

.file "mem.S"

.code16
.type get_mem_size, @function
get_mem_size:
        xorl    %ebx, %ebx

        /* es:di (Address Range Descriptor Structure) */
        movw    $e820_mem_map, %di

mem_check_loop:
        movw    $0xe820, %ax
        movw    $0x14, %cx
        movl    $0x534D4150, %edx

        int     $0x15
        jc      mem_check_fail

        addw    $0x14, %di

        /* increment no of entries */
        incw    e820_nr_entries

        cmp     $0x0, %ebx
        jne     mem_check_loop
        jmp     mem_check_ok

mem_check_fail:
        movw    $0x0, e820_nr_entries
mem_check_ok:
        ret

/*
 *        .code32
 *DispMemInfo:
 *
 *        pushl   %ebp
 *        movl    %esp, %ebp
 *        subl    $0x12, %esp
 *
 *        pushl    %esi
 *        pushl    %edi
 *        pushl    %ecx
 *
 *        movl    $MemChkBuf, %esi
 *        movl    dwMCRNumber, %ecx
 *MyLoop:
 *        movl    $0x5, %edx
 *        movl    $ARDStruct, %edi
 *1:
 *        movl    (%esi), %eax
 *        stosl
 *
 *        addl    $0x4, %esi
 *        dec     %edx
 *
 *        cmp     $0x0, %edx
 *        jnz     1b
 *
 *        cmp     $0x1, dwType
 *        jne     2f
 *
 *        movl    dwBaseAddrLow, %eax
 *        addl    dwLengthLow, %eax
 *
 *        cmpl    dwMemSize, %eax
 *        jb      2f
 *        movl    %eax, dwMemSize
 *
 *2:
 *        loop    MyLoop
 *
 *        pushl   dwMemSize
 *        pushl   $szRAMSize
 *        pushl   $fmt
 *
 *        call    printf
 *        addl    $0x12, %esp
 *
 *
 *        popl    %ecx
 *        popl    %edi
 *        popl    %esi
 *
 *        movl    %ebp, %esp
 *        popl    %ebp
 *        ret
 */

/** @endcond */
