/** @cond DOXYGEN_IGNORE_ASM */

/* Assemble for 16-bit mode */
.code16

.set PROTSTACK, 0x67ff0
.set TSTACK, 0x8000 - 0x10

.set GD_KT, 0x08
.set GD_DT, 0x10

.text
.globl start
start:
        ljmp $0, $go
go:
        /* clean-up regs */

        xorw    %ax, %ax                /* Segment number zero */

        movw    %ax, %ds                /* Data Segment */
        movw    %ax, %es                /* Extra Segment */
        movw    %ax, %ss                /* Stack Segment */
        movw    %ax, %sp                /* Stack pointer */

        /* set up a temporary stack */
        movw    $TSTACK, %ax
        addw    $0x220, %ax

        cli
        movw    %ax, %ss
        movw    $TSTACK, %sp
        sti

        /* we are in boot */
        movl    $msg, %esi
        call    puts

        jmp     bootup


.include "boot/bios.S"          /* BIOS convenience functions */
.include "boot/geom.S"          /* disk query functions */
.include "boot/mem.S"           /* SMAP memory query */

bootup:
        /* disable interrupts */
        cli

        /* Memory > 1MiB wraps around */
        call    enable_a20

        /* more debug messages */
        movl    $msg2, %esi
        call    puts

        /* 
         * we get all necessary information and then avoid doing any BIOS calls
         */

        /* get total_sectors */
        call    get_dap_disk_geom
        /* get disk geometry CHS */
        call    get_disk_geom
        /* get memory size */
        call    get_mem_size

        /* before switching to protected mode */
        movl    $msg3, %esi
        call    puts

        /* reset video, so we see what's going on */
        movb $0x00, %ah
        movb $0x03, %al
        int $0x10       /* call BIOS */

        /*
         * Switch from real to protected mode, using a bootstrap GDT
         * and segment translation that makes virtual addresses 
         * identical to their physical addresses, so that the 
         * effective memory map does not change during the switch.
         */
        lgdt    gdtdesc
        movl    %cr0, %eax
        orl     $0x1, %eax
        movl    %eax, %cr0

        /*
         * Jump to next instruction, but in 32-bit code segment.
         * Switches processor into 32-bit mode.
         */
        ljmp    $GD_KT, $protcseg

.code32
protcseg:
        /* Set up the protected-mode data segment registers */
        movw    $GD_DT, %ax             /* data segment selector */

        movw    %ax, %ds                /* DS: Data Segment */
        movw    %ax, %es                /* ES: Extra Segment */

        movw    %ax, %fs                /* FS */
        movw    %ax, %gs                /* GS */

        movw    %ax, %ss                /* SS: Stack Segment */

        /* Set up the stack/base pointer and call into C. */
        movl    $PROTSTACK, %esp
        movl    $PROTSTACK, %ebp

        /* entry point to C */
        call    bmain
        hlt

/*
* This is the Global Descriptor Table
*
*  An entry, a "Segment Descriptor", looks like this:
*
* 31          24         19   16                 7           0
* ------------------------------------------------------------
* |             | |B| |A|       | |   |1|0|E|W|A|            |
* | BASE 31..24 |G|/|0|V| LIMIT |P|DPL|  TYPE   | BASE 23:16 |
* |             | |D| |L| 19..16| |   |1|1|C|R|A|            |
* ------------------------------------------------------------
* |                             |                            |
* |        BASE 15..0           |       LIMIT 15..0          |
* |                             |                            |
* ------------------------------------------------------------
*
*  Note the ordering of the data items is reversed from the above
*  description.
*/

.p2align        2       /* force 4-byte alignment */

gdt:
        /* null segment */
        .word   0, 0
        .byte   0, 0, 0, 0

        /* code segment */
        .word   0xFFFF, 0
        .byte   0, 0x9A, 0xCF, 0

        /* data segment */
        .word   0xFFFF, 0
        .byte   0, 0x92, 0xCF, 0

        /* 16 bit real mode CS */
        .word   0xFFFF, 0
        .byte   0, 0x9E, 0, 0

        /* 16 bit real mode DS */
        .word   0xFFFF, 0
        .byte   0, 0x92, 0, 0


/* this is the GDT descriptor */
gdtdesc:
        .word   0x27            /* limit */
        .long   gdt             /* addr */
msg:
        .asciz "2"
msg2:
        .asciz "3"
msg3:
        .asciz "Switching...\r\n"

/* data section */
.data
.p2align                2

/* there are global vars for retrieving and later on, passed to the kernel */
.globl heads
heads:                  .long   0
.globl cyls
cyls:                   .long   0
.globl sectors
sectors:                .long   0

/* 
 * see struct drive_parameters, sizeof(struct drive_parameters), used
 * to retrieve total sectors of the hard-drive
 */
.globl DAP
DAP:                    .space 30

/* the same for for retrieving SMAP memory */

/* see struct mmap_entry */
.globl e820_mem_map
e820_mem_map:           .space   512

/* the number of mmap entries */
.globl e820_nr_entries
e820_nr_entries:        .long 0

/** @endcond */
