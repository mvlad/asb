#include "common/x86.h"

#include "boot/disk.h"
#include "boot/mbr.h"
#include "boot/console.h"
#include "boot/string.h"
#include "boot/ext2.h"
#include "boot/errno.h"
#include "boot/boot.h"

/* configuration vars */
#include "boot/config.h"

/* err nos */
extern errno_t errnum;

/** holds current open partition */
struct curr_open_part curr_part;

/** tmp disk buffer parameters */
int tmp_track;

/** tmp disk geometry */
struct geometry disk_geom;

/**
 * @brief sanity checks for drive
 * @retval 1 in case of success, 0 otherwise
 *
 * @note network and cdrom booting is not unsupported
 */
static int
is_sane_drive(int drive)
{
        if (!DRIVE_IS_DISK(drive))
                goto out;
        if (DRIVE_IS_NETWORK(drive)) 
                goto out;
        if (DRIVE_MAYBE_CDROM(drive))
                goto out;

        return 1;

out:
        bprintf("Not a sane drive\n");
        return 0;
}

/**
 * @brief sanity checks for partition
 * @retval 1 in case of success, 0 otherwise
 */
static int 
is_sane_partition(int part)
{
        if (!PART_CHECK_IS_VALID(part))
                return 0;

        /* if the partition is the whole disk */
        if (part == 0xffffff)
                return 0;

        return 1;
}

/**
 * @brief determine if 'part' is the partition we want to open
 * @retval 1 in case 'part' is an EXT2 partition and the partition
 * we want to open, 0 otherwise
 */
static inline int
intended_part(struct curr_open_part *open_part, unsigned long dst_part)
{
        if (SLICE_IS_EXT2(open_part->type) && open_part->part == dst_part) {
                return 1;
        }
        return 0;
}

/**
 * @brief save information gathered by @link get_disk_geom @endlink
 * and @link get_dap_disk_geom @endlink into 'geom'
 * @param geom a pointer to a struct geometry
 * @retval 1 in case of success, 0 otherwise
 *
 * 
 * @note
 * @link cyls @endlink, @link heads @endlink, @link sectors @endlink are
 * retrieved by @link get_disk_geom @endlink, while @link DAP @endlink 
 * is gathered using @link get_disk_geom @endlink
 */
static int
get_disk_info(struct geometry *geom)
{
        struct drive_parameters *dp = &DAP;
        if (!dp) {
                return 0;
        }

        geom->cylinders = cyls;
        geom->heads = heads;
        geom->sectors = sectors;

        geom->sector_size = SECTSIZE;
        geom->flags = BIOSDISK_FLAG_LBA_EXTENSION;
        geom->total_sectors = dp->total_sectors;

        return 1;
}


/**
 * @brief check if a partition type is valid
 * @param slice_no the partition
 * @param type the type of the partition
 * @retval 0 if fails, 1 otherwise
 */
static int
is_slice_valid(int slice_no, int type)
{
        if (slice_no < SLICE_MAX)
                return 1;

        if (!IS_SLICE_TYPE_EXTENDED(type) && IS_SLICE_VALID(type))
                return 1;

        return 0;
}


/**
 * @brief initialize current open partition with default values
 * @param part a pointer to a struct curr_open_part
 *
 * @note this function should be called before iterating over all
 * partitions available (i.e., do_next_part())
 */
static void
init_current_part(struct curr_open_part *part)
{
        /* initialize default vars */
        part->entry = -1;

        part->type = 0;

        part->start = 0;
        part->len = GEOM_T_SECTORS(disk_geom);

        /* must be initialized to 0xffffff */
        part->part = 0xffffff;
}

/**  
 * @brief function used to iterate over partition table stored in MBR
 * @param next_part a pointer to a struct curr_open_part
 * @param buf the buffer where to store the MBR (512 bytes in size)
 * @retval 0 if fails, 1 otherwise
 *
 * @note the partition representation will be stored in 'next_part.part', the
 * partition type in 'next_part.type', the start sector in 'next_part.start' and
 * the length in 'next_part.len'.  The entry number in the partition table in
 * will be stored in 'next_part.entry'.
 *
 * @note init_current_part() must be called first before this function.
 */

static int 
do_next_part(struct curr_open_part *next_part, char *buf)
{
        int slice_no = GET_SLICE_FROM_PART(next_part->part);

        /* If this is the first time...  */
        if (slice_no == 0xff) {
                bprintf("Reseting slice no to 0x%x\n", slice_no);
                slice_no = -1;
        }

        /* store the MBR into buf */
        if (!read_mbr(buf)) {
                return 0;
        }

        /* advance the entry number */
        next_part->entry++;

        if (next_part->entry == SLICE_MAX) {
                bprintf("Maximum slice reached\n");
                bprintf("No primary partition found!\n");
                errnum = ERR_NO_PART;
                return 0;
        }
    
        /* now retrive from buf the info */
        next_part->type = SLICE_TYPE(buf, next_part->entry);
        next_part->start =+ SLICE_START(buf, next_part->entry);
        next_part->len = SLICE_LENGTH(buf, next_part->entry);

        /* check valid slice */
        if (is_slice_valid(slice_no, next_part->type)) {
                slice_no++;
        }

        /* adjust the partition */
        next_part->part = GET_PART_FROM_SLICE(slice_no);

        bprintf("Entry: %d, slice_no: 0x%x, ", next_part->entry, slice_no);
        bprintf("partition: 0x%x, type: 0x%x, start: 0x%x, len: 0x%x\n",
                next_part->part, next_part->type, 
                next_part->start, next_part->len);

        return 1;
}

/* 
 * @brief try to open a partition
 * @param dst_part the partition to open
 * @retval 1 if a valid partition has been found, 0 otherwise
 */
static int
do_open_partition(unsigned long dst_part)
{
        char buf[SECTSIZE];

        tmp_track = -1;

        /* Initialize current partition */
        init_current_part(&curr_part);

        while (do_next_part(&curr_part, buf)) {
                bprintf("Partition has %d sectors (%d bytes)\n", 
                        curr_part.len, curr_part.len * SECTSIZE);
                /* is this is a valid partition, and is the dest partition. */
                if (curr_part.type && intended_part(&curr_part, dst_part)) {
                        bprintf("Found EXT2 partition!\n");
                        return 1;
                }
        }

        return 0;
}

/**
 * @brief find an EXT2 partition and attempt to mount it
 * @param open_part the partition we know that the kernel resides
 * @param dst_part the partition we know that the kernel resides
 * @retval 1 in case the partition is found, 0 otherwise
 *
 * When this function finishes, the SUPERBLOCK resides in 
 * @link SUPER_BUF @endlink
 */
static int
disk_mount(struct curr_open_part *open_part, unsigned long dst_part)
{
        int size_sblk = sizeof(struct ext2_super_block);

        if (!get_disk_info(&disk_geom)) {
                bprintf("Failed to retrieve disk info\n");
                errnum = ERR_NO_DISK;
                return 0;
        }

        if (!do_open_partition(dst_part)) {
                bprintf("Could not open %d partition\n", (dst_part & 0xff000) >> 16);
                return 0;
        }

        if (open_part->len < (unsigned) (SBLOCK + size_sblk / DEV_BSIZE)) {
                bprintf("Invalid partition length\n");
                errnum = ERR_FSYS_CORRUPT;
                return 0;
        }

        bprintf("Partition opened. Attempting to mount...\n");

        if (!ext2_mount()) {
                bprintf("Mount failed!\n");
                errnum = ERR_FSYS_MOUNT;
                return 0;
        }

        return 1;
}

/**
 * @brief mounts the disk and performs sanity checks
 * @param boot_drive the disk we're booting from
 * @param dst_part partition that we want to mount/open, which 
 * holds the kernel
 *
 * @note part is used to initialize curr_part global variable 
 * @retval 1 in case of success, 0 otherwise
 *
 */
static int
disk_setup(int boot_drive, int dst_part)
{
        /* first disk */
        if (!is_sane_drive(boot_drive))
                return 0;

        if (!disk_mount(&curr_part, dst_part))
                return 0;

        if (!is_sane_partition(curr_part.part))
                return 0;

        return 1;
}

/**
 * @brief basic routine to read the disk using IDE PIO mode
 *
 * How to poll (waiting for the drive to be ready to transfer data): 
 * Read the Regular Status port until bit 7 (BSY, value = 0x80) clears, 
 * and bit 3 (DRQ, * value = 8) sets -- or until bit 0 (ERR, value = 1) 
 * or bit 5 (DF, value = 0x20) sets. 
 *
 * If neither error bit is set, the device is ready right then.
 *
 * @param dst where to dump the data read
 * @param lba a logical block address 
 */
static void
readsect(int dst, uint32_t lba)
{
        dprintf("<dst: 0x%x, lba: 0x%x>\n", dst, lba);

        /* wait for disk reaady */
        while ((inb(0x1f7) & 0xc0) != 0x40)
                /* do nothing */;

        outb(0x1f1, 0x00);

        /* sectors count */
        outb(0x1f2, 1);

        /* LBA LOW */
        outb(0x1f3, lba);

        /* LBA Mid */
        outb(0x1f4, lba >> 8);

        /* LBA High */
        outb(0x1f5, lba >> 16);

        /* drive select */
        outb(0x1f6, (lba >> 24) | 0xe0);

        /* cmd 0x20 - read sectors */
        outb(0x1f7, 0x20);	                

        /* wait for disk to be ready */
        while ((inb(0x1f7) & 0xc0) != 0x40)
                /* do nothing */;

        /*
         * how many words to read: a sector
         *
         * dividing by 4 => 128 32-bit ints (128 * 4 = 512 bytes)
         */
        insl(0x1f0, (uint8_t *) dst, SECTSIZE / 4);
}


/**
 * @brief low level read function
 * @param geom stored drive geometry
 * @param sect from which sector to read
 * @param nsec number of sectors
 * @param seg where to write
 *
 * @note 'seg' gets modified
 */
static void 
do_rawread(struct geometry *geom, int sect, int nsec, int seg)
{
        unsigned int lba;

        do {
                /* CHS to LBA conversion */
                lba = sect_to_lba(sect, geom);

                readsect(seg, lba);

                /* decrease the number of sectors */
                nsec--;

                /* advance the segment with one sector */
                seg += SECTSIZE;

                /* and the sector itself */
                sect++;

        } while (nsec > 0 && sect > 0);
}

/**
 * @brief raw read
 * @param sect the sector from where to read
 * @param byte_off offset in bytes
 * @param byte_len length in bytes
 * @param buf where to save the contents
 * @retval 0 in case of failure, 1 otherwise
 *
 * rawread uses @link BUFFADDR @endlink to save temporary data
 * and then it moves it to @param buf
 */
static int
rawread(unsigned long sect, int byte_off, int byte_len, char *buf)
{
        int slen, sect_per_track;

        if (byte_len <= 0) {
                bprintf("Invalid no of bytes given!\n");
                return 0;
        }

        while (byte_len > 0 && !errnum) {

                char *buffaddr;
                int soff, num_sect, track;
                int sect_bits, size = byte_len;

                if (!VALID_SECT(sect, disk_geom)) {
                        return 0;
                }

                slen = get_sect_len(byte_off, byte_len, 
                                GEOM_SECT_SIZE(disk_geom));

                sect_bits = GEOM_SECT_SIZE_BITS(disk_geom);

                /* eliminate buff overflow */
                if ((GEOM_SECTORS(disk_geom) << sect_bits) > BUFFLEN) {
                        sect_per_track = (BUFFLEN >> sect_bits);
                } else {
                        sect_per_track = GEOM_SECTORS(disk_geom);
                }

                /* 
                 * we're passing a sector number but we want a offset in the
                 * track (normally being 63 sectors per track)
                 */
                soff = sect % sect_per_track;

                /* is this sector in the same track? */
                track = sect - soff;

                /* always 63 sectors / track */
                num_sect = sect_per_track;

                /* point to BUFFADDR */
                buffaddr = ((char *) BUFFADDR + 
                            (soff << sect_bits) + byte_off);

                dprintf("buffaddr: 0x%x, soff: %d, track: %d\n", 
                        buffaddr, soff, track);

                /*  if current track differs do a lower read */
                if (track != tmp_track) {
                        int read_start, read_len;

                        read_start = track;

                        /* always read 63 sectors / per track */
                        read_len = sect_per_track;

                        /*
                         * If there's more than one read in this entire loop, 
                         * then only make the earlier reads for the portion 
                         * needed.  
                         *
                         * This saves filling the buffer with data that won't 
                         * be used!
                         */
                        if (slen > num_sect) {
                                bprintf("adjusting read_start to %x, "
                                        "read_len to %x\n", 
                                        read_start, read_len);

                                read_start = sect;
                                read_len = num_sect;

                                buffaddr = (char *) BUFFADDR + byte_off;
                        }

                        dprintf("read_start=%d, "
                                "read_len=%d>\n", read_start, read_len);

                        do_rawread(&disk_geom, read_start, read_len, BUFFADDR);

                        tmp_track = track;
                }

                if (size > ((num_sect << sect_bits) - byte_off)) {
                        bprintf("Adjusting size to %d\n", size);
                        size = (num_sect << sect_bits) - byte_off;
                }

                memmove(buf, buffaddr, size);

                buf += size;
                byte_len -= size;
                sect += num_sect;

                byte_off = 0;
        }

        return (!errnum);
}

int
read_mbr(char *buf)
{
        unsigned long sect = 0;

        /* Read the MBR, in order get the partition table (@446 bytes) */
        if (!rawread(sect, 0, SECTSIZE, buf)) {
                bprintf("Failed to read MBR\n");
                errnum = ERR_READ;
                return 0;
        }

        /* Check if it is valid.  */
        if (!MBR_CHECK_SIG(buf)) {
                bprintf("Bad MBR CHECK SIGNATURE\n");
                errnum = ERR_BAD_PART_TABLE;
                return 0;
        }

        return 1;
}

int 
devread(int sect, int byte_off, int byte_len, char *buf)
{

        unsigned long bndry;

        /* check sanity */
        if (sect < 0) {
                bprintf("Invalid sector given!\n");
                errnum = ERR_OUTSIDE_PART;
                return 0;
        }

        bndry = sect + (byte_off + byte_len - 1);
        bndry >>= SECTOR_BITS;

        /* check partition boundary */
        if (bndry >= curr_part.len) {
                bprintf("Reading outside of partition!\n");
                errnum = ERR_OUTSIDE_PART;
                return 0;
        }

        /* Get the read to the beginning of a partition. */
        sect += byte_off >> SECTOR_BITS;
        byte_off &= SECTSIZE - 1;

        dprintf("<sect: %d, byte_off: %d, byte_len: %d>\n", 
                curr_part.start + sect, byte_off, byte_len);

        return rawread(curr_part.start + sect, byte_off, byte_len, buf);
}

int
disk_open(char *fn, struct file *file)
{
        /* make sure we're at 0 */
        file->pos = 0;

        if (*fn != '/') {
                bprintf("Filename is not absolute!\n");
                errnum = ERR_BAD_FILENAME;
                return 0;
        }

        bprintf("Looking for ELF '%s'\n", fn);

        /* disk_setup() puts SUPERBLOCK in SUPER_BUF */
        if (!disk_setup(BOOT_DRIVE, DEST_PARTITION)) {
                bprintf("Failed to open partition\n");
                return 0;
        }

        if (!errnum && ext2_dir(fn, file)) {
                /* now fn is in INODE */
                return 1;
        }

        bprintf("Failed to find/read '%s'\n", fn);
        return 0;
}

void
disk_close(void)
{
        dprintf("closing partition...\n");
}

int
disk_read(char *buf, int len, struct file *file)
{
        if ((file->pos < 0) || (file->pos > file->max)) {
                file->pos = file->max;
        }

        if ((len < 0) || (len > (file->max - file->pos))) {
                len = file->max - file->pos;
        }

        /* This accounts for partial filesystem implementations. */
        if (file->pos + len > MAXINT) {
                errnum = ERR_FILELENGTH;
                return 0;
        }

        return ext2_read(buf, len, file);
}

int
disk_seek(int off, struct file *file)
{
        if (off > file->max || off < 0) {
                return -1;
        }

        file->pos = off;
        return file->pos;
}
