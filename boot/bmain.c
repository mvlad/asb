#include "common/x86.h"
#include "common/mb.h"

#include "boot/console.h"
#include "boot/serial.h"
#include "boot/cga.h"
#include "boot/disk.h"
#include "boot/elf.h"
#include "boot/string.h"
#include "boot/boot.h"
#include "boot/errno.h"

#include "boot/config.h"

/** our name */
static unsigned char btldr_name[] = BOOTLOADER_NAME;

/** the partition to mount from the kernel */
static unsigned char cmd_line[] = BOOTLOADER_CMDLINE;

/** save here drive information to pass to kernel */
static struct drive_info dri;

/** initialize error */
errno_t errnum = ERR_NONE;

/** kernel entry function */
entry_func entry_addr;

/**
 * @brief print E820 memory map
 */
static void 
print_e820_mem(void)
{
        unsigned long j, csize = 0;
        unsigned long nr_entries = e820_nr_entries;

        struct mmap_entry *mmap = &e820_mem_map;
        unsigned long mmap_start = (unsigned long) &e820_mem_map;
        unsigned long mmap_end = 
                (unsigned long) mmap + (nr_entries * sizeof(struct mmap_entry));

        bprintf("E820 memory map,");
        bprintf(" mmap_start=0x%x, mmap_end=0x%x\n", 
                mmap, mmap_end);

        for (j = 0; (unsigned long) mmap < mmap_end; j++) {

                bprintf("Entry: %d, ", j);

                bprintf("addrLow: 0x%x, len: 0x%x, ",
                        mmap->base_addr_low, mmap->length_low);

                bprintf("addrHigh: 0x%x, len: 0x%x. ",
                        mmap->base_addr_high, mmap->length_high);

                bprintf("type: 0x%x, ", mmap->type);

                csize = mmap->length_low + mmap->base_addr_low;
                bprintf("Total: 0x%x\n", csize);

                mmap = (struct mmap_entry *) 
                        ((unsigned long) mmap + sizeof(struct mmap_entry));

        }
}

/**
 * @brief print disk geometry
 */
static void 
print_disk_geom(void)
{
        struct drive_parameters *dp = &DAP;

        bprintf("\nDisk geometry");

        bprintf(" heads: %d, cyls: %d, sectors per track: %d\n",
                        heads, cyls, sectors);

        bprintf("Total sectors: %d\n", 
                        dp->total_sectors);
}

/**
 * @brief save disk info to pass to the kernel
 */
static void
save_drive_info(struct drive_info *dri)
{
        dri->drive_heads        = heads;
        dri->drive_cylinders    = cyls;
        dri->drive_sectors      = sectors;
        dri->size               = sizeof(*dri);

        /* we automatically assume first drive */
        dri->drive_number       = BOOT_DRIVE; 
        dri->drive_mode         = MB_DI_LBA_MODE;
}

/**
 * @brief save multiboot information to pass to the kernel
 */
static void 
save_multiboot_info(struct multiboot_info *mbi)
{

        mbi->flags = MB_INFO_CMDLINE | 
                     MB_INFO_MEM_MAP | 
                     MB_INFO_DRIVE_INFO |
                     MB_INFO_BOOT_LOADER_NAME;

        mbi->boot_loader_name = (unsigned long) &btldr_name;
        mbi->cmdline = (unsigned long) &cmd_line;

        mbi->mmap_addr = (unsigned long) &e820_mem_map;
        mbi->mmap_length = (unsigned long) e820_nr_entries * 
                           sizeof(struct mmap_entry);

        save_drive_info(&dri);

        mbi->drives_addr = (unsigned long) &dri;
        mbi->drives_length = sizeof(struct drive_info);
}


void bmain(void)
{
        int len;

        struct file kern;
        unsigned char buf[MULTIBOOT_SEARCH];
        struct multiboot_info mbi;

        cons_init();

        print_disk_geom();

        print_e820_mem();

        memset(&kern, 0, sizeof(struct file));

        /*
         * 'open' the partition: put SUPERBLOCK n SUPER_BUF, and KERNEL_IMAGE in INODE
         */
        if (!disk_open(KERNEL_IMAGE, &kern)) {
                goto bad;
        }

        bprintf(">> '%s' file found (%d bytes)\n\n", KERNEL_IMAGE, kern.max);

        /* read kernel ELF header into buffer */
        len = disk_read((char *) buf, MULTIBOOT_SEARCH, &kern);
        if (!len || len < 32) {
                bprintf("failed to get MULTIBOOT_SEARCH...\n");
                disk_close();

                if (!errnum) {
                        errnum = ERR_EXEC_FORMAT;
                }
                goto bad;
        }

        /* check if a valid exec elf file, also set up entry_addr */
        if (!elf_valid(buf, len)) {
                goto bad;
        }

        bprintf("Loading ELF image (filepos=0x%x, filemax=0x%x...)\n", 
                        kern.pos, kern.max);
        /* 
         * load each PT_LOAD section of the ELF image
         */
        if (!elf_load(buf, &kern)) {
                bprintf("Failed to load '%s'\n", KERNEL_IMAGE);
                goto bad;
        }

        bprintf("\nLoaded ELF @ 0x%x\n", (unsigned) entry_addr);
        disk_close();

        /* store multiboot data */
        memset(&mbi, 0, sizeof(struct multiboot_info));
        save_multiboot_info(&mbi);

        /* save it, as the kernel will get it */
        __asm __volatile("movl %0, %%ebx" : : "r" (&mbi));
        __asm __volatile("movl %0, %%eax" : : "r" (MULTIBOOT_VALID));

        /* let it rip!, jump kernel entry addr */
        ((void (*)(void)) entry_addr)();

bad:
        /* jump and hlt */
        print_error();

	outw(0x8A00, 0x8A00);
	outw(0x8A00, 0x8E00);

        __asm __volatile("hlt");
}
