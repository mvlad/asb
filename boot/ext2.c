#include "boot/disk.h"
#include "boot/ext2.h"
#include "boot/string.h"
#include "boot/errno.h"
#include "boot/console.h"

static int mapblock1, mapblock2;

extern errno_t errnum;


/**
 * @brief display inode contents
 */
static void 
display_inode(void)
{
        unsigned char *i;
        unsigned char *end;
        static char hexdigit[] = "0123456789abcdef";

        i = (unsigned char *) INODE;
        end = (unsigned char *) INODE + sizeof(struct ext2_inode);
        while (i < end) {

                bprintf("%c", hexdigit[*i >> 4]);
                bprintf("%c", hexdigit[*i % 16]);

                if (!((i + 1 - (unsigned char *) INODE) % 16)) {
                        bprintf("\n");
                } else {
                        bprintf(" ");
                }
        }
        bprintf("\n");
}

int
ext2_mount(void)
{
        dprintf("\nReading superblock...\n");

        /*
         * reading SBLOCK (2 sectors) into SUPERBLOCK (SUPER_BUF)
         */
        if (!devread(SBLOCK, 0, sizeof(struct ext2_super_block), (char *) SUPERBLOCK)) {
                bprintf("Could not mount fs: devread() failed!\n");
                return 0;
        }

        if (SUPERBLOCK->s_magic != EXT2_SUPER_MAGIC) {
                bprintf("Could not mount fs: Not an EXT2 filesystem!\n");
                return 0;
        }

        return 1;
}

/**
 * @brief Takes a file system block number and reads it into buffer 'buf'
 * @param fsblock the file system block number
 * @param buf integer buffer
 * @retval 1 if succeedes, 0 otherwise
 */
static int 
ext2_rdfsb(int fsblock, int buf)
{
        int blks, blks_per_size;

        blks = EXT2_BLOCK_SIZE(SUPERBLOCK);
        blks_per_size = EXT2_BLOCK_SIZE(SUPERBLOCK) / DEV_BSIZE;

        return devread(fsblock * blks_per_size, 0, blks, (char *) buf);
}


/**
 * @brief determine if the string pointed by 'd' 
 */
static int
file_is_found(char *d)
{
        if (!*d || isspace(*d)) {
                if (!S_ISREG(INODE->i_mode)) {
                        errnum = ERR_BAD_FILETYPE;
                        return 0;
                }
                /* file found */
                return 1;
        }
        return -1;
}

/**
 * @brief determine if the inode has valid size and is not a directory
 * @param inode a pointer to struct ext2_inode
 * @retval 0 in case of failure, 1 otherwise
 */
static int
file_is_valid(struct ext2_inode *inode)
{
        if (!(inode->i_size) || !S_ISDIR(inode->i_mode)) {
                errnum = ERR_BAD_FILETYPE;
                bprintf("Bad file type!\n");
                return 0;
        }
        return 1;
}

static int
get_ind_blocks(int lblock)
{
        unsigned long ind_blks = GET_IND_BLOCKS();

        if (mapblock1 != 1 && !ext2_rdfsb(ind_blks, DATABLOCK1)) {
                errnum = ERR_FSYS_CORRUPT;
                return -1;
        }

        mapblock1 = 1;
        return ((uint32_t *) DATABLOCK1)[lblock];
}

static int
get_dind_blocks(int lblock)
{
        int bnum, block;
        unsigned long d_ind_blks = GET_DIND_BLOCKS();

        if (mapblock1 != 2 && !ext2_rdfsb(d_ind_blks, DATABLOCK1)) {
                errnum = ERR_FSYS_CORRUPT;
                return -1;
        }

        mapblock1 = 2;

        block = lblock >> EXT2_ADDR_PER_BLOCK_BITS(SUPERBLOCK);
        bnum = ((uint32_t *) DATABLOCK1)[block];

        if (bnum != mapblock2 && !ext2_rdfsb(bnum, DATABLOCK2)) {
                errnum = ERR_FSYS_CORRUPT;
                return -1;
        }

        mapblock2 = bnum;
        block = lblock & (EXT2_ADDR_PER_BLOCK(SUPERBLOCK) - 1);
        return ((uint32_t *) DATABLOCK2)[block];
}

static int
get_tind_blocks(int lblock)
{
        unsigned long pbb, tmp;
        unsigned long per_block, tmp2;

        unsigned long t_ind_blks = GET_TIND_BLOCKS();

        if (mapblock1 != 3 && !ext2_rdfsb(t_ind_blks, DATABLOCK1)) {
                errnum = ERR_FSYS_CORRUPT;
                return -1;
        }

        mapblock1 = 3;

        pbb = EXT2_ADDR_PER_BLOCK_BITS(SUPERBLOCK);
        tmp = lblock >> (pbb * 2);

        if (!ext2_rdfsb(((uint32_t *) DATABLOCK1)[tmp], DATABLOCK2)) {
                errnum = ERR_FSYS_CORRUPT;
                return -1;
        }

        per_block = EXT2_ADDR_PER_BLOCK(SUPERBLOCK) - 1;
        tmp2 = (lblock >> pbb) & per_block;

        if (!ext2_rdfsb(((uint32_t *) DATABLOCK2)[tmp2], DATABLOCK2)) {
                errnum = ERR_FSYS_CORRUPT;
                return -1;
        }
        return ((uint32_t *) DATABLOCK2)[lblock & per_block];
}

/**
 * @brief Maps a logical block (the file offset divided by the blocksize) into
 * a physical block (the location in the file system) via an inode. 
 * @param lblock the file offset divided by the blocksize
 * @retval the data block, or -1 in case of failure
 */
static int
ext2_block_map(unsigned int lblock)
{
        dprintf("lblock: %u\n", lblock);

        /* if it is directly pointed to by the inode, return that physical addr */
        if (lblock < EXT2_NDIR_BLOCKS) {
                dprintf("Trying direct blocks\n");
                return INODE->i_block[lblock];
        }

        /* else try the indirect block */
        lblock -= EXT2_NDIR_BLOCKS;
        dprintf("lblock: %u\n", lblock);
        if (lblock < EXT2_ADDR_PER_BLOCK(SUPERBLOCK)) {
                dprintf("Trying indirect blocks\n");
                return get_ind_blocks(lblock);
        }

        /* else try the double indirect block */
        lblock -= EXT2_ADDR_PER_BLOCK(SUPERBLOCK);
        dprintf("lblock: %u\n", lblock);
        if (lblock < (unsigned) (1 << (EXT2_ADDR_PER_BLOCK_BITS(SUPERBLOCK) * 2))) {
                dprintf("Trying double indirect blocks\n");
                return get_dind_blocks(lblock);
        }

        /* else and finaly third indirect block */
        mapblock2 = -1;
        lblock -= (1 << (EXT2_ADDR_PER_BLOCK_BITS(SUPERBLOCK) * 2));

        dprintf("lblock: %u\n", lblock);
        dprintf("Trying triple indirect blocks\n");
        return get_tind_blocks(lblock);
}

int 
ext2_read(char *buf, int len, struct file *file)
{
        int lblock, offset, map;

        int ret = 0;
        int size = 0;

        while (len > 0) {

                /* find the (logical) block component of our location */
                lblock = get_logical_blk(file->pos);
                offset = get_offset_loc(file->pos);

                if ((map = ext2_block_map(lblock)) < 0) {
                        break;
                }

                dprintf("lblock: 0x%x, offset: 0x%x, map: 0x%x\n", 
                        lblock, offset, map);

                size = EXT2_BLOCK_SIZE(SUPERBLOCK);
                size -= offset;

                if (size > len) {
                        size = len;
                }

                if (map == 0) {
                        memset((char *) buf, 0, size);
                } else {
                        uint32_t blocks = GET_NR_BLOCKS();
                        if (!devread(map * blocks, offset, size, buf)) {
                                bprintf("failed devread!\n");       
                        }
                }

                buf += size;
                len -= size;

                file->pos += size;
                ret += size;
        }

        if (errnum) {
                ret = 0;
        }

        return ret;
}

/**
 * @brief 
 */
static unsigned int
find_dirn(char *dirn, char *rest, char ch, unsigned int *loc, 
          struct ext2_dir_entry **dp)
{
        /* hold the results of a string compare */
        int str_chk = 0;		   

        /* fs pointer of a particular block from dir entry */
        long map;

        int off, blk;

        struct ext2_dir_entry *tdp = *dp;

        do {
                /* if location/byte offset into the directory exceeds the size, give up */
                if (*loc >= INODE->i_size) {
                        errnum = ERR_FILE_NOT_FOUND;
                        *rest = ch;
                        bprintf("loc > INODE->i_size\n");
                        return 0;
                }

                /* else, find the (logical) block component of our location */
                blk = *loc >> EXT2_BLOCK_SIZE_BITS(SUPERBLOCK);

                /* 
                 * we know which logical block of the directory entry we are
                 * looking for, now we have to translate that to the physical
                 * (fs) block on the disk 
                 */
                map = ext2_block_map(blk);

                mapblock2 = -1;
                if ((map < 0) || !ext2_rdfsb(map, DATABLOCK2)) {
                        errnum = ERR_FSYS_CORRUPT;
                        *rest = ch;
                        bprintf("FSYS_CORRUPT\n");
                        return 0;
                }

                off = get_offset_loc(*loc);
                tdp = (struct ext2_dir_entry *)(DATABLOCK2 + off);

                /* advance loc prematurely to next on-disk directory entry */
                *loc += tdp->rec_len;

                /* ext2 filenames are NOT null-terminated */
                if (tdp->inode) {
                        int saved_c;

                        /* null-terminate the string */
                        saved_c = tdp->name[tdp->name_len];
                        tdp->name[tdp->name_len] = 0;

                        /* check if this is the file we're looking for...*/
                        str_chk = substring(dirn, tdp->name);

                        /* put it back */
                        tdp->name[tdp->name_len] = saved_c;
                }

                /* write it back to the caller, so we can advanced to next dir entry */
                *dp = tdp;

        } while (!tdp->inode || str_chk);

        return 1;
}

static int 
lookup_inode_blk(int current_ino)
{
        uint32_t first_blk, inode_per_grp;

        /* which group the inode is in */
        int group_id, group_desc, group_index;

        int blk_size, inode_blk;

        group_id = GET_GROUP_ID(current_ino);
        group_desc = GET_GROUP_DESC(group_id);
        group_index = GET_GROUP_INDEX(group_id);

        first_blk = SUPERBLOCK->s_first_data_block;
        if (!ext2_rdfsb(WHICH_SUPER + group_desc + first_blk,
                                (int) GROUP_DESC)) {
                return 0;
        }

        blk_size = GET_BLK_SIZE(sizeof(struct ext2_inode));
        inode_per_grp = GET_INODES_PER_GROUP();

        inode_blk = GROUP_DESC[group_index].bg_inode_table +
                    (((current_ino - 1) % inode_per_grp) >> log2(blk_size));

        return inode_blk;
}

int 
ext2_dir(char *dirn, struct file *file)
{
        /* start at the root */
        int current_ino = EXT2_ROOT_INO;   

        /* inode info corresponding to current_ino */
        struct ext2_inode *raw_inode;	   

        /* pointer to directory entry */
        struct ext2_dir_entry *dp;

        /* the parent of the current directory */
        int updir_ino = current_ino;	   

        /* fs pointer of the inode's information */
        int ino_blk;			   

        /* temp char holder */
        char ch;			   
        char *rest;

        unsigned int loc;

        /* size of inode struct */
        int siz_inode = sizeof(struct ext2_inode);

        /*
         * loop invariants:
         * - current_ino = inode to lookup
         * - dirn = pointer to filename component we are cur looking up within 
         * the directory known pointed to by current_ino (if any)
         */

        bprintf("ext2_dir -> '%s'\n", dirn);

        while (1) {
                bprintf("dirn=%s, inode %d\n", dirn, current_ino);

                /* look up inode */
                ino_blk = lookup_inode_blk(current_ino);
                if (ino_blk == 0) {
                        return 0;
                }

                /* read into INODE the inode block */
                if (!ext2_rdfsb(ino_blk, (int) INODE)) {
                        return 0;
                }

                /* reset indirect blocks! */
                mapblock2 = mapblock1 = -1;
                raw_inode = GET_RAW_INODE(current_ino, GET_BLK_SIZE(siz_inode));

                /* copy inode to fixed location */
                memmove((void *) INODE, (void *) raw_inode, siz_inode);


                /* if end of filename, INODE points to the file's inode */
                switch (file_is_found(dirn)) {
                        case 0: return 0;       /* found, but bad file type */
                        case 1:                 /* found, store size */
                                file->max = INODE->i_size; 
                        return 1;
                        default: break;         /* not found, go on... */
                }

                /* else we have to traverse a directory */
                updir_ino = current_ino;

                /* skip over slashes */
                while (*dirn == '/') {
                        dirn++;
                }

                /* if this isn't a directory of sufficient size to hold file, abort */
                if (!file_is_valid(INODE)) {
                        return 0;
                }

                /* skip to next slash or end of filename (space) */
                rest = dirn;
                dprintf("rest='%s'\n", rest);
                while ((ch = *rest) && !isspace(ch) && ch != '/') {
                        dprintf("ch='%c'\n", ch);
                        rest++;
                }

                /* 
                 * look through this directory and find the next filename
                 * component invariant: rest points to slash after the next
                 * filename component 
                 */
                *rest = 0;
                loc = 0;

                if (!find_dirn(dirn, rest, ch, &loc, &dp)) {
                        return 0;
                }

                /* go to next inode */
                current_ino = dp->inode;
                *(dirn = rest) = ch;
        }
}
