#include "common/types.h"

#include "boot/string.h"
#include "boot/errno.h"

/**
 * @addtogroup BOOT
 * @{
 */
extern errno_t errnum;


/**
 * @brief test if c contains either a space, tab, CR or NL
 * @retval return 1 if true, 0 otherwise
 */
int 
isspace(int c)
{
        switch (c) {
                case ' ':
                case '\t':
                case '\r':
                case '\n':
                        return 1;
                default:
                        break;
        }
        return 0;
}

void *
memset(void *v, int c, size_t n)
{
        char *p;
        int m;

        p = v;
        m = n;
        while (--m >= 0)
                *p++ = c;

        return v;
}

void *
memmove(void *dst, const void *src, size_t n)
{
        const char *s;
        char *d;

        s = src;
        d = dst;

        if (s < d && s + n > d) {
                s += n;
                d += n;
                while (n-- > 0)
                        *--d = *--s;
        } else {
                while (n-- > 0)
                        *d++ = *s++;
        }

        return dst;
}

int 
substring(const char *s1, const char *s2)
{
        const char *p1 = s1;
        const char *p2 = s2;

        while (*p1 == *p2) {

                if (!*(p1++)) {
                        return 0;
                }

                p2++;
        }
        /* if s1 is a substring of s2 */
        if (*p1 == 0) {
                return -1;
        }

        /* if s1 isn't a string of s2 */
        return 1;
}

/**
 * @}
 */
