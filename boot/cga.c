#include "common/x86.h"
#include "boot/cga.h"

/**
 * @addtogroup BOOT
 * @{
 */

static uint16_t *vid_mem;

static uint8_t cur_x = 0;      /**< current x position */
static uint8_t cur_y = 0;      /**< current y position */


void 
cga_init(void)
{
        unsigned int pos;

        /* Extract cursor location */
        pos = extract_cursor_pos();

        /* remeber where cursor is in x and y coordinates */
        cur_x = EXTRACT_CUR_POS_X(pos);
        cur_y = EXTRACT_CUR_POS_Y(pos);

        cga_move_cursor(pos);

        /* a pointer to CGA video memory */
        vid_mem = (uint16_t *) CGA_BUF;
}

void
cga_putc(int c)
{
        uint8_t attr_byte;
        uint16_t cur_pos;

        uint16_t attr, *loc;

        attr_byte = ATTR_BYTE(BG_BLUE, FG_WHITE);

        /*
         * The attribute byte is the top 8 bits of the word we have to send to
         * the VGA board.
         */
        attr = attr_byte << 8;

        switch (c) {
                /* backspace */
                case 0x08:
                        if (cur_x) {
                                cur_x--;
                        }
                        break;
                case '\t':
                        /* 
                         * handle a tab by increasing the cursor's x, but only
                         * to a point where it is divisible by 8.
                         */
                        cur_x = (cur_x + 8) & ~(8 - 1);
                        break;
                case '\n':
                        /* 
                         * Handle newline by moving cursor back to left and
                         * increasing the row
                         */
                        cur_y++;
                        /* fall through */
                case '\r':
                        /* handle carriage return */
                        cur_x = 0;
                        break;
                default:
                        /* Handle any other printable character. */
                        loc = vid_mem + SET_CUR_POS(cur_x, cur_y);
                        *loc = c | attr;
                        cur_x++;
                        break;
        }

        /*
         * Check if we need to insert a new line because we have reached the end
         * of the screen.
         */
        if (cur_x >= CRT_COLS) {
                cur_x = 0;
                cur_y++;
        }

        /* Row 25 is the end, this means we need to scroll up */
        if (cur_y >= CRT_ROWS) {

                /*
                 * Move the current text chunk that makes up the screen back in
                 * the buffer by a line
                 */
                int i;
                for (i = 0 * CRT_COLS; i < 24 * CRT_COLS; i++) {
                        vid_mem[i] = vid_mem[i + CRT_COLS];
                }

                /*
                 * The last line should now be blank. Do this by writing 80
                 * spaces to it.
                 */
                for (i = 24 * CRT_COLS; i < CRT_SIZE; i++) {
                        vid_mem[i] = BLANK;
                }

                /* The cursor should now be on the last line. */
                cur_y = 24;
        }

        cur_pos = SET_CUR_POS(cur_x, cur_y);

        /* move that little blinky thing */
        cga_move_cursor(cur_pos);
}

void 
cls(void)
{
        int i;
        
        for (i = 0; i < CRT_SIZE; i++) {
                vid_mem[i] = BLANK;
        }

        /* Move the hardware cursor back to the start. */
        cur_x = cur_y = 0;
}

/**
 * @}
 */
