/** @cond DOXYGEN_IGNORE_ASM */
.file "geom.S"


.globl get_disk_geom
.type get_disk_geom, @function
.code16
get_disk_geom:
        pushw   %bp
        movw    %sp, %bp

        pushw   %bx
        pushw   %di

        /* drive */
        movb    $0x80, %dl

        movb    $0x8, %ah
        int     $0x13

        /* check if successful */
        testb   %ah, %ah
        jnz     1f

        /* bogus BIOSes may not return an error number */
        testb   $0x3f, %cl      /* 0 sectors means no disk */
        jnz     1f              /* if non-zero, then succeed */

        /* XXX 0x60 is one of the unused error numbers */
        movb    $0x60, %ah
1:
        movb    %ah, %bl        /* save return value in %bl */

        /* heads */
        movb    %dh, %al
        incw    %ax             /* the number of heads is counted from zero */

        movw    $heads, %di
        movw    %ax, (%di)

        /* sectors */
        xorw    %ax, %ax
        movb    %cl, %al
        andb    $0x3f, %al

        movw    $sectors, %di
        movw    %ax, (%di)

        /* cylinders */
        shrb    $6, %cl
        movb    %cl, %ah
        movb    %ch, %al
        incw    %ax            /* the number of cylinders is counted from zero */

        movw    $cyls, %di
        movw    %ax, (%di)
        xorw    %ax, %ax
        movb    %bl, %al        /* return value in %eax */

        popw    %di
        popw    %bx
        popw    %bp

        retw

.globl get_dap_disk_geom
.type get_dap_disk_geom, @function
.code16
get_dap_disk_geom:
        pushw   %bp
        movw    %sp, %bp

        pushw   (%si)
        pushw   %bx

        /* compute the address of disk_address_packet */
        movw    $DAP, %si
        /* must give length */
        movw    $0x30, (DAP)

        xorw    %ax, %ax
        shrw    $4, %ax

        /* save the segment to cx */
        movw    %ax, %cx

        /* drive */
        movb    $0x80, %dl

        /* ax */
        movw    $0x4800, %ax

        movw    %cx, %ds
        int     $0x13

        /* save return value */
        movb    %ah, %dl        

        /* clear the data segment */
        xorw    %ax, %ax
        movw    %ax, %ds

        /* return value in %eax */
        movb    %dl, %al        

        popw    %bx
        popw    (%si)

        popw    %bp

        ret

/** @endcond */
