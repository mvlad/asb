#include "boot/disk.h"
#include "boot/console.h"
#include "boot/elf.h"
#include "boot/string.h"
#include "boot/errno.h"

#include "common/x86.h"
#include "common/types.h"

extern errno_t errnum;
extern entry_func entry_addr;

int
elf_valid(unsigned char *buf, unsigned int len)
{
        struct Elf *elf = (struct Elf *) buf;

        /* is this a valid Elf header */
        if ((len < sizeof(struct Elf))) {
                bprintf("Invalid length of ELF header\n");
                errnum = ERR_BAD_FILETYPE;
                return 0;
        }

        /* does it have valid elf things */
        if (!BOOTABLE_I386_ELF(elf)) {
                bprintf("Invalid file format. Not an ELF\n");
                errnum = ERR_EXEC_FORMAT;
                return 0;
        }

        /* set up entry function */
        entry_addr = (entry_func) elf->e_entry;
        if (entry_addr < (entry_func) 0x100000) {
                errnum = ERR_BELOW_1MB;
                return 0;
        }

        /* more sanity checks */
        if (elf->e_phoff == 0 || elf->e_phnum == 0) {
                errnum = ERR_EXEC_FORMAT;
                return 0;
        }
        if ((elf->e_phoff + (elf->e_phentsize * elf->e_phnum)) >= len) {
                errnum = ERR_EXEC_FORMAT;
                return 0;
        }
        return 1;
}

int
elf_load(unsigned char *buf, struct file *file)
{
        unsigned int loaded = 0;
        unsigned int memaddr;
        unsigned int cur_addr = 0;
        
        struct Elf *elf = (struct Elf *) buf;
        struct Proghdr *phdr;
        int i, filesiz, memsiz;

        /* scan for program segments */
        for (i = 0; i < elf->e_phnum; i++) {

                phdr = (struct Proghdr *) 
                        (elf->e_phoff + ((int) buf) + (elf->e_phentsize * i));

                if (phdr->p_type == PT_LOAD) {
                        dprintf("Loading PT_LOAD section\n");

                        /* offset into file, moves filepos */
                        if (disk_seek(phdr->p_offset, file) == -1) {
                                bprintf("Could not seek to %d\n", phdr->p_offset);
                                return 0;
                        }

                        filesiz = phdr->p_filesz;
                        memaddr = phdr->p_pa;
                        memsiz  = phdr->p_memsz;

                        dprintf("Filesiz: 0x%x bytes, memaddr: 0x%x, "
                                "memsize: 0x%x bytes\n",
                                filesiz, memaddr, memsiz);

                        if (memaddr < 0x100000) {
                                bprintf("Below 1MiB\n");
                                errnum = ERR_BELOW_1MB;
                                return 0;
                        }

                        /* make sure we only load what we're supposed to! */
                        if (filesiz > memsiz) {
                                filesiz = memsiz;
                        }

                        /* mark memory as used */
                        if (cur_addr < memaddr + memsiz) {
                                cur_addr = memaddr + memsiz;
                        }

                        /* load the segment */
                        if (disk_read((char *) memaddr, filesiz, file) == filesiz) {

                                if (memsiz > filesiz) {
                                        memset((char *) (memaddr + filesiz), 
                                               0xa, memsiz - filesiz);
                                }

                                loaded++;

                                bprintf("Section loaded <memaddr: 0x%x, filesize: 0x%x,"
                                        " memsize: 0x%x, length: 0x%x>\n",
                                        memaddr, filesiz, memsiz, 
                                        memsiz - filesiz);
                        } else {
                                bprintf("Failed to load segment\n");
                                break;
                        }
                }
        }

        /* no errs but not loaded ... */
        if (!loaded) {
                errnum = ERR_BOOT_FAILURE;
                return 0;
        }

        bprintf("%d segments loaded\n", loaded);
        return 1;
}

/**
 * @}
 */
