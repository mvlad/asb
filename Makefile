V = @

.DEFAULT_GOAL = all

include env.mk

ifeq ($(V),1)
override V = 
endif
ifeq ($(V),0)
override V = @
endif

TOP = .
OBJDIR := obj

# GDB port
GDBPORT	:= 1234

CC	:= gcc -pipe
AS	:= as
AR	:= ar
LD	:= ld
OBJCOPY	:= objcopy
OBJDUMP	:= objdump
NM	:= nm

# Compiler flags
CFLAGS := $(CFLAGS) -Os -fno-builtin -I$(TOP) -MD

CFLAGS += -fno-strict-aliasing -fno-omit-frame-pointer 

CFLAGS += -Wall -Wno-format -Wno-unused -Werror -fno-stack-protector\
	  -m32 -march=i386

CFLAGS += -Wextra -Wstrict-prototypes -Wmissing-prototypes

ifeq ($(DEBUG), 1)
	CFLAGS += -O0 -gstabs
endif


ifeq ($(DEBUG_BOOT_PRINTF), 1)
	CFLAGS += -DDEBUG_BOOT_PRINTF
endif

ifeq ($(DEBUG_KERNEL_PRINTF), 1)
	CFLAGS += -DDEBUG_KERNEL_PRINTF
endif

# Common linker flags
LDFLAGS := -m elf_i386

GCC_LIB := $(shell $(CC) $(CFLAGS) -print-libgcc-file-name)

# Lists that the boot0,boot,kernel/Makefile
# fragments will add to
OBJDIRS :=

# Eliminate default suffix rules
.SUFFIXES:

# Delete target files if there is an error (or make is interrupted)
.DELETE_ON_ERROR:

# make it so that no intermediate .o files are ever deleted
.PRECIOUS: %.o $(OBJDIR)/boot0/%.o $(OBJDIR)/boot/%.o \
		$(OBJDIR)/kernel/%.o 

KERN_CFLAGS := $(CFLAGS) -D__KERNEL__ -D__FAST_MEMMOVE__ -gstabs

# Include Makefiles for subdirectories
include boot0/Makefile	# first stage bootloader
include boot/Makefile	# second stage bootloader
include kernel/Makefile	# the kernel itself

CPUS ?= 1
QEMU ?= qemu-system-i386 -no-kvm
MEMORY ?= -m 8

QEMUOPTS = -hda $(OBJDIR)/kernel/hda.img -gdb tcp::$(GDBPORT) -serial mon:stdio
QEMU_TRACE_FILE = qemu.log
QEMUSEABIOS = -chardev stdio,id=seabios -device isa-debugcon,iobase=0x402,chardev=seabios

IMAGES = $(OBJDIR)/kernel/hda.img

QEMUOPTS += -smp $(CPUS)
QEMUOPTS += -net user -net nic,model=e1000
QEMUOPTS += $(MEMORY)

#
# The following are defined in kernel's make
#

# this builds only the targets
all: $(TARGETS)

# this build the disk as well
image: $(OBJDIR)/kernel/kernel.img

# this builds only the boot loader
asb: $(TARGET_BOOT)

run: all image $(IMAGES)
	$(QEMU) $(QEMUOPTS)

run-nox: $(IMAGES)
	@echo "* Use Ctrl-a x to exit qemu..."
	$(QEMU) -nographic $(QEMUOPTS)

debug: all image $(IMAGES)
	@echo "* Waiting for 'gdb' connection..." 1>&2
	$(QEMU) $(QEMUOPTS) -S

debug-nox: $(IMAGES)
	@echo "* Now run 'gdb'." 1>&2
	$(QEMU) -nographic $(QEMUOPTS) -S

debug-bios: $(IMAGES)
	@echo "* Running with SeaBIOS." 1>&2
	$(QEMU) $(QEMUOPTS) $(QEMUSEABIOS)

trace: $(IMAGES)
	@echo "* Tracing to $(QEMU_TRACE_FILE)..."
	$(QEMU) $(QEMUOPTS) -D $(QEMU_TRACE_FILE) -d in_asm,cpu,ioport

help: 
	@echo "*Targets*"
	@echo "========"
	@echo "\tall		builds the boot loader and the kernel"
	@echo "\tasb		builds only the boot loader (boot0 + boot)"
	@echo "\tboot0		builds only first stage loader"
	@echo "\tboot		builds only the second stage loader"
	@echo "\timage		creates a disk image suitable for testing in qemu"
	@echo ""
	@echo "\trun		run the image under qemu"
	@echo "\trun-nox		run the image under qemu, w/o graphics"
	@echo "\tdebug      	run the image under qemu, suitable for gdb"
	@echo "\tdebug-nox  	run the image under qemu, suitable for gdb, w/o graphics"
	@echo "\tdebug-bios 	run the image under qemu, including BIOS"
	@echo "\ttrace      	dump executed instructions, CPU state and I/O to $(QEMU_TRACE_FILE)"
	@echo ""
	@echo "\thelp            this message"
	@echo "\tdoc		build documentation"
	@echo "\tclean           remove build directory and object files"
	@echo "\tdistclean       remove all generated files"
	@echo "*Options*"
	@echo "========"
	@echo "\tDEBUG=1			build with -O0"
	@echo "\tDEBUG_BOOT_PRINTF=1	build boot loader with additional print information"
	@echo "\tDEBUG_KERNEL_PRINTF=1	build kernel with additional print information"
	

# For deleting the build
clean:
	@rm -rf $(OBJDIR) files qemu.log trace-* core\.*

distclean: clean
	@rm -rf TAGS cscope.* tags doc/html

cscope.files:
	@rm -rf cscope* TAGS
	@find -name \*.[chS] > cscope.files

TAGS:	cscope.files
	@ctags -R -L cscope.files

tags:	TAGS

cscope: cscope.files
	-cscope -b -q -k

# generate documentation
doc: doc/doxygen.conf
	-doxygen doc/doxygen.conf

always:
	@:

.PHONY: all always clean TAGS cscope doc tags
