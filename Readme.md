ASB 
===

*A simple multi-boot loader with ext2 support*

---------------------

*ASB* is a stripped down boot loader with the capability of loading a kernel
from an **ext2** partition. It is only intended as an educational tool as to
understand the start-up procedure on x86 architecture, and most likely will not
work on real hardware.  *ASB* makes uses of
[grub-legacy](https://www.gnu.org/software/grub/grub-legacy.html) disk reading
procedure to load an **ext2** partition. Do note that it is heavily modified to
accommodate only **ext2** support. See more under *Limitations* section. 

Booting is done by using two boot loaders. As the first boot stage boot loader
can only occupy a sector (512 bytes) in length, and because we need load a
kernel from an **ext2** partition (thus requiring more space than just a
sector) the loader is divided into two boot loaders. 

*Additionally*, boot loader(s) (such as
[GRUB](http://www.gnu.org/software/grub/)) jump into protected mode as fast as
possible, and have trampoline code to go back to 16-bit whenever BIOS calls are
need. *ASB* avoids that, by saving all required information before jumping to
protected mode.

## Stages

* first stage boot loader:  [boot0](doc/Stage1.md)
* and second stage boot loader: [boot](doc/Stage2.md)

Assuming that the following table is the HDD layout, here's
where **boot0** and **boot** reside:

| size (bytes)  | size (sectors)| offset sector     | offset byte       | what                      |
|-------------  |-------------  |---------------    |-------------      |-------------------------  |
| 0             | 1             | 0                 | `0x00000000`      | boot0                     |
| 512           | 48            | 1                 | `0x00000200`      | boot                      |
| …          | ...           | ...               | `…`            | …                      |
| 1M            | 2             | 2048              | `0x00100000`      | start first partition     |
| 1M + 1k       | 2             | 2050              | `0x00100400`      | Ext2 superblock           |

The first stage is stored in the **MBR**[1], while the second is embedded in the
first megabyte of the disk, before the first partition of the disk starts.

The kernels' `Makefile` is responsible for generating a qemu image for useful in
testing the booting procedure.

## Compiling & Running

Typing `make` will compile the boot loader together with a simple kernel.
Typing `make help` will show the available targets you might want to play with.
*GCC* multilib (for generating 32-bit images) versions 4.7, 4.8 and 4.9 seem to
work fine.

    *Targets*
    ========
            all             builds the boot loader and the kernel
            asb             builds only the boot loader (boot0 + boot)
            boot0           builds only first stage loader
            boot            builds only the second stage loader
            image           creates a disk image suitable for testing in qemu

            run             run the image under qemu
            run-nox         run the image under qemu, w/o graphics
            debug           run the image under qemu, suitable for gdb
            debug-nox       run the image under qemu, suitable for gdb, w/o graphics
            debug-bios      run the image under qemu, including BIOS
            trace           dump executed instructions, CPU state and I/O to qemu.log

            help            this message
            doc             build documentation
            clean           remove all build and object files
            distclean       remove all generated files
    *Options*
    ========
            DEBUG=1         build with -O0
            DEBUG_PRINTF=1  build with additional debugging information


## Requirements & Tools

### Configure & Building

* You'll need GNU make and the *GCC* suite.
* Rough configuration takes place in `include/config.h` header in
case you'd like to modify default boot parameters/configuration 
variables.
* Debugging most function calls can be enabled by using `DEBUG_PRINTF=1` variable
when invoking `make`.

#### Testing under Linux

* Linux loop devices support for accessing the **ext2** partition.
* `kpartx`, `sfdisk` and `qemu-img` for [generating](doc/Disk.md) a suitable disk image.
* `e2tools` for making directories and copying the kernel in the **ext2**
partition.
* finally, `qemu` for running the boot loader and the kernel.

On a Debian based system you can simply invoke `apt-get install $package(s)` to
install them. Refer to your package management system for alternative ways of installing
them.

#### Testing under others OS ####

Alternatively, you need to find the tools to create a valid hard-drive image. Refer
to kernels' `Makefile` (`$(OBJDIR)/kernel/kernel.img` target) to understand
how write the boot-loader(s) to disk, so that it also contains/maintains an ext2
file system and a partition table.

### Running ###

A minimalistic kernel is provided which upon loading it prints the drive
parameters and the *E820* memory map. The kernel is a stripped-down version of
*PDOS MIT* group, which is used in their [6.828](http://pdos.csail.mit.edu/6.828) class, 
but with some slight **modifications**.

Targets for makefile:

* type `make` to build both the boot-loader and the kernel.
* type `make image` to create a disk image.
* type `make test` to test/load the boot loader and the kernel
* type `make debug` to start qemus' debugger.

### Limitations ###

#### General ####

* *ASB* was only tested under `qemu` so that it might now work
on real hardware.
* *ASB* multi-boot protocol support is somehow lacking. Most likely
won't be able to boot other kernels without modifications.
* Compressed ELF images are not supported (i.e., there is no
support to decompress them on the fly).

#### ext2 ####

* *ASB* assumes that the kernel resides on a **ext2** partition and
that the disk you provide to `qemu` is the first disk in the system.
* Support for extended partitions is not supported, therefore the kernel
must reside on a primary partition.
* **ext2** support does not include symlinks so it won't be able to find
a link that points to the kernel.


[1]: Well not exactly, we need room to store the partition table entries.
