#ifndef __COMMON_SERIAL_H
#define __COMMON_SERIAL_H

/*
 * @file common/serial.h USART definitions shared by both the boot loader and the kernel
 * @addtogroup COMMON
 * @{
 */

#define USART1		        0x3F8   /**< first USART port */

#define USART_RX		0	/**< In: Receive buffer (DLAB=0) */
#define USART_TX		0	/**< Out: Transmit buffer (DLAB=0) */

#define USART_DLL		0	/**< Out: Divisor Latch Low (DLAB=1) */
#define USART_DLM		1	/**< Out: Divisor Latch High (DLAB=1) */

#define USART_IER		1	/**< Out: Interrupt Enable Register */
#define USART_IER_RDI	        0x01	/**< Enable receiver data interrupt */

#define USART_IIR		2	/**< In: Interrupt ID Register */
#define USART_FCR		2	/**< Out: FIFO Control Register */

#define USART_LCR		3	/**< Out: Line Control Register */
#define	USART_LCR_DLAB	        0x80	/**< Divisor latch access bit */
#define	USART_LCR_WLEN8	        0x03	/**< Wordlength: 8 bits */

#define USART_MCR		4	/**< Out: Modem Control Register */
#define	USART_MCR_RTS	        0x02	/**< RTS complement */
#define	USART_MCR_DTR	        0x01	/**< DTR complement */
#define	USART_MCR_OUT2	        0x08	/**< Out2 complement */

#define USART_LSR		5	/**< In: Line Status Register */
#define USART_LSR_DATA	        0x01	/**< Data available */
#define USART_LSR_TXRDY	        0x20	/**< Transmit buffer avail */
#define USART_LSR_TSRE	        0x40	/**< Transmitter off */

static inline void
delay(void)
{
	inb(0x84); inb(0x84);
	inb(0x84); inb(0x84);
}

static inline uint8_t 
data_avail(void)
{
        return (inb(USART1 + USART_LSR) & USART_LSR_TXRDY);
}

#endif
