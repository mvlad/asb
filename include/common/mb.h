#ifndef __COMMON_MB_H
#define __COMMON_MB_H

/** 
 * @file common/mb.h Multiboot for bootloader and kernel
 * @addtogroup COMMON
 * @{
 */

#define MULTIBOOT_SEARCH                8192

/**
 *  The following value must be present in the EAX register.
 */
#define MULTIBOOT_VALID                 0x2BADB002

/** The magic number for the Multiboot header.  */
#define MULTIBOOT_HEADER_MAGIC          0x1BADB002

/** The bits in the required part of flags field we don't support. */
#define MULTIBOOT_UNSUPPORTED           0x0000fffc

/** Alignment of multiboot modules. */
#define MULTIBOOT_MOD_ALIGN             0x00001000

/** Alignment of the multiboot info structure. */
#define MULTIBOOT_INFO_ALIGN            0x00000004

/* Flags set in the 'flags' member of the multiboot header. */

/** Align all boot modules on i386 page (4KB) boundaries. */
#define MULTIBOOT_PAGE_ALIGN            0x00000001

/** Must pass memory information to OS. */
#define MULTIBOOT_MEMORY_INFO           0x00000002

/** Must pass video information to OS. */
#define MULTIBOOT_VIDEO_MODE            0x00000004

/** This flag indicates the use of the address fields in the header. */
#define MULTIBOOT_AOUT_KLUDGE           0x00010000

#ifndef __ASSEMBLER__
/**
 * @brief multiboot header
 */
struct multiboot_header {
        /** Must be MULTIBOOT_HEADER_MAGIC.  */
        unsigned magic;

        /** Feature flags.  */
        unsigned flags;
        unsigned checksum;

        /** These are only valid if MULTIBOOT_AOUT_KLUDGE is set.  */
        unsigned header_addr;
        unsigned load_addr;
        unsigned load_end_addr;
        unsigned bss_end_addr;
        unsigned entry_addr;

        /** These are only valid if MULTIBOOT_VIDEO_MODE is set */
        unsigned mode_type;
        unsigned width;
        unsigned height;
        unsigned depth;
};
#endif

/**
 * @brief test if the kernel is multiboot aware
 */
#define MULTIBOOT_FOUND(addr, len) \
  (!((addr) & 0x3) && (len) >= 12 \
   && *((int *) (addr)) == MULTIBOOT_MAGIC \
   && ! (*((unsigned *) (addr)) + *((unsigned *) (addr + 4)) + *((unsigned *) (addr + 8))) \
   && (!(MULTIBOOT_AOUT_KLUDGE & *((int *) (addr + 4))) || (len) >= 32) \
   && (!(MULTIBOOT_VIDEO_MODE & *((int *) (addr + 4))) || (len) >= 48))

#ifndef __ASSEMBLER__

/**
 *  @brief The structure type "mod_list" is used by the 
 *  "multiboot_info" structure.
 */
struct mod_list {
        /**
         * te memory used goes from bytes 'mod_start' 
         * to 'mod_end-1' inclusive 
         */
        unsigned long mod_start;
        unsigned long mod_end;

        /* Module command line */
        unsigned long cmdline;

        /* padding to take it to 16 bytes (must be zero) */
        unsigned long pad;
};

/**
 * @brief Drive Info structure.
 */
struct drive_info {
        /** The size of this structure.  */
        unsigned long size;

        /** The BIOS drive number.  */
        unsigned char drive_number;

#define MB_DI_CHS_MODE              0
#define MB_DI_LBA_MODE              1
        /** The access mode.  */
        unsigned char drive_mode;

        /** The BIOS geometry.  */
        unsigned short drive_cylinders;
        unsigned char drive_heads;
        unsigned char drive_sectors;

        /** The array of I/O ports used for the drive.  */
        unsigned short drive_ports[0];
};

/**
 * @brief E820 Memory map description
 */
struct mmap_entry {
        unsigned long base_addr_low;
        unsigned long base_addr_high;
        unsigned long length_low;
        unsigned long length_high;
        unsigned long type;
} __attribute__((packed));


/** 
 * @brief APM BIOS info.  
 */
struct apm_info {
        unsigned short version;
        unsigned short cseg;
        unsigned long offset;
        unsigned short cseg_16;
        unsigned short dseg_16;
        unsigned short cseg_len;
        unsigned short cseg_16_len;
        unsigned short dseg_16_len;
};
#endif

/*
 *  Flags to be set for multiboot_info struct
 */

/** is there basic lower/upper memory information? */
#define MB_INFO_MEMORY                  0x00000001

/** is there a boot device set? */
#define MB_INFO_BOOTDEV                 0x00000002

/** is the command-line defined? */
#define MB_INFO_CMDLINE                 0x00000004

/** are there modules to do something with? */
#define MB_INFO_MODS                    0x00000008

/* These next two are mutually exclusive */

/** is there a symbol table loaded? */
#define MB_INFO_AOUT_SYMS               0x00000010

/** is there an ELF section header table? */
#define MB_INFO_ELF_SHDR                0x00000020

/** is there a full memory map? */
#define MB_INFO_MEM_MAP                 0x00000040

/** Is there drive info?  */
#define MB_INFO_DRIVE_INFO              0x00000080

/** Is there a config table?  */
#define MB_INFO_CONFIG_TABLE            0x00000100

/** Is there a boot loader name?  */
#define MB_INFO_BOOT_LOADER_NAME        0x00000200

/** Is there a APM table?  */
#define MB_INFO_APM_TABLE               0x00000400

/** Is there video information?  */
#define MB_INFO_VIDEO_INFO              0x00000800

#ifndef __ASSEMBLER__
/**
 * @brief MultiBoot Info description
 *
 * The format of the Multiboot information structure 
 * (as defined so far) follows: 
 *
 * @verbatim
 *            +-------------------+
 *    0       | flags             |    (required)
 *            +-------------------+
 *    4       | mem_lower         |    (present if flags[0] is set)
 *    8       | mem_upper         |    (present if flags[0] is set)
 *            +-------------------+
 *    12      | boot_device       |    (present if flags[1] is set)
 *            +-------------------+
 *    16      | cmdline           |    (present if flags[2] is set)
 *            +-------------------+
 *    20      | mods_count        |    (present if flags[3] is set)
 *    24      | mods_addr         |    (present if flags[3] is set)
 *            +-------------------+
 *    28 - 40 | syms              |    (present if flags[4] or
 *            |                   |                flags[5] is set)
 *            +-------------------+
 *    44      | mmap_length       |    (present if flags[6] is set)
 *    48      | mmap_addr         |    (present if flags[6] is set)
 *            +-------------------+
 *    52      | drives_length     |    (present if flags[7] is set)
 *    56      | drives_addr       |    (present if flags[7] is set)
 *            +-------------------+
 *    60      | config_table      |    (present if flags[8] is set)
 *            +-------------------+
 *    64      | boot_loader_name  |    (present if flags[9] is set)
 *            +-------------------+
 *    68      | apm_table         |    (present if flags[10] is set)
 *            +-------------------+
 *    72      | vbe_control_info  |    (present if flags[11] is set)
 *    76      | vbe_mode_info     |
 *    80      | vbe_mode          |
 *    82      | vbe_interface_seg |
 *    84      | vbe_interface_off |
 *    86      | vbe_interface_len |
 *            +-------------------+
 * @endverbatim
 */
struct multiboot_info {
        /** MultiBoot info version number */
        unsigned long flags;

        /** Available memory from BIOS */
        unsigned long mem_lower;
        unsigned long mem_upper;

        /** "root" partition */
        unsigned long boot_device;

        /** Kernel command line */
        unsigned long cmdline;

        /** Boot-Module list */
        unsigned long mods_count;
        unsigned long mods_addr;

        union {
                struct {
                        /** (a.out) Kernel symbol table info */
                        unsigned long tabsize;
                        unsigned long strsize;
                        unsigned long addr;
                        unsigned long pad;
                } a;

                struct {
                        /** (ELF) Kernel section header table */
                        unsigned long num;
                        unsigned long size;
                        unsigned long addr;
                        unsigned long shndx;
                } e;
        } syms;

        /** Memory Mapping buffer */
        unsigned long mmap_length;
        unsigned long mmap_addr;

        /** Drive Info buffer */
        unsigned long drives_length;
        unsigned long drives_addr;

        /** ROM configuration table */
        unsigned long config_table;

        /** Boot Loader Name */
        unsigned long boot_loader_name;

        /** APM table */
        unsigned long apm_table;

        /** Video */
        unsigned long vbe_control_info;
        unsigned long vbe_mode_info;
        unsigned short vbe_mode;
        unsigned short vbe_interface_seg;
        unsigned short vbe_interface_off;
        unsigned short vbe_interface_len;
};


#endif

/** 
 * @}
 */

#endif /* !__MB_H */
