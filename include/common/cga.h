#ifndef __COMMON_CGA_H
#define __COMMON_CGA_H

/*
 * @file common/cga.h Shared VGA display definitions
 * @addtogroup COMMON
 * @{
 */

#define CGA_BASE	        0x3d4
#define CGA_BUF		        0xb8000

#define CRT_COLS                80
#define CRT_ROWS                25
#define CRT_SIZE	        (CRT_ROWS * CRT_COLS)

#define BG_WHITE                0x00
#define BG_BLUE                 0x01
#define FG_WHITE                0x0f

/**
 * The attribute byte is made up of two nibbles - the lower being the
 * foreground colour, and the upper the background colour.
 */
#define ATTR_BYTE(bg, fg)       ((bg << 4) | (fg & 0x0f))

/**
 * @brief a BLANK attribute character, useful for clearing up
 */
#define BLANK                   (0x20 | ATTR_BYTE(BG_WHITE, FG_WHITE) << 8)

/**
 * @brief given a 'x' and 'y' values, compute the current position
 */
#define SET_CUR_POS(x, y)       (((y) * CRT_COLS) + (x))

/**
 * @brief move cursor to position 'pos'
 */
static void
cga_move_cursor(uint16_t pos)
{
        outb(CGA_BASE, 14);
        outb(CGA_BASE + 1, pos >> 8);

        outb(CGA_BASE, 15);
        outb(CGA_BASE + 1, pos);
}

/**
 * @brief extract cursor
 */
static unsigned int
extract_cursor_pos(void)
{
        unsigned int pos;

        outb(CGA_BASE, 14);
        pos = inb(CGA_BASE + 1) << 8;

        outb(CGA_BASE, 15);
        pos |= inb(CGA_BASE + 1);

        return pos;
}

/**
 * @brief given 
 */
#define EXTRACT_CUR_POS_X(pos)  (pos >> 8)

/**
 * @brief
 */
#define EXTRACT_CUR_POS_Y(pos)  (pos & 0x00ff)


#endif /* __COMMON_CGA_H */
