#ifndef __STRING_H
#define __STRING_H

/**
 * @file boot/string.h simple string manipulation functions
 * @addtogroup BOOT
 * @{
 */


/**
 * @brief
 */
extern int 
isspace(int c);

/**
 * @brief
 */
extern int 
substring(const char *s1, const char *s2);

/**
 * @brief copy memory area from 'src' to 'dst'
 * @param src from memory area 
 * @param dst to memory area
 * @param len amount maximum in bytes to copy
 * @retval a pointer to 'dst'
 */
extern void *
memmove(void *dst, const void *src, size_t len);

/**
 * @brief fill memory with a constant byte
 * @param dst the memory area that will be filled
 * @param c the constant byte c used to fill the memory area
 * @param len the amount
 * @retval a pointer to the memory area 'dst'
 */
extern void *
memset(void *dst, int c, size_t len);

/**
 * @}
 */

#endif
