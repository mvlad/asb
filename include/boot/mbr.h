#ifndef __MBR_H
#define __MBR_H

/**
 * @file boot/mbr.h Master Boot Record information
 * @addtogroup BOOT
 * @{
 */

/*
 * These define the basic PC MBR sector characteristics
 */
#define MBR_SECTOR           0

#define MBR_SIG_OFFSET       510
#define MBR_SIGNATURE        0xaa55

#define SLICE_OFFSET         446
#define SLICE_MAX            4


/*
 *  Defines to guarantee structural alignment.
 */
#define MBR_CHECK_SIG(mbr_ptr) \
  ( *( (unsigned short *) (((int) mbr_ptr) + MBR_SIG_OFFSET) ) \
   == MBR_SIGNATURE )

#define MBR_SIG(mbr_ptr) \
  ( *( (unsigned short *) (((int) mbr_ptr) + MBR_SIG_OFFSET) ) )

#define SLICE_FLAG(mbr_ptr, part) \
  ( *( (unsigned char *) (((int) mbr_ptr) + SLICE_OFFSET \
			  + (part << 4)) ) )

#define SLICE_HEAD(mbr_ptr, part) \
  ( *( (unsigned char *) (((int) mbr_ptr) + SLICE_OFFSET + 1 \
			  + (part << 4)) ) )

#define SLICE_SEC(mbr_ptr, part) \
  ( *( (unsigned char *) (((int) mbr_ptr) + SLICE_OFFSET + 2 \
			  + (part << 4)) ) )

#define SLICE_CYL(mbr_ptr, part) \
  ( *( (unsigned char *) (((int) mbr_ptr) + SLICE_OFFSET + 3 \
			  + (part << 4)) ) )

#define SLICE_TYPE(mbr_ptr, part) \
  ( *( (unsigned char *) (((int) mbr_ptr) + SLICE_OFFSET + 4 \
			  + (part << 4)) ) )

#define SLICE_EHEAD(mbr_ptr, part) \
  ( *( (unsigned char *) (((int) mbr_ptr) + SLICE_OFFSET + 5 \
			  + (part << 4)) ) )

#define SLICE_ESEC(mbr_ptr, part) \
  ( *( (unsigned char *) (((int) mbr_ptr) + SLICE_OFFSET + 6 \
			  + (part << 4)) ) )

#define SLICE_ECYL(mbr_ptr, part) \
  ( *( (unsigned char *) (((int) mbr_ptr) + SLICE_OFFSET + 7 \
			  + (part << 4)) ) )

#define SLICE_START(mbr_ptr, part) \
  ( *( (unsigned long *) (((int) mbr_ptr) + SLICE_OFFSET + 8 \
			  + (part << 4)) ) )

#define SLICE_LENGTH(mbr_ptr, part) \
  ( *( (unsigned long *) (((int) mbr_ptr) + SLICE_OFFSET + 12 \
			  + (part << 4)) ) )


/*
 *  PC flag types are defined here.
 */
#define SLICE_FLAG_NONE      0
#define SLICE_FLAG_BOOTABLE  0x80

/*
 *  Known PC partition types are defined here.
 */

#define SLICE_TYPE_NONE         	0
#define SLICE_TYPE_FAT12        	1
#define SLICE_TYPE_FAT16_LT32M  	4
#define SLICE_TYPE_EXTENDED     	5
#define SLICE_TYPE_FAT16_GT32M  	6
#define SLICE_TYPE_FAT32		0xb
#define SLICE_TYPE_FAT32_LBA		0xc
#define SLICE_TYPE_FAT16_LBA		0xe
#define SLICE_TYPE_WIN95_EXTENDED	0xf
#define SLICE_TYPE_EZD        	        0x55
#define SLICE_TYPE_MINIX		0x80
#define SLICE_TYPE_LINUX_MINIX	        0x81
#define SLICE_TYPE_SWAP                 0x82
#define SLICE_TYPE_EXT2FS       	0x83
#define SLICE_TYPE_LINUX_EXTENDED	0x85
#define SLICE_TYPE_VSTAFS		0x9e
#define SLICE_TYPE_DELL_UTIL		0xde
#define SLICE_TYPE_LINUX_RAID	        0xfd

/* 
 * These ones are special, as they use their own partitioning scheme to
 * subdivide the PC partitions from there.  
 */
#define SLICE_TYPE_FREEBSD		0xa5
#define SLICE_TYPE_OPENBSD		0xa6
#define SLICE_TYPE_NETBSD		0xa9

/* 
 * FAT 
 */
#define SLICE_TYPE_FAT(type)	\
  ({ type == SLICE_TYPE_FAT12 \
     || type == SLICE_TYPE_FAT16_LT32M \
     || type == SLICE_TYPE_FAT16_GT32M \
     || type == SLICE_TYPE_FAT16_LBA \
     || type == SLICE_TYPE_FAT32 \
     || type == SLICE_TYPE_FAT32_LBA \
     || type == SLICE_TYPE_DELL_UTIL; })

#define IS_SLICE_TYPE_EXTENDED(type)	\
  (((type) == SLICE_TYPE_EXTENDED)	\
   || ((type) == SLICE_TYPE_WIN95_EXTENDED)	\
   || ((type) == SLICE_TYPE_LINUX_EXTENDED))

/* For convenience.  */
#define SLICE_TYPE_BSD_WITH_FS(type,fs)	\
  ((type) == (SLICE_TYPE_FREEBSD | ((fs) << 8)) \
   || (type) == (SLICE_TYPE_OPENBSD | ((fs) << 8)) \
   || (type) == (SLICE_TYPE_NETBSD | (fs) << 8))

#define SLICE_TYPE_BSD(type)	\
        SLICE_TYPE_BSD_WITH_FS(type,0)

/**
 * @}
 */
#endif /* __MBR_H */
