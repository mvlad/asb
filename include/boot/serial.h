#ifndef __BOOT_SERIAL_H
#define __BOOT_SERIAL_H

/**
 * @file boot/serial.h Boot loader USART methods
 * @addtogroup BOOT
 * @{
 */

#include "common/serial.h"

/**
 * @brief initialize serial port
 */
extern void
serial_init(void);

/**
 * @brief output a char on the serial port
 * @param c the char to output
 */
extern void
serial_putc(int c);

/**
 * @}
 */

#endif /* __SERIAL_H */
