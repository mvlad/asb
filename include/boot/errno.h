#ifndef __ERRNO_H
#define __ERRNO_H

/**
 * @file boot/errno.h list of possible errors
 * @addtogroup BOOT
 * @{
 */

typedef enum {
        ERR_NONE = 0,
        ERR_BAD_FILENAME,
        ERR_BAD_FILETYPE,
        ERR_BAD_PART_TABLE,
        ERR_BELOW_1MB,
        ERR_BOOT_FAILURE,
        ERR_EXEC_FORMAT,
        ERR_FILELENGTH,
        ERR_FILE_NOT_FOUND,
        ERR_FSYS_CORRUPT,
        ERR_FSYS_MOUNT,
        ERR_NO_DISK,
        ERR_NO_PART,
        ERR_OUTSIDE_PART,
        ERR_READ,
        ERR_WONT_FIT,

        MAX_ERR_NUM
} errno_t;


void 
print_error(void);

/**
 * @}
 */

#endif /* __ERRNO_H */
