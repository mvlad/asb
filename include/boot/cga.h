#ifndef __BOOT_CGA_H
#define __BOOT_CGA_H

/**
 * @file boot/cga.h VGA display definitions
 * @addtogroup BOOT
 * @{
 */

#include "common/cga.h"

/**
 * @brief init CGA
 *
 * @see http://stanislavs.org/helppc/video_information.html
 */
extern void 
cga_init(void);

/**
 * @brief write to CGA_BUF a char
 * @param c the char you want to write
 */
extern void 
cga_putc(int c);


/**
 * @brief Clears the screen, by copying lots of spaces to the framebuffer. 
 */
void 
cls(void);

/**
 * @}
 */
#endif /* __BOOT_CGA_H */
