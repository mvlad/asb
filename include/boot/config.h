#ifndef __BOOT_CONFIG_H
#define __BOOT_CONFIG_H

#define BOOT_DRIVE              0x80

/** which partition to mount => where the kernel resides */
#define DEST_PARTITION          DEST_PART_2

/** where is the kernel located on the partition */
#define KERNEL_IMAGE            "/boot/kernel/image/path/to/kern"

#define BOOTLOADER_NAME         "ASB"
#define BOOTLOADER_CMDLINE      "root=/dev/hda1"

#endif  /* __BOOT_CONFIG_H */
