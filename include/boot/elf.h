#ifndef __ELF_H
#define __ELF_H

/**
 * @file boot/elf.h ELF definition for bootloader
 * @addtogroup BOOT
 * @{
 */

#define ELF_MAGIC 0x464C457FU	/**< "\x7FELF" in little endian */

/** 
 * @brief ELF Header 
 */
struct Elf {
	uint32_t e_magic;	/**< must equal ELF_MAGIC */
	uint8_t e_elf[12];
	uint16_t e_type;
	uint16_t e_machine;
	uint32_t e_version;
	uint32_t e_entry;       /**< entry point, where start exec */
	uint32_t e_phoff;
	uint32_t e_shoff;
	uint32_t e_flags;
	uint16_t e_ehsize;
	uint16_t e_phentsize;
	uint16_t e_phnum;
	uint16_t e_shentsize;
	uint16_t e_shnum;
	uint16_t e_shstrndx;
};

/**
 * @brief program header
 */
struct Proghdr {
	uint32_t p_type;
	uint32_t p_offset;
	uint32_t p_va;
	uint32_t p_pa;
	uint32_t p_filesz;
	uint32_t p_memsz;
	uint32_t p_flags;
	uint32_t p_align;
};

/**
 * @brief section header
 */
struct Secthdr {
	uint32_t sh_name;
	uint32_t sh_type;
	uint32_t sh_flags;
	uint32_t sh_addr;
	uint32_t sh_offset;
	uint32_t sh_size;
	uint32_t sh_link;
	uint32_t sh_info;
	uint32_t sh_addralign;
	uint32_t sh_entsize;
};

/** 
 * segment types - page 5-3, figure 5-2 
 */
#define PT_NULL                 0
#define PT_LOAD                 1
#define PT_DYNAMIC              2
#define PT_INTERP               3
#define PT_NOTE                 4
#define PT_SHLIB                5
#define PT_PHDR                 6

#define PT_LOPROC               0x70000000
#define PT_HIPROC               0x7fffffff

/** segment permissions - page 5-6 */
#define PF_X                    0x1
#define PF_W                    0x2
#define PF_R                    0x4
#define PF_MASKPROC             0xf0000000


/** Values for Proghdr::p_type */
#define ELF_PROG_LOAD		1

/** Flag bits for Proghdr::p_flags */
#define ELF_PROG_FLAG_EXEC	1
#define ELF_PROG_FLAG_WRITE	2
#define ELF_PROG_FLAG_READ	4

/** Values for Secthdr::sh_type */
#define ELF_SHT_NULL		0
#define ELF_SHT_PROGBITS	1
#define ELF_SHT_SYMTAB		2
#define ELF_SHT_STRTAB		3

/** Values for Secthdr::sh_name */
#define ELF_SHN_UNDEF		0

/** Dynamic array tags - page 5-16, figure 5-10.  */
#define DT_NULL                 0
#define DT_NEEDED               1
#define DT_PLTRELSZ             2
#define DT_PLTGOT               3
#define DT_HASH                 4
#define DT_STRTAB               5
#define DT_SYMTAB               6
#define DT_RELA                 7
#define DT_RELASZ               8
#define DT_RELAENT              9
#define DT_STRSZ                10
#define DT_SYMENT               11
#define DT_INIT                 12
#define DT_FINI                 13
#define DT_SONAME               14
#define DT_RPATH                15
#define DT_SYMBOLIC             16
#define DT_REL                  17
#define DT_RELSZ                18
#define DT_RELENT               19
#define DT_PLTREL               20
#define DT_DEBUG                21
#define DT_TEXTREL              22
#define DT_JMPREL               23


#define EM_386                  3       /**< i386 -- obviously use this one */
#define ET_EXEC                 2       /**< we only care about executable types */

#define BOOTABLE_I386_ELF(h) ((h->e_magic == ELF_MAGIC) &       \
                              (h->e_type == ET_EXEC) &          \
                              (h->e_machine == EM_386))

/**
 * @brief check the kernel image for the presence of regular things:
 * - valid header image
 * - it is bootable
 * - it is executabile
 * @param buf pointer to file containing the kernel
 * @param len lenght of the file
 * @retval 1 in case of success, 0 otherwise
 */
int
elf_valid(unsigned char *buf, unsigned int len);

/**
 * @brief load the kernel in memory
 * @param buf a pointer to the kernel filename on disk
 * @param file a pointer to struct file
 * @retval 1 in case of success, 0 otherwise
 */
int
elf_load(unsigned char *buf, struct file *file);

/**
 * @brief the entry in the kernel
 *
 * @see Elf::e_entry
 */
typedef void (*entry_func)(int, int, int, int, int, int) 
__attribute__ ((noreturn));

/**
 * @}
 */
#endif /* __ELF_H */
