#ifndef __DISK_IO_H
#define __DISK_IO_H

#include "common/types.h"

/**
 * @file boot/disk.h Disk information
 * @addtogroup BOOT
 * @{
 */

#define SECTSIZE                        0x200
#define SECTOR_BITS                     9
#define BIOS_FLAG_FIXED_DISK            0x80

#define DRIVE_IS_DISK(drv)              (drv & BIOS_FLAG_FIXED_DISK)

#define BIOSDISK_FLAG_LBA_EXTENSION     0x1
#define BIOSDISK_ERROR_GEOMETRY         0x100

/** here the MBR gets loaded */
#define BOOTSEC_LOCATION                0x7C00

/** the boot sector must end with these 2 bytes */
#define BOOTSEC_SIGNATURE               0xAA55

/** offset of boot partition block */
#define BOOTSEC_BPB_OFFSET              0x3

/** length of boot partition block */
#define BOOTSEC_BPB_LENGTH              0x3B

#define BOOTSEC_BPB_SYSTEM_ID           0x3
#define BOOTSEC_BPB_HIDDEN_SECTORS      0x1C

/** the start of the first partition */
#define BOOTSEC_PART_OFFSET             0x1BE

/**
 * Partition table entry format:
 * * BYTE = 1 byte
 * * WORD = 2 bytes
 * * LONG = 4 bytes
 * 
 * |off         |size | type                                            |
 * |----------- |---- | ----------------------------------------------- |
 * |`0x00`      |BYTE | boot indicator (0x80 = active, 0x00 = inactive) |
 * |`0x01`      |BYTE | start head                                      |
 * |`0x02`      |WORD | start cylinder, sector                          |
 * |`0x04`      |BYTE | system type (0x83 = Linux, 0xA6 = OpenBSD)      |
 * |`0x05`      |BYTE | end head                                        |
 * |`0x06`      |WORD | end cylinder, sector                            |
 * |`0x08`      |LONG | start LBA sector                                |
 * |`0x0C`      |LONG | number of sectors in partition                  |
 *
 * In the case of a partition that extends beyond the 8GB boundary,
 * the LBA values will be correct, the CHS values will have their
 * maximums (typically `(C,H,S) = (1023,255,63)`).
 *
 * Each partition is `16 bytes * 4 = 64 bytes`, or `0x40`
 */
#define BOOTSEC_PART_LENGTH             0x40

/** the magic signature so the BIOS can boot from it */
#define BOOTSEC_SIG_OFFSET              0x1FE

#define BOOTSEC_LISTSIZE                8

#define NETWORK_DRIVE                   0x20
#define INVALID_DRIVE                   0x88

#define DRIVE_IS_NETWORK(drv)           (drv == NETWORK_DRIVE)
#define DRIVE_MAYBE_CDROM(drv)          (drv >= INVALID_DRIVE)

/** This is the location of the raw device buffer.*/
#define BUFFADDR                        0x70000
/** Size of the raw device buffer - 31.5K in size. */
#define BUFFLEN                         0x7e00

/** here we load the SUPERBLOCK */
#define SUPER_BUF                       0x68000
/** size of buf, 32K in size */
#define SUPER_BUFLEN                    0x8000

/**
 * The information for a disk geometry. The CHS information is only for
 * DOS/Partition table compatibility, and the real number of sectors is
 * stored in TOTAL_SECTORS.  
 */
struct geometry {
        unsigned long cylinders;        /**< The number of cylinders */
        unsigned long heads;            /**< The number of heads */
        unsigned long sectors;          /**< The number of sectors */
        unsigned long total_sectors;    /**< The total number of sectors */
        unsigned long sector_size;      /**< Device sector size */
        unsigned long flags;            /**< Flags */
};

#define GEOM_CYLS(g)            (g.cylinders)
#define GEOM_HEADS(g)           (g.heads)
#define GEOM_SECTORS(g)         (g.sectors)
#define GEOM_T_SECTORS(g)       (g.total_sectors)
#define GEOM_SECT_SIZE(g)       (g.sector_size)
#define GEOM_FLAGS(g)           (g.flags)
#define GEOM_SECT_SIZE_BITS(g)  (log2(GEOM_SECT_SIZE(g)))

#define VALID_SECT(sect, geom)  \
        ((sect) > 0 || (sect) < GEOM_T_SECTORS(geom))

/**
 * @brief accounting data for current open file
 */
struct file {
        int pos; /*< maintain file position */
        int max; /*< maintain file max size */
};

/**
 * @brief used to iterate over the partitions found in the partition table
 */
struct curr_open_part {

        /** entry number in the partition table */
        int entry;

        /** current partition type */
        int type;

        /** current partition */
        unsigned long part;

        /** partition length */
        unsigned long int len;

        /** partition start */
        unsigned long int start;
};

/**
 * @brief use INT 0x13 Extended Mode (http://wiki.osdev.org/ATA_in_x86_RealMode_(BIOS)#LBA_in_Extended_Mode)
 * to query the total sectors of the hard-drive.
 *
 * This structure, called disk address packet is used by @link get_dap_disk_geom @endlink.
 *
 * @see http://www.ctyme.com/intr/rb-0715.htm
 */
struct drive_parameters {
        unsigned short size;
        unsigned short flags;
        unsigned long cylinders;
        unsigned long heads;
        unsigned long sectors;
        unsigned long long total_sectors;
        unsigned short bytes_per_sector;

        /* ver 2.0 or higher */
        unsigned long EDD_configuration_parameters;

        /* ver 3.0 or higher */
        unsigned short signature_dpi;
        unsigned char length_dpi;
        unsigned char reserved[3];
        unsigned char name_of_host_bus[4];
        unsigned char name_of_interface_type[8];
        unsigned char interface_path[8];
        unsigned char device_path[8];
        unsigned char reserved2;
        unsigned char checksum;

        /**
         * reserved for buggy BIOS
         */
        unsigned char dummy[16];
} __attribute__ ((packed));

enum partitions {
        PART_1,
        PART_2,
        PART_3,
        PART_4
};

/**
 * @brief the partition we're trying to boot from
 *
 * The primary partitions are encoded as:
 * 1 -> 0ffff -> first
 * 2 -> 1ffff -> second
 * 3 -> 2ffff -> third
 * 4 -> 3ffff -> forth
 */
enum dest_partitions {
        DEST_PART_1     = (PART_1 << 16) | 0xffff,
        DEST_PART_2     = (PART_2 << 16) | 0xffff,
        DEST_PART_3     = (PART_3 << 16) | 0xffff,
        DEST_PART_4     = (PART_4 << 16) | 0xffff
};

#define PART_CHECK_1(part)      (part & 0xff000000uL)
#define PART_CHECK_2(part)      ((part & 0xff) == 0xff)
#define PART_CHECK_3(part)      ((part & 0xff00) == 0xff00)
#define PART_CHECK_4(part)      ((part & 0xff00) < 0x800)

#define PART_CHECK_IS_VALID(part)                               \
        !(PART_CHECK_1(part)) && PART_CHECK_2(part) &&          \
        (PART_CHECK_3(part) || PART_CHECK_4(part))

/**
 * @brief conversion from a slice to a partition
 */
#define GET_SLICE_FROM_PART(part)       (((part) & 0xff0000) >> 16)

/**
 * @brief conversion from a partition to a slice
 */
#define GET_PART_FROM_SLICE(slice)      (((slice) << 16) | 0xffff)

/**
 * @brief determine is the slice in question contains a EXT2 file system
 */
#define SLICE_IS_EXT2(slice)            (slice == SLICE_TYPE_EXT2FS)

/**
 * @brief check if 
 */
#define IS_SLICE_VALID(type)            ((type) != SLICE_TYPE_NONE)


static inline unsigned long
log2(unsigned long word)
{
        __asm __volatile("bsfl %1, %0" : "=r" (word) : "r" (word));
        return word;
}


/**
 * @brief
 */
static inline int
get_sect_len(int byte_off, int byte_len, unsigned long sect_size)
{
        return ((byte_off + byte_len + sect_size - 1) >> log2(sect_size));
}

/**
 * @brief convert a sector to LBA
 *
 * Converting CHS addressing to LBA is straightforward using the
 * following formula:
 * \verbatim
   (Cylinder * TotalHeads + SelectedHead) * SectorsPerTrack + (SectorNum - 1).
   \endverbatim
 */
static inline unsigned int
sect_to_lba(int sect, struct geometry *geom)
{
        unsigned int lba;
        int cyl_off, head_off, sect_off;

        int shead = sect / geom->sectors;

        head_off = shead % geom->heads;
        sect_off = sect % geom->sectors + 1;
        cyl_off = shead / geom->heads;

        lba = ((cyl_off * geom->heads) + head_off) * 
                geom->sectors + (sect_off - 1);

        return lba;
}

/**
 * @brief read the master boot record for retrieving the partition boot table
 * @param buf a buffer where to store the MBR
 * @retval 1 in case of success, 0 otherwise
 */
int
read_mbr(char *buf);

/**
 * @brief read into 'buf' from 'sect' at 'byte_off' offset in the amount of
 * 'byte_len'
 * @param sect start from this sector
 * @param byte_off offset 
 * @param byte_len how many bytes
 * @param buf where to save the data
 * @retval 1 in case it succeeds, 0 otherwise
 *
 * @note devread() is a wrapper over @link rawread @endlink
 *
 * @note devread() assumes that curr_part data structure has been populated,
 * more specifically that 'curr_part.start' stores the starting sector for the
 * destination partition as it passes to @link rawread() @endlink 'sect' plus
 * 'curr_part.start'.
 */
extern int
devread(int sect, int byte_off, int byte_len, char *buf);


/** 
 * @brief invoke @link ext2_dir() @endlink to find the 'INODE' where 
 * 'fn' resides
 * @param fn a string representing a path 
 * @param file a pointer to struct file
 * @retval 1 in case it succeeds, that is 'fn' has been found and now
 * resides in 'INODE'.
 * @retval 0 otherwise
 *
 * @note populates 'SUPERBLOCK' into @link SUPER_BUF @endlink
 *
 * @note populates 'INODE' with the contents of 'fn'.
 *
 */
extern int
disk_open(char *fn, struct file *file);

/**
 * @brief read into buf the len amount of bytes, assuming that the 
 * file is already copied into 'INODE'.
 * 
 * Copies from 'INODE' to 'buf' in the amount of 'len' while re-positioning the
 * file.
 *
 * @param buf buffer used to save the data
 * @param len size in bytes
 * @param file a pointer to struct file, used to maintain file position
 * @retval 0 in case of failure, 1 otherwise
 *
 * @note invokes @link ext2_read() @endlink to perform the operation.
 *
 * @note disk_open() must be invoked before this function to 
 *
 */
extern int
disk_read(char *buf, int len, struct file *file);

/**
 * @brief close open partition
 *
 * @note stub function, does nothing.
 */
extern void 
disk_close(void);

/** 
 * @brief Reposition a file offset.  
 * @param offset where to re-position the file offset
 * @param file a pointer to struct file
 * @retval -1 in case of failure
 * @retval 'offset' value set
 */
extern int
disk_seek(int offset, struct file *file);

/**
 * @brief
 */
extern void 
print_error(void);

/**
 * @}
 */

#endif /* __DISK_IO_H */
