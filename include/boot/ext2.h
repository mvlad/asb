#ifndef __EXT2_H
#define __EXT2_H

#include "common/types.h"

/**
 * @file boot/ext2.h EXT2 definitions for bootloader
 * @addtogroup BOOT
 * @{
 */

/** 
 * sizes are always in bytes, 
 * BLOCK values are always in DEV_BSIZE (sectors) 
 */
#define DEV_BSIZE 512

/** initial block size for superblock read */
#define BLOCK_SIZE 1024		

/** made up, defaults to 1 but can be passed via mount_opts */
#define WHICH_SUPER 1

/** = 2 */
#define SBLOCK (WHICH_SUPER * BLOCK_SIZE / DEV_BSIZE)	

/**
 * Constants relative to the data blocks
 */
#define EXT2_NDIR_BLOCKS        12
#define EXT2_IND_BLOCK          EXT2_NDIR_BLOCKS        /**< = 12 */
#define EXT2_DIND_BLOCK         (EXT2_IND_BLOCK + 1)    /**< = 13 */
#define EXT2_TIND_BLOCK         (EXT2_DIND_BLOCK + 1)   /**< = 14 */
#define EXT2_N_BLOCKS           (EXT2_TIND_BLOCK + 1)   /**< = 15 */

#define EXT2_SUPER_MAGIC        0xEF53
#define EXT2_ROOT_INO           2

/**
 * @brief ext2 super block structure
 */
struct ext2_super_block {
        uint32_t s_inodes_count;	/**< Inodes count */
        uint32_t s_blocks_count;	/**< Blocks count */
        uint32_t s_r_blocks_count;	/**< Reserved blocks count */
        uint32_t s_free_blocks_count;	/**< Free blocks count */
        uint32_t s_free_inodes_count;	/**< Free inodes count */
        uint32_t s_first_data_block;	/**< First Data Block */
        uint32_t s_log_block_size;	/**< Block size */
        uint32_t s_log_frag_size;	/**< Fragment size */
        uint32_t s_blocks_per_group;	/**< no of Blocks per group */
        uint32_t s_frags_per_group;	/**< no of Fragments per group */
        uint32_t s_inodes_per_group;	/**< no of Inodes per group */
        uint32_t s_mtime;		/**< Mount time */
        uint32_t s_wtime;		/**< Write time */

        uint16_t s_mnt_count;		/**< Mount count */
        int16_t  s_max_mnt_count;	/**< Maximal mount count */
        uint16_t s_magic;		/**< Magic signature */
        uint16_t s_state;		/**< File system state */
        uint16_t s_errors;		/**< Behaviour when detecting errors */
        uint16_t s_pad;

        uint32_t s_lastcheck;		/**< time of last check */
        uint32_t s_checkinterval;	/**< max. time between checks */
        uint32_t s_creator_os;		/**< OS */
        uint32_t s_rev_level;		/**< Revision level */

        uint16_t s_def_resuid;		/**< Default uid for reserved blocks */

        uint16_t s_def_resgid;		/**< Default gid for reserved blocks */
        uint32_t s_reserved[235];	/**< Padding to the end of the block */
};
/**
 * @brief ext2 group descriptor
 */
struct ext2_group_desc {
        uint32_t bg_block_bitmap;	/**< Blocks bitmap block */
        uint32_t bg_inode_bitmap;	/**< Inodes bitmap block */
        uint32_t bg_inode_table;	/**< Inodes table block */

        uint16_t bg_free_blocks_count;	/**< Free blocks count */
        uint16_t bg_free_inodes_count;	/**< Free inodes count */
        uint16_t bg_used_dirs_count;	/**< Directories count */
        uint16_t bg_pad;

        uint32_t bg_reserved[3];
};

/**
 * @brief ext2 inode 
 */
struct ext2_inode {
        uint16_t i_mode;		/**< File mode */
        uint16_t i_uid;		        /**< Owner Uid */

        uint32_t i_size;		/**< 4: Size in bytes */

        uint32_t i_atime;		/**< Access time */
        uint32_t i_ctime;		/**< 12: Creation time */
        uint32_t i_mtime;		/**< Modification time */
        uint32_t i_dtime;		/**< 20: Deletion Time */

        uint16_t i_gid;		        /**< Group Id */
        uint16_t i_links_count;	        /**< 24: Links count */

        uint32_t i_blocks;		/**< Blocks count */
        uint32_t i_flags;		/**< 32: File flags */

        union {
                struct {
                        uint32_t l_i_reserved1;
                } linux1;
                struct {
                        uint32_t h_i_translator;
                } hurd1;
                struct {
                        uint32_t m_i_reserved1;
                } masix1;
        } osd1;			                /**< OS dependent 1 */

        uint32_t i_block[EXT2_N_BLOCKS];	/**< 40: Pointers to blocks */
        uint32_t i_version;		        /**< File version (for NFS) */
        uint32_t i_file_acl;		        /**< File ACL */
        uint32_t i_dir_acl;		        /**< Directory ACL */
        uint32_t i_faddr;		        /**< Fragment address */

        union {
                struct {
                        uint8_t l_i_frag;	/**< Fragment number */
                        uint8_t l_i_fsize;	/**< Fragment size */

                        uint16_t i_pad1;

                        uint32_t l_i_reserved2[2];
                } linux2;
                struct {
                        uint8_t h_i_frag;	/**< Fragment number */
                        uint8_t h_i_fsize;	/**< Fragment size */

                        uint16_t h_i_mode_high;
                        uint16_t h_i_uid_high;
                        uint16_t h_i_gid_high;

                        uint32_t h_i_author;
                } hurd2;
                struct {
                        uint8_t m_i_frag;	/**< Fragment number */
                        uint8_t m_i_fsize;	/**< Fragment size */

                        uint16_t m_pad1;
                        uint32_t m_i_reserved2[2];
                } masix2;
        } osd2;			                /**< OS dependent 2 */
};


/** no of chars in a file name */
#define NAME_MAX        255	

#define PATH_MAX        1024
#define MAX_LINK_COUNT  5

#define EXT2_NAME_LEN   255

/**
 * @brief ext2 directory entry - file representation
 */
struct ext2_dir_entry {
        uint32_t inode;		        /**< Inode number */
        uint16_t rec_len;		/**< Directory entry length */
        uint8_t name_len;		/**< Name length */
        uint8_t file_type;
        char name[EXT2_NAME_LEN];	/**< File name */
};

/**
 * EXT2_DIR_PAD defines the directory entries boundaries
 * NOTE: It must be a multiple of 4
 */
#define EXT2_DIR_PAD                    4
#define EXT2_DIR_ROUND                  (EXT2_DIR_PAD - 1)
#define EXT2_DIR_REC_LEN(name_len)      \
        (((name_len) + 8 + EXT2_DIR_ROUND) & ~EXT2_DIR_ROUND)

#define log2(n)                 ffz(~(n))

#define EXT2_BLOCK_SIZE_BITS(s) ((s)->s_log_block_size + 10)
#define EXT2_BLOCK_SIZE(s)	(1 << EXT2_BLOCK_SIZE_BITS(s))

#define SUPERBLOCK \
    ((struct ext2_super_block *)(SUPER_BUF))

#define GROUP_DESC \
    ((struct ext2_group_desc *) \
     ((int) SUPERBLOCK + sizeof(struct ext2_super_block)))

#define INODE \
    ((struct ext2_inode *) \
     ((int) GROUP_DESC + EXT2_BLOCK_SIZE(SUPERBLOCK)))

#define DATABLOCK1 \
    ((int)((int) INODE + sizeof(struct ext2_inode)))

#define DATABLOCK2 \
    ((int)((int) DATABLOCK1 + EXT2_BLOCK_SIZE(SUPERBLOCK)))

#define EXT2_ADDR_PER_BLOCK(s)          \
        (EXT2_BLOCK_SIZE(s) / sizeof(uint32_t))

#define EXT2_ADDR_PER_BLOCK_BITS(s)	\
        (log2(EXT2_ADDR_PER_BLOCK(s)))

#define EXT2_DESC_PER_BLOCK(s)          \
        (EXT2_BLOCK_SIZE(s) / sizeof(struct ext2_group_desc))

#define GET_IND_BLOCKS()  \
        INODE->i_block[EXT2_IND_BLOCK]

#define GET_DIND_BLOCKS() \
        INODE->i_block[EXT2_DIND_BLOCK]

#define GET_TIND_BLOCKS() \
        INODE->i_block[EXT2_TIND_BLOCK]

#define GET_INODES_PER_GROUP()    \
        (SUPERBLOCK->s_inodes_per_group)

#define GET_GROUP_ID(current_ino)       \
        (((current_ino) - 1) / GET_INODES_PER_GROUP())

#define GET_GROUP_DESC(grp_id) \
        ((grp_id) >> log2(EXT2_DESC_PER_BLOCK(SUPERBLOCK)))

#define GET_GROUP_INDEX(grp_id) \
        ((grp_id) & (EXT2_DESC_PER_BLOCK(SUPERBLOCK) - 1))

#define GET_RAW_INODE(current_ino, blk_size) \
        (INODE + (((current_ino) - 1) & ((blk_size) - 1)))

#define GET_NR_BLOCKS() \
        (EXT2_BLOCK_SIZE(SUPERBLOCK) / DEV_BSIZE)

#define GET_BLK_SIZE(size_inode)        \
        (EXT2_BLOCK_SIZE(SUPERBLOCK) / (size_inode))

#define S_IFMT          0xf000
#define S_IFLNK         0xa000
#define S_IFREG         0x8000
#define S_IFDIR         0x4000

#define S_ISLNK(m)	(((m) & S_IFMT) == S_IFLNK)
#define S_ISREG(m)      (((m) & S_IFMT) == S_IFREG)
#define S_ISDIR(m)      (((m) & S_IFMT) == S_IFDIR)

/**
 * @brief
 */
static inline unsigned int
get_logical_blk(int pos)
{
        return (pos >> EXT2_BLOCK_SIZE_BITS(SUPERBLOCK));
}

/**
 * @brief
 */
static inline int
get_offset_loc(unsigned int pos)
{
        return (pos & (EXT2_BLOCK_SIZE(SUPERBLOCK) - 1));
}


/**
 * @brief ffz = Find First Zero in word. 
 *
 * Undefined if no zero exists,
 * so code should check against ~0UL first..
 */
static __inline__ unsigned long 
ffz(unsigned long word)
{
        __asm __volatile("bsfl %1,%0" : "=r" (word) : "r" (~word));
        return word;
}

/** 
 * @brief check filesystem type and read superblock into 
 * @link SUPER_BUF @endlink
 * @retval 1 in case of success, 0 otherwise
 *
 */
extern int
ext2_mount(void);

/**
 * @brief read a filename into @link INODE @endlink
 * @param dirn a pointer to a string representing the file
 * @param file a pointer to struct file
 * @retval 0 if error, nonzero iff we were able to find the file successfully
 *
 * @note messes up @link GROUP_DESC @endlink buffer area
 *
 * @note @link ext2_mount() @endlink already executed, therefore supblk in
 * buffer known as SUPERBLOCK
 *
 * @note on a nonzero return, buffer known as @link INODE @endlink contains the
 * inode of the file we were trying to look up
 */
extern int
ext2_dir(char *dirn, struct file *file);

/**
 * @brief reads into 'buf' the amount 'len' bytes from @link INODE @endlink
 * @param buf a char pointer where to read
 * @param len the size to read
 * @param file a pointer to struct file
 *
 * @note all preconds of @link ext2_block_map() @endlink
 *
 * Copy from INODE to buf
 */
extern int
ext2_read(char *buf, int len, struct file *file);


/**
 * @}
 */
#endif /* __EXT2_H */
