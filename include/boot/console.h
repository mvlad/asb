#ifndef __BOOT_CONSOLE_H
#define __BOOT_CONSOLE_H

/**
 * @addtogroup BOOT
 * @{
 */

/**
 * @brief Format a string and print it on the screen, just like the 
 * libc function printf. 
 * @param format the format string
 */
extern void 
bprintf(const char *format, ...);

/**
 * @brief initialize the console
 *
 * This function calls the respective cga|serial_init methods.
 *
 * @note can't call cprintf before this!
 *
 */
extern void 
cons_init(void);

/**
 * @brief debug method 
 */
#ifdef DEBUG_BOOT_PRINTF
#define str(x)  #x
#define xstr(x) str(x)
#define dprintf(format, args...) do {                   \
        bprintf("<%s(), ", __func__);                   \
        bprintf(__FILE__ ":" xstr(__LINE__) "> ");      \
        bprintf(format, ##args);                        \
} while (0)
#else
#define dprintf(format, args...) { }
#endif

/**
 * @}
 */
#endif /* __BOOT_CONSOLE_H */
