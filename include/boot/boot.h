#ifndef __BOOT_H
#define __BOOT_H

/**
 * @defgroup BOOT Second stage loader
 * @addtogroup BOOT
 * @{
 */

/** 
 * number of entries in E820 map
 *
 * @see @link get_mem_size @endlink 
 */
extern unsigned long e820_nr_entries;

/** 
 * where to save the E820 memory map
 *
 * @see @link get_mem_size @endlink 
 */
extern struct mmap_entry e820_mem_map;

/** 
 * where to save heads
 *
 * @see @link get_disk_geom @endlink  
 */
extern unsigned int heads;

/** 
 * where to save cyls
 *
 * @see @link get_disk_geom @endlink 
 */
extern unsigned int cyls;

/** 
 * where to save sectors 
 * @see @link get_disk_geom @endlink 
 */
extern unsigned int sectors;

/** 
 * buffer to save disk geometry
 * @see @link get_dap_disk_geom @endlink 
 */
extern struct drive_parameters DAP;

/**
 * @brief clears the video buffer, using a special case in
 * the BIOS function "SCROLL UP WINDOW".  
 *
 * @note this function is only available in real mode, and 
 * some buggy BIOSes destroy the base pointer %bp.
 */
extern void clrscr(void);

/**
 * @brief set cursor position to 0:0
 *
 * Moves the cursor to position 0:0 (top:left),
 * using the BIOS function "SET CURSOR POSITION".  
 *
 * @note this function is only available in real mode.
 *
 */
extern void curshome(void);

/**
 * @brief output char stored %al via BIOS call int 10h, func 0Eh 
 *
 * putc() displays the byte %al on the default video
 * buffer, using the BIOS function "TELETYPE OUTPUT".
 * 
 * @note this function interprets some but not all control characters correctly.
 *
 * @note this function is only available in real mode.
 *
 */
extern void putc(void); 

/**
 * @brief display 0-terminated string via @link putc() @endlink
 *
 * puts() repeatedly loads a byte from the buffer pointed
 * to by %ds:%si into %al, and displays that byte by calling
 * putc(), until a \0-byte is encountered.
 */
extern void puts(void);

/**
 * @brief get the E820 memory map the machine has in @link e820_mem_map @endlink
 * before jumping to protected mode via INT 0x15, eax= 0xE820
 */
extern void get_mem_size(void);

/**
 * @brief get the geometry the disk geometry before jumping to protected mode
 * and save them into @link sectors @endlink, @link cyls @endlink and @link
 * heads @endlink
 */
extern void get_disk_geom(void);

/**
 * @brief retrieve the disk geomentry of the hard drive via BIOS call 0x13, %ah = 0x42
 *
 * @note used to retrieve, and later on store, total count of sectors
 *
 * @see http://www.ctyme.com/intr/rb-0715.htm
 */
extern void get_dap_disk_geom(void);

/**
 * @brief backwards compatibility with the earliest PCs, physical
 * address line 20 is tied low, so that addresses higher than
 * 1MB wrap around to zero by default.  
 *
 * This code undoes this.
 */
extern void enable_a20(void);


/**
 * @brief gets called from bios.S
 *
 * Entry point for the second stage bootloader:
 * - opens an EXT2 partition to retrieve the kernel image into a buffer
 * - it checks if the image is indeed a ELF file, and loads the image into
 *   mememory
 * - it display E820 memory map and drive geometry
 * - saves the memory map and the drive geometry into 
 *   @link multiboot_info @endlink structure, and passes the 
 *   physical address with the help of registers
 * - finally it passes control to @link entry_func @endlink from @link
 *   e_entry @endlink section.
 */
void bmain(void);

/**
 * @}
 */

#endif
