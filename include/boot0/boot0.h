/**
 * @defgroup BOOT0 First stage loader
 * @addtogroup BOOT0
 * @{
 */
#ifndef __BOOT0_H
#define __BOOT0_H

/**
 * @brief clears the video buffer, using a special case in
 * the BIOS function "SCROLL UP WINDOW".  
 *
 * @note this function is only available in real mode, and 
 * some buggy BIOSes destroy the base pointer %bp.
 */
extern void clrscr(void);

/**
 * @brief set cursor position to 0:0
 *
 * Moves the cursor to position 0:0 (top:left),
 * using the BIOS function "SET CURSOR POSITION".  
 *
 * @note this function is only available in real mode.
 *
 */
extern void curshome(void);

/**
 * @brief output char stored in %al via BIOS call int 0x10, function 0x0e
 *
 * putc() displays the byte %al on the default video
 * buffer, using the BIOS function "TELETYPE OUTPUT".
 * 
 * @note this function interprets some but not all control characters correctly.
 *
 * @note this function is only available in real mode.
 *
 */
extern void putc(void); 

/**
 * @brief read sectors from disk into mememory via BIOS call int 0x13, function 0x2
 *
 * Registers used:
 * %ax = from sector
 * %cl = how many sectors
 * %dx:%bx = segment:offset in memory where to save
 *
 * @note carry set: failure
 * %ah = err code, %al = number of sectors transferred
 *
 * @note carry clear: success
 * %al = 0x0 OR number of sectors transferred
 * (depends on BIOS, according to Ralph Brown Int List)
 */
extern void read_sect(void);

/**
 * @}
 */
#endif
