#ifndef __KERNEL_DRAW_H
#define __KERNEL_DRAW_H

/**
 * @brief
 */
extern void
draw_dot(uint8_t x, uint8_t y, uint8_t color);


extern void
draw_line(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t color);

/**
 * @brief
 */
extern void
fill_by_dot(uint8_t color);

extern void
draw_circle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color);

extern void
draw_vert_line(uint8_t x, uint8_t y, uint8_t h, uint8_t color);

extern void
draw_horiz_line(uint8_t x, uint8_t y, uint8_t w, uint8_t color);

extern void
draw_rect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color);

extern
void cls(void);

#endif /* __KERNEL_DRAW_H */
