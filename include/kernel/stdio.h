#ifndef __KERNEL_STDIO_H
#define __KERNEL_STDIO_H

/**
 * @addtogroup KERNEL
 * @{
 */

#include "stdarg.h"

/* from kernel/console.c */

/**
 * @brief
 */
void cons_putc(int c);

/**
 * @brief
 */
int getchar(void);

/**
 * @brief
 */
int iscons(int fd);

/**
 *
 */
int  
vcprintf(const char *fmt, va_list ap);

/** 
 * @brief main printf function
 * @param fmt the input string 
 * @retval ignored
 * @note implementation in kernel/printf.c
 */
int kprintf(const char *fmt, ...);

#ifdef DEBUG_KERNEL_PRINTF
#define str(x)  #x
#define xstr(x) str(x)
#define dprintf(format, args...) do {                   \
                kprintf("<%s(), ", __func__);                   \
                kprintf(__FILE__ ":" xstr(__LINE__) "> ");      \
                kprintf(format, ##args);                        \
} while (0)

#else
#define dprintf(format, args...) { }
#endif

/**
 * @}
 */
#endif /* __KERNEL_STDIO_H */
