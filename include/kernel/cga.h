#ifndef __KERNEL_CGA_H
#define __KERNEL_CGA_H

/**
 * @addtogroup KERNEL
 * @{
 */

#include "common/cga.h"

/**
 * @brief initialize CGA
 */
extern void cga_init(void);

/**
 * @brief output a char on the display
 * @param c the char to output
 */
extern void cga_putc(int c);

extern void cls(void);

/**
 * @}
 */

#endif /* __KERNEL_CGA_H */
