#ifndef __KERNEL_SERIAL_H
#define __KERNEL_SERIAL_H

/**
 * @file kernel/serial.h KERNEL USART methods
 * @addtogroup KERNEL
 * @{
 */

#include "common/serial.h"

/**
 * @brief initialize serial port
 */
extern void
serial_init(void);

/**
 * @brief output a char on the serial port
 * @param c the char to output
 */
extern void
serial_putc(int c);

/**
 * @brief
 */
extern void
serial_intr(void);

/**
 * @}
 */

#endif /* __KERNEL_SERIAL_H */
