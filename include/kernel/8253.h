#ifndef __KERNEL_8253_H
#define __KERNEL_PIT_H

/**
 * Channel 0 data port
 *
 * The output from PIT channel 0 is connected to the PIC chip, so that it
 * generates an "IRQ 0". Typically during boot the BIOS sets channel 0 with a
 * count of 65535 or 0 (which translates to 65536), which gives an output
 * frequency of 18.2065 Hz (or an IRQ every 54.9254 mS). 
 *
 * Channel 0 is is the only channel that is connected to an IRQ. 
 *
 * It can be used to generate an infinte series of "timer ticks" at a
 * frequency of your choice (as long as it is higher than 18 Hz), or to generate
 * single CPU interrupts (in "one shot" mode) after programmable short delays
 * (less than an 18th of a second). 
 */
#define PIT_CHANNEL_0           0x40 

/**
 * Channel 1 data port -- unused
 */
#define PIT_CHANNEL_1           0x41

/**
 * Channel 2 data port
 *
 * The output of PIT channel 2 is connected to the PC speaker, so the frequency
 * of the output determines the frequency of the sound produced by the speaker.
 * This is the only channel where the gate input can be controlled by software
 * (via bit 0 of I/O port 0x61), and the only channel where its output (a high
 * or low voltage) can be read by software (via bit 5 of I/O port 0x61).
 */
#define PIT_CHANNEL_2           0x42

/**
 * The Mode/Command register
 * Bits         Usage
 * 6 and 7      Select channel :
 *                 0 0 = Channel 0
 *                 0 1 = Channel 1
 *                 1 0 = Channel 2
 *                 1 1 = Read-back command (8254 only)
 * 4 and 5      Access mode :
 *                 0 0 = Latch count value command
 *                 0 1 = Access mode: lobyte only
 *                 1 0 = Access mode: hibyte only
 *                 1 1 = Access mode: lobyte/hibyte
 * 1 to 3       Operating mode :
 *                 0 0 0 = Mode 0 (interrupt on terminal count)
 *                 0 0 1 = Mode 1 (hardware re-triggerable one-shot)
 *                 0 1 0 = Mode 2 (rate generator)
 *                 0 1 1 = Mode 3 (square wave generator)
 *                 1 0 0 = Mode 4 (software triggered strobe)
 *                 1 0 1 = Mode 5 (hardware triggered strobe)
 *                 1 1 0 = Mode 2 (rate generator, same as 010b)
 *                 1 1 1 = Mode 3 (square wave generator, same as 011b)
 * 0            BCD/Binary mode: 0 = 16-bit binary, 1 = four-digit BCD
 * 
 *
 */

#define PIT_MODE                0x43

/**
 * @brief
 */
extern void 
time_tick(void);

/**
 *
 */
void 
delay_ms(unsigned int ms);

/**
 * @brief
 */
extern void
pit_init(uint16_t freq);

#endif /* __KERNEL_PIT_H */
