#ifndef __KERNEL_PMAP_H
#define __KERNEL_PMAP_H

extern struct page *pages;

/**
 * @brief maximum memory allowed
 */
#define MAX_MEM 0x100000000

/**
 * @brief data structure holding accountable pages
 */
struct accounting_pages {
        size_t npages;          /** Amount of physical memory (in pages) */
        size_t npages_base;     /** Amount of base memory (in pages) */
        size_t free;            /** Amount of free memory (in pages) */
        size_t reserved;        /** Amount of reserved memory (in pages) */
        size_t used;            /** Amount of used memory (in pages) */
        size_t total;           /** Total amount of used memory (in pages) */
        size_t ram;             /** Total amount of RAM available (in kiB) */
};

extern struct accounting_pages account_pages;

/**
 * @brief linear address representation
 *
 * A linear address 'la' has a three-part structure as follows:
 *
 * +--------10------+-------10-------+---------12----------+
 * | Page Directory |   Page Table   | Offset within Page  |
 * |      Index     |      Index     |                     |
 * +----------------+----------------+---------------------+
 *  \--- PDX(la) --/ \--- PTX(la) --/ \---- PGOFF(la) ----/
 *  \---------- PGNUM(la) ----------/
 *
 * The PDX, PTX, PGOFF, and PGNUM macros decompose linear addresses as shown.
 * To construct a linear address la from PDX(la), PTX(la), and PGOFF(la),
 * use PGADDR(PDX(la), PTX(la), PGOFF(la)).
 *
 *  ----
 * |  0 | ------------->/-------\
 * |----|               |   0   | ------------> ---------
 * |  1 |               |-------|               |   0   |
 * |----|               |   1   |               |-------|
 * |    |               |-------|               |   1   |
 *  ....                 .......                |-------|
 * |    |               |       |                .......
 * |1023|               |  1023 |               |       |
 *  ----                \-------/               |  4095 |
 *   PD                   PT                    ---------
 *   (1024)              (1024)                    PTE
 *   (2^10)              (2^10)                 (4096, 2^12)
 */

/** 
 * @brief page number field of address 
 */
#define PGNUM(la)       (((uintptr_t) (la)) >> PTXSHIFT)

/** 
 * @brief page directory index 
 */
#define PDX(la)         ((((uintptr_t) (la)) >> PDXSHIFT) & 0x3ff)

/**
 * @brief page table index 
 */
#define PTX(la)         ((((uintptr_t) (la)) >> PTXSHIFT) & 0x3ff)

/** 
 * @brief Address in page table or page directory entry 
 */
#define PTE_ADDR(pte)   ((physaddr_t) (pte) & ~0xfff)

static inline physaddr_t
_paddr(void *kva)
{
        assert((uint32_t) kva > KERNBASE);
        return (physaddr_t) kva - KERNBASE;
}

/**
 * @brief take a kernel virtual address (an address that points above
 * KERNBASE, where the machine's maximum physical memory is mapped), and returns
 * the corresponding physical address.
  */
#define PADDR(kva) _paddr(kva)

static inline void *
_kaddr(physaddr_t pa)
{
        assert(PGNUM(pa) < account_pages.npages);
        return (void *) (pa + KERNBASE);
}

/**
 * @brief take a physical address and returns the corresponding kernel
 * virtual address.
 */
#define KADDR(pa) _kaddr(pa)

/**
 * @brief page descriptor structures, mapped at UPAGES.
 * Read/write to the kernel, read-only to user programs.
 *
 * @note each struct page stores metadata for one physical page.
 * Is it NOT the physical page itself, but there is a one-to-one
 * correspondence between physical pages and struct page's.
 *
 */
struct page {
        /** Next page on the free list. */
        struct page *next;

        /*
         * count of pointers (usually in page table entries) to this page, for
         * pages allocated using page_alloc().
         *
         * Pages allocated at boot time using boot_alloc do not have valid
         * reference count fields.
         */
        uint16_t ref_cnt;
};

/**
 * @brief translate a page to physical address
 */
static inline physaddr_t
page2pa(struct page *pp)
{
        return (pp - pages) << PGSHIFT;
}

/**
 * @brief translate physical address to a page
 */
static inline struct page *
pa2page(physaddr_t pa)
{
        assert(PGNUM(pa) <= account_pages.npages);
        return &pages[PGNUM(pa)];
}

/**
 * @brief translate a page to kernel virtual address
 */
static inline void *
page2kva(struct page *pp)
{
        return KADDR(page2pa(pp));
}

enum {
        /**< For page_alloc, zero the returned physical page. */
        ALLOC_ZERO = 1 << 0,
};

/**
 * @brief show memory info
 */
extern void
mem_info(void);

/**
 * @brief initialize memory
 */
extern void
mem_init(struct multiboot_info *mbi);

#define THREADS_NO         (1 << 8)

/** Values of env_status in struct Env */
typedef enum {
        THREAD_FREE = 0,
        THREAD_DYING,
        THREAD_RUNNABLE,
        THREAD_RUNNING,
        THREAD_SLEEPING,
        THREAD_NOT_RUNNABLE
} thread_status;

/** Special environment types */
typedef enum {
        TYPE_USER = 0,
        TYPE_IDLE,
} thread_type;

struct thread {
        struct tf thread_tf;            /**< Saved registers */
        struct thread *next;            /**< Next free thread */

        int32_t id;                     /**< Unique identifier */
        int32_t parent_id;              /**< id of this thread's parent */

        thread_type type;               /**< Indicates special thread */
        thread_status status;           /**< Status of the thread */
        uint32_t thread_runs;           /**< Number of times thread has run */

        int cpunum;                     /**< The CPU that the thread is running on */

        /* Address space */
        uint32_t *pgdir;                /**< Kernel virtual address of page dir */

        void *dstva;                    /**< VA at which to map received page */ 
        int perm;                       /**< Perm of page mapping received */
};

#define COUNT(NAME, COND)                               \
static uint32_t                                         \
count_##NAME(void)                                      \
{                                                       \
        uint32_t cnt = 0;                               \
        struct page *pp = pages;                        \
        uint32_t npages = account_pages.npages;         \
                                                        \
        while (pp && npages--) {                        \
                if (COND) {                             \
                        cnt++;                          \
                }                                       \
                pp++;                                   \
        }                                               \
        return cnt;                                     \
}

#endif /* __KERNEL_PMAP_H */
