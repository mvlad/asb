#ifndef __KERNEL_STRING_H
#define __KERNEL_STRING_H

#include "common/types.h"

/**
 * @addtogroup KERNEL
 * @{
 */

/**
 * @brief calculate the length of a string
 * @param s the input string
 * @retval the size of the string
 */
int strlen(const char *s);

/**
 * @brief determine the length of a fixed-size string
 * @param s the input string
 * @param size the maximum length
 * @retval returns strlen(s), if that is less than 'size', or 'size' if there is
 * no null byte ('\0') among the first size bytes pointed to by s.
 */
int strnlen(const char *s, size_t size);

/**
 * @brief copies the string pointed to by src to the buffer pointed to by dest
 * @param dst the destination buf
 * @param src the source buf
 * @retval a pointer to the destination string dst
 */
char *strcpy(char *dst, const char *src);

/**
 * @brief copies at most size bytes of the string pointed to by src to the 
 * buffer pointed to by dest
 * @param dst the destination buf
 * @param src the source buf
 * @param size the amount to copy
 * @retval  a pointer to the destination string dst
 */
char *strncpy(char *dst, const char *src, size_t size);

/**
 * @brief safer and more consistent version of strcpy
 * @param dst destination string
 * @param src source string
 * @param size the amount to copy
 * @retval a pointer to the resulting string dst
 */
size_t strlcpy(char *dst, const char *src, size_t size);

/**
 * @brief concatenate two strings
 * @param dst destination string
 * @param src source string
 * @retval a pointer to the resulting string dst
 */
char    *strcat(char *dst, const char *src);

/**
 * @brief compare two strings
 * @param s1 input string
 * @param s2 compare against
 * @retval an integer equal to zero if the two strings match
 */
int strcmp(const char *s1, const char *s2);

/**
 * @brief compare strings at most 'size' bytes 
 * @param s1 input string
 * @param s2 compare against
 * @param size amount in bytes to compare
 * @retval an integer equal to zero if the two strings match
 */
int strncmp(const char *s1, const char *s2, size_t size);

/**
 * @brief finds the first occurrence of 'c' in 'str'
 * @param str input string
 * @param c the char to look for
 * @retval a pointer to the first occurrence of 'c' in 's', or a null 
 * pointer if the string has no 'c'.
 */
char *strchr(const char *str, char c);

/**
 * @brief finds the first occurrence of 'c' in 'str'
 * @param str input string
 * @param c the char to look for
 * @retval a pointer to the first occurrence of 'c' in 'str', or a pointer
 * to the null byte at the end of 'str'
 */
char *strfind(const char *str, char c);

/**
 * @brief fill memory with a constant byte
 * @param dst the memory area that will be filled
 * @param c the constant byte c used to fill the memory area
 * @param len the amount of bytes to fill
 * @retval a pointer to the memory area 'dst'
 */
void *memset(void *dst, int c, size_t len);

/**
 * @brief copy memory area from 'src' to 'dst' 
 * @param src from memory area 
 * @param dst to memory area
 * @param len the maximum amount to copy
 * @retval a pointer to 'dst'
 */
void *memmove(void *dst, const void *src, size_t len);
  
/**
 * @brief compares the first 'len' bytes of the memory areas 's1' and 's2'
 * @param s1 first memory area
 * @param s2 second memory area
 * @param len the amount of bytes to compare
 * @retval an integer equal to zero if the two areas match
 * @retval an integer less than or greater than zero otherwise
 */
int memcmp(const void *s1, const void *s2, size_t len);

/**
 * @brief compares the first 'len' bytes of the memor area 's' agaisnt
 * the char 'c'
 * @param s memory area 
 * @param c the char to look for
 * @param len 
 * @retval a pointer to the first occurrence of 'c' in 's', or a pointer
 * to the null byte at the end of 's'
 */
void *memfind(const void *s, int c, size_t len);

/**
 * @brief converts the string in s to a long integer
 * value according to the base
 * @param s the string to convert
 * @param endptr stores invalid chars 
 * @param base the base to convert to
 */
long int strtol(const char *s, char **endptr, int base);

/**
 * @}
 */
#endif /* __KERNEL_STRING_H */
