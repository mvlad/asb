#ifndef __KERNEL_PANIC_H
#define __KERNEL_PANIC_H


/**
 * @brief panic is called on unresolvable fatal errors.
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes enter the kernel monitor.
 */
extern void
_panic(const char *file, int line, const char *fmt, ...);

/**
 * @brief like panic, but don't 
 */
void
_warn(const char *file, int line, const char *fmt,...);

#endif /* __KERNEL_PANIC_H */
