#ifndef __KERNEL_MMU_H
#define __KERNEL_MMU_H

/*
 * @file kernel/mmu.h definitions for the x86 memory management unit (MMU),
 * paging-related constants, the %cr0, %cr4, and %eflags registers.
 * @addtogroup KERNEL
 * @{
 */

/* Page directory and page table constants. */

#define NPDENTRIES	1024	        /**< page directory entries per page directory */
#define NPTENTRIES	1024	        /**< page table entries per page table */
#define PGSIZE		4096	        /**< bytes mapped by a page */
#define PTSIZE		0x400000        /**< bytes mapped by a page directory entry */
#define PTSHIFT		22	        /**< log2(PTSIZE) */
#define PGSHIFT		12	        /**< log2(PGSIZE) */
#define PTXSHIFT	12	        /**< offset of PTX in a linear address */
#define PDXSHIFT	22	        /**< offset of PDX in a linear address */

/* Page table/directory entry flags. */
#define PTE_P		0x001	        /**< Present */
#define PTE_W		0x002	        /**< Writeable */
#define PTE_U		0x004	        /**< User */
#define PTE_PWT		0x008	        /**< Write-Through */
#define PTE_PCD		0x010	        /**< Cache-Disable */
#define PTE_A		0x020	        /**< Accessed */
#define PTE_D		0x040	        /**< Dirty */
#define PTE_PS		0x080	        /**< Page Size */
#define PTE_G		0x100	        /**< Global */

/* EFLAGS bits */
#define X86_EFLAGS_CF   0x00000001      /**< Carry Flag */
#define X86_EFLAGS_PF   0x00000004      /**< Parity Flag */
#define X86_EFLAGS_AF   0x00000010      /**< Auxiliary carry Flag */
#define X86_EFLAGS_ZF   0x00000040      /**< Zero Flag */
#define X86_EFLAGS_SF   0x00000080      /**< Sign Flag */
#define X86_EFLAGS_TF   0x00000100      /**< Trap Flag */
#define X86_EFLAGS_IF   0x00000200      /**< Interrupt Flag */
#define X86_EFLAGS_DF   0x00000400      /**< Direction Flag */
#define X86_EFLAGS_OF   0x00000800      /**< Overflow Flag */
#define X86_EFLAGS_IOPL 0x00003000      /**< IOPL mask */
#define X86_EFLAGS_NT   0x00004000      /**< Nested Task */
#define X86_EFLAGS_RF   0x00010000      /**< Resume Flag */
#define X86_EFLAGS_VM   0x00020000      /**< Virtual Mode */
#define X86_EFLAGS_AC   0x00040000      /**< Alignment Check */
#define X86_EFLAGS_VIF  0x00080000      /**< Virtual Interrupt Flag */
#define X86_EFLAGS_VIP  0x00100000      /**< Virtual Interrupt Pending */
#define X86_EFLAGS_ID   0x00200000      /**< CPUID detection flag */

/* Control Register flags */
#define CR0_PE		0x00000001	/**< Protection Enable */
#define CR0_MP		0x00000002	/**< Monitor coProcessor */
#define CR0_EM		0x00000004	/**< Emulation */
#define CR0_TS		0x00000008	/**< Task Switched */
#define CR0_ET		0x00000010	/**< Extension Type */
#define CR0_NE		0x00000020	/**< Numeric Errror */
#define CR0_WP		0x00010000	/**< Write Protect */
#define CR0_AM		0x00040000	/**< Alignment Mask */
#define CR0_NW		0x20000000	/**< Not Writethrough */
#define CR0_CD		0x40000000	/**< Cache Disable */
#define CR0_PG		0x80000000	/**< Paging */

#define CR4_PCE		0x00000100	/**< Performance counter enable */
#define CR4_MCE		0x00000040	/**< Machine Check Enable */
#define CR4_PSE		0x00000010	/**< Page Size Extensions */
#define CR4_DE		0x00000008	/**< Debugging Extensions */
#define CR4_TSD		0x00000004	/**< Time Stamp Disable */
#define CR4_PVI		0x00000002	/**< Protected-Mode Virtual Interrupts */
#define CR4_VME		0x00000001	/**< V86 Mode Extensions */

/* EFLAG register */
#define FL_CF		0x00000001	/**< Carry Flag */
#define FL_PF		0x00000004	/**< Parity Flag */
#define FL_AF		0x00000010	/**< Auxiliary carry Flag */
#define FL_ZF		0x00000040	/**< Zero Flag */
#define FL_SF		0x00000080	/**< Sign Flag */
#define FL_TF		0x00000100	/**< Trap Flag */
#define FL_IF		0x00000200	/**< Interrupt Flag */
#define FL_DF		0x00000400	/**< Direction Flag */
#define FL_OF		0x00000800	/**< Overflow Flag */
#define FL_IOPL_MASK	0x00003000	/**< I/O Privilege Level bitmask */
#define FL_IOPL_0	0x00000000	/**<   IOPL == 0 */
#define FL_IOPL_1	0x00001000	/**<   IOPL == 1 */
#define FL_IOPL_2	0x00002000	/**<   IOPL == 2 */
#define FL_IOPL_3	0x00003000	/**<   IOPL == 3 */
#define FL_NT		0x00004000	/**< Nested Task */
#define FL_RF		0x00010000	/**< Resume Flag */
#define FL_VM		0x00020000	/**< Virtual 8086 mode */
#define FL_AC		0x00040000	/**< Alignment Check */
#define FL_VIF		0x00080000	/**< Virtual Interrupt Flag */
#define FL_VIP		0x00100000	/**< Virtual Interrupt Pending */
#define FL_ID		0x00200000	/**< ID flag */


/** @brief Global descriptor numbers */
#define GD_KT           0x08            /**< kernel text */
#define GD_KD           0x10            /**< kernel data */
#define GD_UT           0x18            /**< user text */
#define GD_UD           0x20            /**< user data */
#define GD_TSS0         0x28            /**< Task segment selector for CPU 0 */

/**
 * @brief physical memory mapped at this address
 */
#define	KERNBASE	0xc0000000

/**
 * At IOPHYSMEM (640K) there is a 384K hole for I/O.  
 *
 * From the kernel, IOPHYSMEM can be addressed at KERNBASE + IOPHYSMEM.
 */
#define IOPHYSMEM	0x0A0000

/**
 * The hole ends at physical address EXTPHYSMEM. (1MiB)
 */
#define EXTPHYSMEM	0x100000

/** Physical address of startup code for non-boot CPUs (APs) */
#define MPENTRY_PADDR   0x7000


#define KSTACKTOP	(KERNBASE - PTSIZE)     /**< kernel stack @ 0xbfc00000 */
#define KSTKSIZE	(8 * PGSIZE)   	        /**< size of a kernel stack */
#define KSTKGAP		(8 * PGSIZE)   	        /**< size of a kernel stack guard */
#define ULIM		(KSTACKTOP - PTSIZE)    /**< @ 0xbf800000 */

/** User read-only virtual page table @ 0xbf400000 */
#define UVPT            (ULIM - PTSIZE)

/** Read-only copies of the Page structures @ 0xbf000000 */
#define UPAGES          (UVPT - PTSIZE)

/** Read-only copies of the global env structures @ 0xbec00000 */
#define UENVS           (UPAGES - PTSIZE)

/** The physical address where IO memory starts */
#define IOMEM_PADDR	0xfe000000

/** The virtual address we map IO memory to */
#define IOMEMBASE	0xfe000000


#define USTACKTOP       (UENVS - 2 * PGSIZE)

/** Where user programs generally begin */
#define UTEXT           (2 * PTSIZE)

#define UTEMP           ((void*) PTSIZE)


/**
 * @}
 */
#endif /* __KERNEL_MEMLAYOUT_H */
