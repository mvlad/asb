#ifndef __KERNEL_ASSERT_H
#define __KERNEL_ASSERT_H

#include "stdio.h"

/**
 * @file include/kernel/assert.h standard assert for kernel
 * @addtogroup KERNEL
 */

void _warn(const char*, int, const char*, ...);
void _panic(const char*, int, const char*, ...) __attribute__((noreturn));

/**
 * @ingroup KERN
 */
#define warn(...)       _warn(__FILE__, __LINE__, __VA_ARGS__)

/**
 * @ingroup KERN
 */
#define panic(...)      _panic(__FILE__, __LINE__, __VA_ARGS__)

/**
 * @brief a assert version
 *
 * @warning that using either OR, or, AND might not have
 * the expected behaviour. You might want to use `!!` to convert
 * an integer variable to a boolean one. 
 *
 * For instance:
 *
 * @code{cpp}
 * int a, b, c;
 * a = 10, c = 3, c = 1;
 * int d = !!((a + b == 13) || (c == 0));
 * int p = !((a + b == 13) || (c == 0));
 * // test output
 * d = 1 
 * p = 0
 * @endcode
 *
 */
#define assert(x)  do {                                 \
        if (!(x))                                       \
                panic("assertion failed: %s", #x);      \
} while (0)


#endif /* __KERNEL_ASSERT_H */
