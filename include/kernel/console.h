/**
 * @addtogroup KERNEL
 * @{
 */
#ifndef __KERNEL_CONSOLE_H
#define __KERNEL_CONSOLE_H


#define CONSBUFSIZE 512

/**
 * @brief initialize the console devices
 * @note this calls cga_init(), kbd_init() and serial_init()
 */
extern void
cons_init(void);

/**
 * @brief get the next input character from the console
 * @retval return the next input character from the console
 * @retval 0 if none waiting
 */
int
cons_getc(void);


/**
 * @brief output a character to the console 
 */
void
cons_putc(int c);

/**
 * @brief called by device interrupt routines to feed input characters
 * into the circular console input buffer.
 */
void
cons_intr(int (*proc)(void));

/**
 * @brief
 */
char
*readline(const char *prompt);

/**
 * @brief
 */
int
getchar_kbd(void);

/**
 * @brief
 */
int
getchar_serial(void);

/**
 * @}
 */
#endif /* __CONSOLE_H */
